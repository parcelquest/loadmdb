#ifndef  _SQLEXT_H
#define  _SQLEXT_H

#include "hlAdo.h"

#define  SIZ_ZIPCITY                25
#define  SIZ_ZIPCOUNTRY             20

typedef struct _tZipTbl
{
   char  ZipCode[5];
   char  ZipSfx;
   char  City[SIZ_ZIPCITY];
   char  St[2];
   char  Country[SIZ_ZIPCOUNTRY];
} ZIPTBL;

int  locateCity(LPCSTR pZipcode, LPSTR pCity, LPSTR pState, LPSTR pCountry, LPCSTR pCounty=NULL);
int  importZipcode(LPCSTR pZipfile);
int  execCmd(LPCTSTR strCmd);
int  execCmdEx(LPCTSTR strCmd, hlAdo *phDb);
bool sqlConnEx(LPCSTR pProvider, hlAdo *phDb);

bool sqlConnect(LPCSTR pProviderTmpl, LPCSTR strDb, hlAdo *phDb=NULL);
int  execSqlCmd(LPCTSTR strCmd, hlAdo *phDb=NULL);
int  runSP(char *strCmd, char *pDBName, char *pServerName, char *pProviderTmpl);

#ifdef _SQLEXT_CPP
   int      m_iTimeOut;
   extern   bool  m_bConnected;
   extern   hlAdo hl;
#else
   extern   int   m_iTimeOut;
#endif

#endif