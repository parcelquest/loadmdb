/**************************************************************************
 *
 * Notes: 
 *
 * 10/13/2015 Add Tax_CreateTaxOwnerCsv(), importTaxFile(), and simplify doTaxImport().
 * 10/15/2015 Modify Tax_CreateTaxOwnerCsv() due to TAXOWNER changed
 * 01/04/2016 Add Load_TC() and TC_*() to load scrape tax data.
 * 01/05/2016 Add TC_SetDateFmt() to specify date format in TC file. 
 *            Modify TC_ParseTaxBase() to set default due date if not provided.
 * 03/07/2016 Rename "Detail" to "Items"
 * 03/09/2016 Update lLastTaxFileDate in Load_TC()
 * 03/16/2016 Change sort parameters in TC_LoadTaxAgencyAndDetail() to dedup agency file.
 * 04/06/2016 Modify TC_ParseTaxDist() to fix bug when tax rate is fixed.
 * 04/08/2016 Modify TC_ParseTaxBase() to add default DueDate.
 * 04/12/2016 Add Tax_CreatePaidCsv().  Modify doTaxImport() to support loading TAXPAID.
 * 04/14/2016 Add DelqStatus to Tax_CreateDelqCsv()
 * 04/20/2016 Add updateTaxAgency() for RIV
 * 05/03/2016 Modify TC_ParseTaxBase() to populate PaidDate1 when PaidDate2 is avail.
 * 08/04/2016 Modify Tax_CreateTaxBaseCsv() to add delqYear and UpdatedDate
 *            Modify Tax_CreateAgencyCsv() to add TaxAmt
 * 08/17/2016 Add LoadTaxCodeTable() and findTaxDist(). Modify Tax_CreateDetailCsv() to include TaxRate
 *            in Tax_Items since same Agency may have multiple rate.
 * 08/18/2016 Move iNumTaxDist to Tax.h
 * 08/19/2016 Modify TC_ParseTaxDist() to support external TaxDist list
 * 08/24/2016 Modify Tax_CreateDelqCsv() to fill BillNum with DefaultNo if available.
 * 08/25/2016 Add doTaxPrep() to create empty table.
 * 08/29/2016 Add findTaxAgency() to find agency name using TaxCode
 * 09/01/2016 Modify prototype of findTaxAgency() to support different starting point, default is 0.
 * 09/07/2016 Modify Tax_CreateTaxBaseCsv() to add option to use Upd_Date & findTaxDist() to add 
 *            option to start search at any given position.
 * 09/14/2016 Use global lTaxYear in TC_ParseTaxBase() and TC_ParseTaxDist()
 * 09/15/2016 Add iTaxYear in Tax.h
 * 10/12/2016 Modify LoadTaxCodeTable() to add option for tax code translation.
 * 10/14/2016 Modify Load_TC() to load TaxBase first so the TaxYear is set correctly.
 *            Modify TC_ParseTaxBase() to use iTaxYear instead of lLienYear to format DueDate.
 * 10/15/2016 Modify TC_ParseTaxBase(): If TaxYear < 1900, drop that record
 *            Modify Tax_CreateAgencyCsv() to force uppercase on Agency
 * 10/19/2016 Add InstDueDate() and modify TC_ParseTaxBase() and TC_ParseTaxDist()
 *            since co3DIST.TC layout has been changed.  TC_ParseTaxDist now will
 *            pick up phone if available.  Add comment.
 * 10/21/2016 Fix PaidAmt1 in TC_ParseTaxBase().  Modify TC_ParseTaxDist() to add BillNum.
 * 10/23/2016 Modify Tax_CreatePaidCsv() to use Default_No if present (ORG).
 * 10/24/2016 Add update_TI_BillNum() to update Tax_Items.BillNum using Tax_Base
 * 10/25/2016 Add Tax_CreateAgencyTable() to load district file into SQL table TAX_AGENCY
 * 11/24/2016 Modify TC_LoadTaxAgencyAndDetail() to add Phone number to Agency record
 * 11/29/2016 Add TC_LoadTaxBaseAndDelq() to create Tax_Delq if data available.
 * 01/09/2017 Add m_bUseFeePrcl to turn ON/OFF the use of FeePrcl in place of BillNum when BillNum is not available.
 *            Add ChkDueDate() function to check whether updated date has passed certain due date.
 * 01/11/2017 Modify TC_ParseTaxDist() to ignore item that has no tax amt.
 * 01/18/2017 Modify TC_ParseTaxBase() to add check for DEF_DATE in new TC layout.  
 *            If PaidStatus is CANCEL, do not set DueDate.  If ROLL_CAT=SEC, set isSecd=1.
 * 01/24/2017 Modify TC_ParseTaxBase() to set isSecd & isSupp based on ROLL_CAT.  Also check
 *            date format amd use correct formating template. Set default date based in bill type (sec or supp).
 * 01/27/2017 Add bInclTaxRate to include TaxRate in Agency table.  Modify Load_TC() to use
 *            external Agency table via LoadTaxCodeTable().
 * 02/08/2017 Modify TC_ParseTaxBase() to update DEF_DATE & RED_DATE to Delq record.
 * 02/10/2017 Add updateTaxDelq()
 * 02/14/2017 Rename updateTaxDelq() to updateDelqFlag().
 * 02/16/2017 Modify TC_ParseTaxDist() to remove TaxAmt from Agency table since this is not unique (SOL).
 * 02/17/2017 Modify Load_TC() to get TaxYear from INI file if not found in TC file.
 * 03/03/2017 Modify Tax_CreateAgencyCsv(), Tax_CreateDetailCsv(), TC_ParseTaxDist(), LoadTaxCodeTable() to add TC_Flag.
 * 03/04/2017 Add findExactTaxDist() in addition to findTaxDist()
 * 03/07/2017 Add new version of ChkDueDate().  Add Add Inst1Status & Inst2Status (MEN)
 *            to replace PaidStatus.
 * 03/08/2017 Modify TC_ParseTaxBase() to populate DueDate only if tax has not been paid.
 * 03/09/2017 Modify Tax_CreateTaxBaseCsv() to add inst1Status, inst2Status, billType to taxBase.
 * 03/13/2017 Fix potential error in Tax_CreateAgencyCsv() & Tax_CreateDetailCsv()
 * 03/15/2017 Fix TC_LoadTaxBaseAndDelq() that first record is missing.
 *            Modify TC_ParseTaxDist() to use phone number from TaxDist file.
 *            Modify sort command in TC_LoadTaxAgencyAndDetail() to dupout on Agency only.
 * 03/16/2017 Modify TC_ParseTaxBase() to insert due date if bill was paid.
 * 03/22/2017 Modify Tax_CreateTaxBaseCsv() to add tb_altApn
 * 03/24/2017 Modify Load_TC() to check for new file before processing.  Add isNewTaxFile()
 * 03/26/2017 Fix bug in isNewTaxFile(), add iNewCodeIdx.  Modify TC_ParseTaxDist() to 
 *            auto add new Agency into table.  Modify TC_LoadTaxAgencyAndDetail() to allow
 *            caller option to ignore item without taxamt.
 * 04/02/2017 Modify TC_LoadTaxBaseAndDelq() to update TRA using R01 file.
 *            Add OpenR01() and UpdateTRA().
 * 04/03/2017 Fix bug in Tax_CreatePaidCsv().  Remove back slash from agency name.
 *            Modify isNewTaxFile() to allow passing file date as params.
 * 04/09/2017 Remove Delq file if it's too smale (<10 bytes)
 * 04/13/2017 Modify TC_ParseTaxDist() to support future adding fields in district file.
 * 04/16/2017 Add doTaxImpExt() & doTaxPrepExt() to support update on multiple server automatically
 *            Modify updateDelqFlag() & update_TI_BillNum() to support multiple servers update
 * 04/19/2017 Add TC_ParseTaxBase_S1(), TC_ParseTaxDist_S1() & TC_ParseTaxBase_S2() to support SOL & TUL
 * 04/25/2017 Modify TC_ParseTaxBase() to set Inst1Status=PAID if payment2 is paid.  Also 
 *            set Upd_Date using TC_UPD_DATE if available.
 * 04/26/2017 Fix TC_ParseTaxBase() since the format of TC_UPD_DATE was changed to mm/dd/yyyy
 *
 **************************************************************************/

#include "stdafx.h"
#include "Logs.h"
#include "Utils.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "doSort.h"
#include "R01.h"

#define  _TAX_CPP    1
#include "Tax.h"

extern char acIniFile[], sImportLogTmpl[], sTaxOutTmpl[], acTmpPath[];
extern int  execSqlCmd(LPCTSTR strCmd, hlAdo *phDb=NULL);
extern char *apTokens[], cDelim;
extern int  iTokens, iApnLen, iRecLen;
extern long lLastTaxFileDate, lLienYear;

#define  MAX_TAX_DISTRICT      4800
TAXAGENCY asTaxDist[MAX_TAX_DISTRICT];
int       iDateFmt, iNewCodeIdx;
bool      m_bUseFeePrcl;

/**************************** TC_SetDateFmt **********************************
 *
 *
 *****************************************************************************/

void TC_SetDateFmt(int iFmt)
{
   if (iFmt > 0)
      iDateFmt = iFmt;
}

/**************************** Tax_CreateTaxOwner ***************************
 *
 *
 *****************************************************************************/

char *Tax_CreateTaxOwnerCsv(char *pOutbuf, TAXOWNER *pInrec)
{
   sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
      pInrec->Apn,pInrec->BillNum,pInrec->TaxYear,pInrec->Name1,pInrec->Name2,pInrec->CareOf,pInrec->Dba,
      pInrec->MailAdr[0],pInrec->MailAdr[1],pInrec->MailAdr[2],pInrec->MailAdr[3]);

   return pOutbuf;
}

/**************************** Tax_CreateTaxBaseCsv ***************************
 *
 * If BillNum is not available, try Assmnt_No
 *
 *****************************************************************************/

char *Tax_CreateTaxBaseCsv(char *pOutbuf, TAXBASE *pInrec)
{
   if (pInrec->Upd_Date[0] < '1')
      sprintf(pInrec->Upd_Date, "%d", lLastTaxFileDate);

   if (pInrec->BillNum[0] > ' ')
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->BillNum,pInrec->TRA,pInrec->TaxYear,pInrec->DelqYear,pInrec->TaxAmt1,pInrec->TaxAmt2,
         pInrec->PenAmt1,pInrec->PenAmt2,pInrec->PaidAmt1,pInrec->PaidAmt2,
         pInrec->DueDate1,pInrec->DueDate2,pInrec->PaidDate1,pInrec->PaidDate2,
         pInrec->TotalTaxAmt,pInrec->TotalDue,pInrec->TotalFees,pInrec->TotalRate,
         pInrec->isDelq,pInrec->isSecd,pInrec->isSupp, pInrec->Upd_Date,
         pInrec->Inst1Status,pInrec->Inst2Status,pInrec->BillType,pInrec->Assmnt_No);
   else
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->Assmnt_No,pInrec->TRA,pInrec->TaxYear,pInrec->DelqYear,pInrec->TaxAmt1,pInrec->TaxAmt2,
         pInrec->PenAmt1,pInrec->PenAmt2,pInrec->PaidAmt1,pInrec->PaidAmt2,
         pInrec->DueDate1,pInrec->DueDate2,pInrec->PaidDate1,pInrec->PaidDate2,
         pInrec->TotalTaxAmt,pInrec->TotalDue,pInrec->TotalFees,pInrec->TotalRate,
         pInrec->isDelq,pInrec->isSecd,pInrec->isSupp, pInrec->Upd_Date,
         pInrec->Inst1Status,pInrec->Inst2Status,pInrec->BillType,pInrec->Assmnt_No);

   return pOutbuf;
}

/******************************** Tax_CreateDelqCsv **************************
 *
 * If DefaultNo is available, use it.  Otherwise use BillNum
 *
 *****************************************************************************/

char *Tax_CreateDelqCsv(char *pOutbuf, TAXDELQ *pInrec)
{
   if (pInrec->Default_No[0] > ' ')
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->Default_No,pInrec->TaxYear,pInrec->Def_Date,pInrec->Def_Amt,
         pInrec->Red_Date,pInrec->Red_Amt,pInrec->Upd_Date,pInrec->PrclType,pInrec->InstDel,pInrec->DelqStatus,
         pInrec->isDelq,pInrec->isSupp);
   else
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->BillNum,pInrec->TaxYear,pInrec->Def_Date,pInrec->Def_Amt,
         pInrec->Red_Date,pInrec->Red_Amt,pInrec->Upd_Date,pInrec->PrclType,pInrec->InstDel,pInrec->DelqStatus,
         pInrec->isDelq,pInrec->isSupp);

   return pOutbuf;
}

/******************************** Tax_CreateSuppCsv **************************
 *
 *
 *****************************************************************************/

char *Tax_CreateSuppCsv(char *pOutbuf, TAXSUPP *pInrec)
{   
   sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
      pInrec->Apn,pInrec->ApnSeq,pInrec->CurPri,pInrec->Prorate,pInrec->BillNum,
      pInrec->RollYear,pInrec->RollFctr,pInrec->NetAmt,pInrec->PenAmt,
      pInrec->Status1,pInrec->TaxAmt1,pInrec->PenAmt1,pInrec->DelqDate1,pInrec->PaidDate1,
      pInrec->Status2,pInrec->TaxAmt2,pInrec->PenAmt2,pInrec->DelqDate2,pInrec->PaidDate2,
      pInrec->NoteDate,pInrec->EventDate,pInrec->Type,pInrec->RollSupp);
   
   return pOutbuf;
}

/****************************** Tax_CreateAgencyCsv **************************
 *
 *
 *****************************************************************************/

char *Tax_CreateAgencyCsv(char *pOutbuf, TAXAGENCY *pInrec)
{
   char  TC_Flag[2];

   // Default
   if (pInrec->TC_Flag[0])
      TC_Flag[0] = pInrec->TC_Flag[0];
   else
      TC_Flag[0] = '0';

   TC_Flag[1] = 0;

   sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s\n",
      pInrec->Code,_strupr(pInrec->Agency),pInrec->Phone,pInrec->TaxRate,pInrec->TaxAmt,TC_Flag);

   return pOutbuf;
}

/**************************** Tax_CreateDetailCsv ****************************
 *
 * If BillNum is not available, try Assmnt_No
 * Return 0 if success
 *
 *****************************************************************************/

char *Tax_CreateDetailCsv(char *pOutbuf, TAXDETAIL *pInrec)
{
   char  TC_Flag[2];

   // Default
   if (pInrec->TC_Flag[0])
      TC_Flag[0] = pInrec->TC_Flag[0];
   else
      TC_Flag[0] = '0';

   TC_Flag[1] = 0;
   if (pInrec->BillNum[0] > ' ')
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->BillNum,pInrec->TaxYear,pInrec->TaxAmt,pInrec->TaxCode,pInrec->TaxRate,TC_Flag);
   else
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->Assmnt_No,pInrec->TaxYear,pInrec->TaxAmt,pInrec->TaxCode,pInrec->TaxRate,TC_Flag);
   return pOutbuf;
}

/******************************** Tax_CreatePaidCsv **************************
 *
 *
 *****************************************************************************/

char *Tax_CreatePaidCsv(char *pOutbuf, TAXPAID *pInrec)
{
   if (pInrec->Default_No[0] > ' ')
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->Default_No,pInrec->PaidDate,pInrec->PaidAmt,pInrec->TaxAmt,
         pInrec->PenAmt,pInrec->FeeAmt,pInrec->PaidFlg);
   else
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->BillNum,pInrec->PaidDate,pInrec->PaidAmt,pInrec->TaxAmt,
         pInrec->PenAmt,pInrec->FeeAmt,pInrec->PaidFlg);

   return pOutbuf;
}

/******************************** updateTaxDB *******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * Return TRUE if success
 *
 ****************************************************************************/

int updateTaxDB(char *strCmd)
{                 
   static bool  bConnected=false;
   static hlAdo hDB;

   char acServer[256];
   int  iRet;

   iRet = GetIniString("Database", "TaxProvider", "", acServer, 128, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing TaxProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   LogMsg("Execute command on: %s", acServer);
   LogMsg(strCmd);
   try
   {
      // open the database connection
      if (!bConnected)
         bConnected = hDB.Connect(acServer);
   } catch(_com_error &e)
   {
      LogMsg("%s", ComError(e));
      return -2;
   }

   // Update county profile table
   iRet = execSqlCmd(strCmd, &hDB);
   return iRet;
}

int updTaxDB(char *strCmd, char*pServerName)
{                 
   static hlAdo hDB;

   char acServer[256], acTmp[256];
   int  iRet;
   bool bConnected;

   iRet = GetIniString("Database", "TaxProvider", "", acServer, 128, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing TaxProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   if (pServerName && *pServerName)
   {
      sprintf(acTmp, acServer, pServerName);
      strcpy(acServer, acTmp);
      bConnected = false;
   } else
      bConnected = true;


   LogMsg("Execute command on: %s", acServer);
   LogMsg(strCmd);
   try
   {
      // open the database connection
      if (!bConnected)
      {
         LogMsg("Connect to DB server: %s", pServerName);
         bConnected = hDB.Connect(acServer);
      }
   } catch(_com_error &e)
   {
      LogMsg("%s", ComError(e));
      return -2;
   }

   // Update county profile table
   iRet = execSqlCmd(strCmd, &hDB);
   return iRet;
}

/******************************** doTaxImport *******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * iType values: see tax.h
 *
 * Return 0 if success
 *
 ****************************************************************************/

int importTaxFile(char *pCnty, char *pType, char *pServerName)
{
   char sInFile[128], sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], acTmp[1024];
   int  iRet=1;

   GetIniString("Data", "TaxImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
   sprintf(sFmtFile, sImportTmpl, pType);
   sprintf(sInFile, sTaxOutTmpl, pCnty, pCnty, pType);
   sprintf(sErrFile, sImportLogTmpl, "Tax", pCnty, pType);
   sprintf(sTblName, "%s_Tax_%s", pCnty, pType);

   LogMsg("Import %s table", sTblName);
   if (!_access(sErrFile, 0))
      DeleteFile(sErrFile);
   sprintf(acTmp, "%s.Error.txt", sErrFile);
   if (!_access(acTmp, 0))
      DeleteFile(acTmp);

   if (_access(sInFile, 0))
   {
      LogMsg("***** Missing import file: %s", sInFile);
      return -1;
   }
   if (_access(sFmtFile, 0))
   {
      LogMsg("***** Missing import format file: %s", sFmtFile);
      return -1;
   }

   // Prepare to import - Create/truncate table
   sprintf(acTmp, "EXEC [dbo].[spPrep_Tax%s] '%s'", pType, pCnty);
   //iRet = updateTaxDB(acTmp);
   iRet = updTaxDB(acTmp, pServerName);

   if (!iRet)
   {
      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sInFile, sFmtFile, sErrFile);
      iRet = updTaxDB(acTmp, NULL);
   }

   return iRet;
}

int doTaxImpExt(char *pCnty, int iType, char *pServerName)
{
   int  iRet=1;

   //LogMsg("Import Tax data to: %s", pCnty);
   switch (iType)
   {
      case TAX_OWNER:
         iRet = importTaxFile(pCnty, "Owner", pServerName);
         break;

      case TAX_BASE:
         iRet = importTaxFile(pCnty, "Base", pServerName);
         break;

      case TAX_DETAIL:
         iRet = importTaxFile(pCnty, "Items", pServerName);
         break;

      case TAX_AGENCY:
         iRet = importTaxFile(pCnty, "Agency", pServerName);
         break;

      case TAX_DELINQUENT:
         iRet = importTaxFile(pCnty, "Delq", pServerName);
         break;
 
      case TAX_SUPPLEMENT:
         iRet = importTaxFile(pCnty, "Supp", pServerName);
         break;

      case TAX_PAID:
         iRet = importTaxFile(pCnty, "Paid", pServerName);
         break;

      default:
         LogMsg("*** Import file type [%d] has not been implemented.  Please check calling function", iType);
         break;
   }

   return iRet;
}

/*********************************** doTaxImport ***********************************
 *
 * Support multiple server update.  Server name can be defined in INI file
 *
 ***********************************************************************************/

int doTaxImport(char *pCnty, int iType)
{
   int   iRet;
   char  acServerName[64];

   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = doTaxImpExt(pCnty, iType, acServerName);

   iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = doTaxImpExt(pCnty, iType, acServerName);

   iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = doTaxImpExt(pCnty, iType, acServerName);

   return iRet;
}

/************************************ doTaxPrep ************************************
 *
 * Create empty table
 *
 ***********************************************************************************/

int doTaxPrepExt(char *pCnty, int iType, char *pServerName)
{
   int  iRet=1;
   char  acTmp[64], acType[16];

   acTmp[0] = 0;
   switch (iType)
   {
      case TAX_OWNER:
         strcpy(acType, "Owner");
         break;

      case TAX_BASE:
         strcpy(acType, "Base");
         break;

      case TAX_DETAIL:
         strcpy(acType, "Items");
         break;

      case TAX_AGENCY:
         strcpy(acType, "Agency");
         break;

      case TAX_DELINQUENT:
         strcpy(acType, "Delq");
         break;
 
      case TAX_SUPPLEMENT:
         strcpy(acType, "Supp");
         break;

      case TAX_PAID:
         strcpy(acType, "Paid");
         break;

      default:
         acType[0] = 0;
         LogMsg("*** Table type [%d] has not been implemented.  Please check calling function", iType);
         break;
   }

   if (acType[0] > ' ')
   {
      LogMsg("Create empty table %s_Tax_%s", pCnty, acType);
      sprintf(acTmp, "EXEC [dbo].[spPrep_Tax%s] '%s'", acType, pCnty);
      iRet = updTaxDB(acTmp, pServerName);
   }

   return iRet;
}

int doTaxPrep(char *pCnty, int iType)
{
   int   iRet;
   char  acServerName[64];

   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = doTaxPrepExt(pCnty, iType, acServerName);

   iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = doTaxPrepExt(pCnty, iType, acServerName);

   iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = doTaxPrepExt(pCnty, iType, acServerName);

   return iRet;
}

/*************************** TC_ParseTaxBase() ********************************
 *
 * DUE_PAID_DATE1 & DUE_PAID_DATE2 may have different format.  Watch out
 *
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int TC_ParseTaxBase(char *pOutbuf, char *pInbuf, char cDelim)
{
   char     acTmp[256];
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_NET_VAL)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));
#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "24201130060000", 12))
   //   iTmp = 0;
#endif
   
   if (!memcmp(apTokens[TC_ROLL_CAT], "SEC", 3))
   {
      pOutBase->isSecd[0] = '1';
      pOutBase->BillType[0] = BILLTYPE_SECURED;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSupp[0] = '1';
      pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
   } else
   {
      pOutBase->isSecd[0] = '0';
      pOutBase->isSupp[0] = '0';
   }

   // Verify date format
   if (*(apTokens[TC_DUE_PAID_DT2]+2) == '/')
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = iDateFmt;

   if (!memcmp(apTokens[TC_ROLL_CAT], "DEL", 3))
   {
      int iDelqYear;

      // Delq Year
      iDelqYear = atol(apTokens[TC_TAX_YR]);
      if (iDelqYear < 1900)
      {
         LogMsg("*** WARNING: No tax data APN=%s", apTokens[TC_APN_S]);
         return -2;
      }

      if (iTokens > TC_DEF_DATE && *apTokens[TC_DEF_DATE] > ' ')
         strcpy(pOutDelq->Def_Date, apTokens[TC_DEF_DATE]);
      else
         sprintf(pOutDelq->Def_Date, "%d", iDelqYear);

      // APN
      strcpy(pOutDelq->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutDelq->Default_No, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
         strcpy(pOutDelq->BillNum, apTokens[TC_FEE_PCL]);

      // Default amt
      dTaxTotal = atof(apTokens[TC_TOTAL_DUE]);
      if (dTaxTotal > 0.0)
      {
         iTmp = sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
         pOutDelq->isDelq[0] = '1';
         if (iTokens >= TC_DEF_DATE)
         {
            if (*apTokens[TC_DEF_DATE] >= '0' && dateConversion(apTokens[TC_DEF_DATE], acTmp, iFmt))
               strcpy(pOutDelq->Def_Date, acTmp);
         }

         // Paid amt
         dTmp = atof(apTokens[TC_TOTAL_PAID]);
         if (dTmp > 0.0)
         {
            iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            if (dTaxTotal == dTmp)
            {
               pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
               pOutDelq->isDelq[0] = '0';
               if (iTokens >= TC_DEF_DATE)
               {
                  if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
                     strcpy(pOutDelq->Red_Date, acTmp);
               }
            } else if (dTaxTotal > dTmp)
               pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
         } else
         {
            pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year
      iTaxYear = atol(apTokens[TC_TAX_YR]);
      if (iTaxYear < 1900)
      {
         LogMsg("*** WARNING: No tax base data APN=%s", apTokens[TC_APN_S]);
         return -2;
      }
      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // APN
      strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutBase->BillNum, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
      {
         if (pOutBase->isSecd[0] == '1')
            sprintf(pOutBase->BillNum, "%s-1", apTokens[TC_APN_S]);
         else
            sprintf(pOutBase->BillNum, "%s-2", apTokens[TC_APN_S]);
      }

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
         sprintf(pOutBase->TRA, "%0.6d", iTmp);

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > '0')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > '0')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);

      if (dTax1 > 0.0)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > '0')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      if (dTaxTotal > 0.0)
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // Paid/Due - SAC: PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);
         else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax2)                                         // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         else if (pOutBase->isSupp[0] == '1')
            strcpy(pOutBase->DueDate2, "20170630");
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/')
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         else if (pOutBase->isSupp[0] == '1')
            strcpy(pOutBase->DueDate1, "20170228");
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         if (ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   return iRet;
}

/*************************** TC_ParseTaxBase() ********************************
 *
 * Special version for SOL where we generate BillNum using APN and occurence number
 *
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int TC_ParseTaxBase_S1(char *pOutbuf, char *pInbuf, char cDelim)
{
   static   char sLastApn[20];
   static   int  iCnt=0;

   char     acTmp[256];
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_NET_VAL)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));
#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "0052455070", 10))
   //   iTmp = 0;
#endif
   
   if (!memcmp(apTokens[TC_ROLL_CAT], "SEC", 3))
   {
      pOutBase->isSecd[0] = '1';
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSupp[0] = '1';
   } else if (*apTokens[TC_ROLL_CAT] < '0' || *apTokens[TC_TAX_YR] < '0')
   {
      return -2;
   }

   // Tax Year
   iTaxYear = atol(apTokens[TC_TAX_YR]);
   if (iTaxYear < 1900)
   {
      LogMsg("*** WARNING: Bad tax year APN=%s.   Ignore!", apTokens[TC_APN_S]);
      return -2;
   }

   // Verify date format
   if (*(apTokens[TC_DUE_PAID_DT2]+2) == '/')
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = iDateFmt;

   // APN
   strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

   // Check for DELQ
   if (!memcmp(apTokens[TC_PAID_STAT1], "DEL", 3) && iTaxYear < lLienYear)
   {
      if (iTokens >= TC_DEF_DATE && *apTokens[TC_DEF_DATE] > ' ' && dateConversion(apTokens[TC_DEF_DATE], acTmp, iFmt))
         strcpy(pOutDelq->Def_Date, acTmp);
      else
         sprintf(pOutDelq->Def_Date, "%d", iTaxYear);

      // Default amt
      dTaxTotal = atof(apTokens[TC_TOTAL_DUE]);
      if (dTaxTotal > 0.0)
      {
         iTmp = sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
         pOutDelq->isDelq[0] = '1';

         // Paid amt
         dTmp = atof(apTokens[TC_TOTAL_PAID]);
         if (dTmp > 0.0)
         {
            iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            if (dTaxTotal == dTmp)
            {
               pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
               pOutDelq->isDelq[0] = '0';
               if (iTokens >= TC_RED_DATE)
               {
                  if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
                     strcpy(pOutDelq->Red_Date, acTmp);
               }
            } else if (dTaxTotal > dTmp)
               pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
         } else
         {
            pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         }
      }

      iRet = 2;
   } else if (!memcmp(apTokens[TC_PAID_STAT1], "RED", 3))
   {
      pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
      pOutDelq->isDelq[0] = '0';

      if (iTokens >= TC_RED_DATE && *apTokens[TC_RED_DATE] > ' ' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
         strcpy(pOutDelq->Red_Date, acTmp);
      else if (*apTokens[TC_DUE_PAID_DT1] > ' ' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
         strcpy(pOutDelq->Red_Date, acTmp);
            
      if (*apTokens[TC_DEF_DATE] > ' ' && dateConversion(apTokens[TC_DEF_DATE], acTmp, iFmt))
         strcpy(pOutDelq->Def_Date, acTmp);
      //else
      //   sprintf(pOutDelq->TaxYear, "%d", iTaxYear);

      // Paid amt
      dTmp = atof(apTokens[TC_TOTAL_PAID]);
      if (dTmp > 0.0)
      {
         iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
         if (iTokens >= TC_RED_DATE)
         {
            if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
               strcpy(pOutDelq->Red_Date, acTmp);
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year
      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // Save APN
      if (memcmp(sLastApn, pOutBase->Apn, iApnLen))
      {
         strcpy(sLastApn, apTokens[TC_APN_S]);
         iCnt = 1;
      } else
         iCnt++;

      // BillNum
      sprintf(pOutBase->BillNum, "%s-%.2d", sLastApn, iCnt);

      // BillType
      if (pOutBase->isSecd[0] == '1')
         pOutBase->BillType[0] = BILLTYPE_SECURED;
      else if (pOutBase->isSupp[0] == '1')
         pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
      else
         pOutBase->BillType[0] = '?';

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
         sprintf(pOutBase->TRA, "%0.6d", iTmp);

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > ' ')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > ' ')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);

      if (dTax1)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > ' ')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      if (dTaxTotal)
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // INST2 - PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING/REFUND PAID/REFUND DUE
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);
         else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!memcmp(apTokens[TC_PAID_STAT2], "REF", 2))     // Refund
         pOutBase->Inst2Status[0] = TAX_BSTAT_REFUND;
      else if (!dTax2)                                         // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         else if (pOutBase->isSupp[0] == '1')
            strcpy(pOutBase->DueDate2, "20170630");
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/')
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!memcmp(apTokens[TC_PAID_STAT1], "REF", 2))     // Refund
         pOutBase->Inst1Status[0] = TAX_BSTAT_REFUND;
      else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         else if (pOutBase->isSupp[0] == '1')
            strcpy(pOutBase->DueDate1, "20170228");
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         if (ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   return iRet;
}

/*************************** TC_ParseTaxBase() ********************************
 *
 * Special version for TUL where we generate BillNum using APN and occurence number
 * DELQ record has separate TaxAmt, Penalty, and other fee
 *
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int TC_ParseTaxBase_S2(char *pOutbuf, char *pInbuf, char cDelim)
{
   static   char sLastApn[20];
   static   int  iCnt=0;

   char     acTmp[256];
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_NET_VAL)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));
#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "0052455070", 10))
   //   iTmp = 0;
#endif
   
   if (!memcmp(apTokens[TC_ROLL_CAT], "SEC", 3))
   {
      pOutBase->isSecd[0] = '1';
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSupp[0] = '1';
   } else if (*apTokens[TC_ROLL_CAT] < '0' || *apTokens[TC_TAX_YR] < '0')
   {
      return -2;
   }

   // Tax Year
   iTaxYear = atol(apTokens[TC_TAX_YR]);
   if (iTaxYear < 1900)
   {
      LogMsg("*** WARNING: Bad tax year APN=%s.   Ignore!", apTokens[TC_APN_S]);
      return -2;
   }

   // Verify date format
   if (*(apTokens[TC_DUE_PAID_DT2]+2) == '/')
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = iDateFmt;

   // APN
   strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

   // Check for DELQ
   if (!memcmp(apTokens[TC_PAID_STAT1], "DEL", 3) && iTaxYear < lLienYear)
   {
      if (iTokens >= TC_DEF_DATE && *apTokens[TC_DEF_DATE] > ' ' && dateConversion(apTokens[TC_DEF_DATE], acTmp, iFmt))
         strcpy(pOutDelq->Def_Date, acTmp);
      else
         sprintf(pOutDelq->Def_Date, "%d", iTaxYear);

      // Default amt
      dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      if (dTaxTotal > 0.0)
      {
         iTmp = sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
         pOutDelq->isDelq[0] = '1';

         // Total Penalty
         dTmp = atof(apTokens[TC_TOTAL_PEN]);
         if (dTmp > 0.0)
            sprintf(pOutDelq->Pen_Amt, "%.2f", dTmp);

         // Other fee
         dTmp = atof(apTokens[TC_OTHER_FEES]);
         if (dTmp > 0.0)
            sprintf(pOutDelq->Fee_Amt, "%.2f", dTmp);

         // Total Due
         dTmp = atof(apTokens[TC_TOTAL_DUE]);
         if (dTmp > 0.0)
         {
            sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         }

         // Paid amt
         dTmp = atof(apTokens[TC_TOTAL_PAID]);
         if (dTmp > 0.0)
         {
            iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            if (dTaxTotal == dTmp)
            {
               pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
               pOutDelq->isDelq[0] = '0';
               if (iTokens >= TC_RED_DATE)
               {
                  if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
                     strcpy(pOutDelq->Red_Date, acTmp);
               }
            } else if (dTaxTotal > dTmp)
               pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
         }
      }

      iRet = 2;
   } else if (!memcmp(apTokens[TC_PAID_STAT1], "RED", 3))
   {
      pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
      pOutDelq->isDelq[0] = '0';

      if (iTokens >= TC_RED_DATE && *apTokens[TC_RED_DATE] > ' ' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
         strcpy(pOutDelq->Red_Date, acTmp);
      else if (*apTokens[TC_DUE_PAID_DT1] > ' ' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
         strcpy(pOutDelq->Red_Date, acTmp);
            
      if (*apTokens[TC_DEF_DATE] > ' ' && dateConversion(apTokens[TC_DEF_DATE], acTmp, iFmt))
         strcpy(pOutDelq->Def_Date, acTmp);
      //else
      //   sprintf(pOutDelq->TaxYear, "%d", iTaxYear);

      // Paid amt
      dTmp = atof(apTokens[TC_TOTAL_PAID]);
      if (dTmp > 0.0)
      {
         iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
         if (iTokens >= TC_RED_DATE)
         {
            if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
               strcpy(pOutDelq->Red_Date, acTmp);
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year
      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // Save APN
      if (memcmp(sLastApn, pOutBase->Apn, iApnLen))
      {
         strcpy(sLastApn, apTokens[TC_APN_S]);
         iCnt = 1;
      } else
         iCnt++;

      // BillNum
      sprintf(pOutBase->BillNum, "%s-%.2d", sLastApn, iCnt);

      // BillType
      if (pOutBase->isSecd[0] == '1')
         pOutBase->BillType[0] = BILLTYPE_SECURED;
      else if (pOutBase->isSupp[0] == '1')
         pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
      else
         pOutBase->BillType[0] = '?';

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
         sprintf(pOutBase->TRA, "%0.6d", iTmp);

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > ' ')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > ' ')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);

      if (dTax1)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > ' ')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      if (dTaxTotal)
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // INST2 - PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING/REFUND PAID/REFUND DUE
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);
         else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!memcmp(apTokens[TC_PAID_STAT2], "REF", 2))     // Refund
         pOutBase->Inst2Status[0] = TAX_BSTAT_REFUND;
      else if (!dTax2)                                         // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         else if (pOutBase->isSupp[0] == '1')
            strcpy(pOutBase->DueDate2, "20170630");
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/')
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!memcmp(apTokens[TC_PAID_STAT1], "REF", 2))     // Refund
         pOutBase->Inst1Status[0] = TAX_BSTAT_REFUND;
      else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         else if (pOutBase->isSupp[0] == '1')
            strcpy(pOutBase->DueDate1, "20170228");
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         if (ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   return iRet;
}

/*************************** TC_LoadTaxBaseAndDelq ****************************
 *
 *
 ******************************************************************************/

int TC_LoadTaxBaseAndDelq(char *pCnty, char *pTaxFile, char *pBaseFile, char *pDelqFile, bool bImport)
{
   char     *pTmp, acBuf[2048], acRec[2048], sDelqYear[8], sApn[32], acR01File[_MAX_PATH];
   int      iRet, iParseType=0;
   long     lBase=0, lDelq=0, lCnt=0;
   FILE     *fdIn, *fdBase, *fdDelq, *fdR01;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acBuf[0];

   // Open input file
   LogMsg("Open Tax file %s", pTaxFile);
   fdIn = fopen(pTaxFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", pTaxFile);
      return -2;
   }  

   // Open R01 file
   iRet = GetIniString("Data", "RawFile", "", acRec, 128, acIniFile);
   sprintf(acR01File, acRec, pCnty, pCnty, "R01");
   if (_access(acR01File, 0))
      sprintf(acR01File, acRec, pCnty, pCnty, "S01");
   try {
      fdR01 = fopen(acR01File, "rb");
   } catch (...)
   {
      LogMsg("***** Error opening R01 file: %s", acR01File);
      fdR01 = NULL;
   }

   // Open Base Output file
   LogMsg("Open Base output file %s", pBaseFile);
   fdBase = fopen(pBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", pBaseFile);
      return -3;
   }

   // Open Delq Output file
   LogMsg("Open Delq output file %s", pDelqFile);
   fdDelq = fopen(pDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", pDelqFile);
      return -4;
   }

   // Set which parser to use
   if (!_memicmp(pCnty, "SOL", 3))
      iParseType = 1;
   else if (!_memicmp(pCnty, "TUL", 3))
      iParseType = 2;

   // Skip header record - file is sorted, no header to skip
   //pTmp = fgets((char *)&acRec[0], 2048, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 2048, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

#ifdef _DEBUG
      //if (!memcmp(acRec, "140680110", 9))
      //   iRet = 0;
#endif

      // Create new TAXBASE record
      if (iParseType == 1)    // SOL
         iRet = TC_ParseTaxBase_S1(acBuf, acRec, '|');
      else if (iParseType == 2)
         iRet = TC_ParseTaxBase_S2(acBuf, acRec, '|');
      else
         iRet = TC_ParseTaxBase(acBuf, acRec, '|');

      if (iRet == 1)
      {
         // Update TRA if not there yet
         if (pTaxBase->TRA[0] < '0' && fdR01)
            UpdateTRA(acBuf, fdR01);

         if (!strcmp(sApn, pTaxBase->Apn))
         {
            strcpy(pTaxBase->DelqYear, sDelqYear);
            pTaxBase->isDelq[0] = '1';
            sDelqYear[0] = 0;
         }
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         fputs(acRec, fdBase);
         lBase++;
         sApn[0] = 0;
      } else if (iRet == 2)
      {
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf);
         fputs(acRec, fdDelq);
         lDelq++;

         // Save DelqYear
         strcpy(sDelqYear, pTaxDelq->Def_Date);
         strcpy(sApn, pTaxDelq->Apn);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDelq)
      fclose(fdDelq);
   if (fdR01)
      fclose(fdR01);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total Base output:       %u", lBase);
   LogMsg("Total Delq output:       %u", lDelq);

   printf("\nTotal output records: %u", lCnt);

   return 0;
}

/*************************** TC_ParseTaxDist() ********************************
 *
 *
 ******************************************************************************/

int TC_ParseTaxDist(char *pTaxDetail, char *pTaxAgency, char *pInbuf, char cDelim)
{
   int      iTmp;
   double	dTax, dRate;

   TAXDETAIL *pOutDetail = (TAXDETAIL *)pTaxDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)pTaxAgency;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < TC_TAX_BILLNUM)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pTaxDetail, 0, sizeof(TAXDETAIL));
   memset(pTaxAgency, 0, sizeof(TAXAGENCY));

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "002061440", 9))
   //   iTmp = 0;
#endif
   
   // APN
   strcpy(pOutDetail->Apn, apTokens[TC_APN_S]);

   // BillNum
   if (*apTokens[TC_TAX_BILLNUM] >= '0')
      strcpy(pOutDetail->BillNum, remCrLf(apTokens[TC_TAX_BILLNUM]));

   // Tax Year
   sprintf(pOutDetail->TaxYear, "%d", iTaxYear);

   // Tax amount
   dTax = atof(apTokens[TC_TAX_TOTAL]);
   //if (!dTax)
   //   return 1;

   sprintf(pOutDetail->TaxAmt, "%.2f", dTax);

   // Tax Rate - In SOL, FIXED amt known to be varied by community
   //if (!_memicmp(apTokens[TC_TAX_RATE], "FIX", 3))
   //   strcpy(pOutAgency->TaxAmt, pOutDetail->TaxAmt);
   //else 
   if (*apTokens[TC_TAX_RATE] > ' ')
   {
      dRate = atof(apTokens[TC_TAX_RATE]);
      if (dRate > 0.0)
      {
         strcpy(pOutDetail->TaxRate, apTokens[TC_TAX_RATE]);
         if (bInclTaxRate)
            strcpy(pOutAgency->TaxRate, apTokens[TC_TAX_RATE]);
      }
   }

   // Tax Agency - remove special char
   iTmp = remChar(apTokens[TC_TAX_DIST], '\\');
   strcpy(pOutDetail->TaxDesc, apTokens[TC_TAX_DIST]);
   strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);

   // Tax code
   if (iNumTaxDist > 1)
   {
      replStrAll(apTokens[TC_TAX_DIST], "&QUOT;", "\"");
      replStr(apTokens[TC_TAX_DIST], "&#39;", "'");
      TAXAGENCY *pAgency = findTaxDist(apTokens[TC_TAX_DIST]);
      if (pAgency)
      {
         strcpy(pOutDetail->TaxCode, pAgency->Code);
         strcpy(pOutAgency->Code, pAgency->Code);
         strcpy(pOutAgency->Agency, pAgency->Agency);
         strcpy(pOutAgency->Phone, pAgency->Phone);
         pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
         pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
      } else
      {
         char sDesc[256], sCode[32];

         // Update dist table
         asTaxDist[iNumTaxDist].iCodeLen = sprintf(sCode, "X%.3d", ++iNewCodeIdx);
         asTaxDist[iNumTaxDist].iNameLen = sprintf(sDesc, "%s", apTokens[TC_TAX_DIST]);
         strcpy(asTaxDist[iNumTaxDist].Agency, sDesc);
         strcpy(asTaxDist[iNumTaxDist].Code, sCode);
         strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
         asTaxDist[iNumTaxDist].TC_Flag[0] = '1';
         iNumTaxDist++;

         strcpy(pOutAgency->Agency, sDesc);
         strcpy(pOutAgency->Code, sCode);
         pOutAgency->TC_Flag[0] = '1';
         LogMsg("*** New Tax Agency: %s", apTokens[TC_TAX_DIST]);
      }
   } else
   {
      strcpy(pOutDetail->TaxCode, apTokens[TC_TAX_CODE]);
      strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
      pOutAgency->TC_Flag[0] = '0';
      pOutDetail->TC_Flag[0] = '0';
   }

   if (*apTokens[TC_TAX_PHONE] > ' ' && pOutAgency->Phone[0] <= ' ')
      strcpy(pOutAgency->Phone, apTokens[TC_TAX_PHONE]);

   return 0;
}

int TC_ParseTaxDist_S1(char *pTaxDetail, char *pTaxAgency, char *pInbuf, char cDelim)
{
   static   char  sLastRollCat[32];
   static   int   iCnt=0;

   int      iTmp;
   double	dTax, dRate;

   TAXDETAIL *pOutDetail = (TAXDETAIL *)pTaxDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)pTaxAgency;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < TC_TAX_BILLNUM)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pTaxDetail, 0, sizeof(TAXDETAIL));
   memset(pTaxAgency, 0, sizeof(TAXAGENCY));

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "002061440", 9))
   //   iTmp = 0;
#endif
   
   // APN
   strcpy(pOutDetail->Apn, apTokens[TC_APN_S]);
   
   // BillNum
   if (*apTokens[TC_TAX_BILLNUM] >= '0')
      strcpy(pOutDetail->BillNum, remCrLf(apTokens[TC_TAX_BILLNUM]));
   else
   {
      if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SUP", 3))
         sprintf(pOutDetail->BillNum, "%s-02", apTokens[TC_APN_S]);
      else
         sprintf(pOutDetail->BillNum, "%s-01", apTokens[TC_APN_S]);
   }

   // Tax Year
   sprintf(pOutDetail->TaxYear, "%d", iTaxYear);

   // Tax amount
   dTax = atof(apTokens[TC_TAX_TOTAL]);
   //if (!dTax)
   //   return 1;

   sprintf(pOutDetail->TaxAmt, "%.2f", dTax);

   // Tax Rate - In SOL, FIXED amt known to be varied by community
   //if (!_memicmp(apTokens[TC_TAX_RATE], "FIX", 3))
   //   strcpy(pOutAgency->TaxAmt, pOutDetail->TaxAmt);
   //else 
   if (*apTokens[TC_TAX_RATE] > ' ')
   {
      dRate = atof(apTokens[TC_TAX_RATE]);
      if (dRate > 0.0)
      {
         strcpy(pOutDetail->TaxRate, apTokens[TC_TAX_RATE]);
         if (bInclTaxRate)
            strcpy(pOutAgency->TaxRate, apTokens[TC_TAX_RATE]);
      }
   }

   // Tax Agency - remove special char
   iTmp = remChar(apTokens[TC_TAX_DIST], '\\');
   strcpy(pOutDetail->TaxDesc, apTokens[TC_TAX_DIST]);
   strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);

   // Tax code
   if (iNumTaxDist > 1)
   {
      replStrAll(apTokens[TC_TAX_DIST], "&QUOT;", "\"");
      replStr(apTokens[TC_TAX_DIST], "&#39;", "'");
      TAXAGENCY *pAgency = findTaxDist(apTokens[TC_TAX_DIST]);
      if (pAgency)
      {
         strcpy(pOutDetail->TaxCode, pAgency->Code);
         strcpy(pOutAgency->Code, pAgency->Code);
         strcpy(pOutAgency->Agency, pAgency->Agency);
         strcpy(pOutAgency->Phone, pAgency->Phone);
         pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
         pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
      } else
      {
         char sDesc[256], sCode[32];

         // Update dist table
         asTaxDist[iNumTaxDist].iCodeLen = sprintf(sCode, "X%.3d", ++iNewCodeIdx);
         asTaxDist[iNumTaxDist].iNameLen = sprintf(sDesc, "%s", apTokens[TC_TAX_DIST]);
         strcpy(asTaxDist[iNumTaxDist].Agency, sDesc);
         strcpy(asTaxDist[iNumTaxDist].Code, sCode);
         strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
         asTaxDist[iNumTaxDist].TC_Flag[0] = '1';
         iNumTaxDist++;

         strcpy(pOutAgency->Agency, sDesc);
         strcpy(pOutAgency->Code, sCode);
         pOutAgency->TC_Flag[0] = '1';
         LogMsg("*** New Tax Agency: %s", apTokens[TC_TAX_DIST]);
      }
   } else
   {
      strcpy(pOutDetail->TaxCode, apTokens[TC_TAX_CODE]);
      strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
      pOutAgency->TC_Flag[0] = '0';
      pOutDetail->TC_Flag[0] = '0';
   }

   if (*apTokens[TC_TAX_PHONE] > ' ' && pOutAgency->Phone[0] <= ' ')
      strcpy(pOutAgency->Phone, apTokens[TC_TAX_PHONE]);

   return 0;
}

/*************************** Load_TaxAgencyAndDetail() ************************
 *
 * Use ???DIST.TC to load Tax_Agency, Tax_Detail
 *
 ******************************************************************************/

int TC_LoadTaxAgencyAndDetail(char *pCnty, char *pDistFile, char *pAgencyFile, char *pDetailFile, bool bImport, bool bIgnoreNoValue)
{
   char     *pTmp, acTmpAgency[256], acAgency[1024], acDetail[1024], acRec[1024];
   int      iRet, iParseType=0;
   long     lOut=0, lCnt=0;
   FILE     *fdIn, *fdDetail, *fdAgency;
   TAXDETAIL *pDetail = (TAXDETAIL *)acDetail;

   // Open input file
   LogMsg("Open DIST file %s", pDistFile);
   fdIn = fopen(pDistFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening Tax file: %s\n", pDistFile);
      return -2;
   }  

   // Open Detail output
   LogMsg("Open Detail file %s", pDetailFile);
   fdDetail = fopen(pDetailFile, "w");
   if (!fdDetail)
   {
      LogMsg("***** Error creating Detail file: %s\n", pDetailFile);
      return -4;
   }

   // Open Agency output
   sprintf(acTmpAgency, "%s\\%s\\%s_Agency.tmp", acTmpPath, pCnty, pCnty);
   LogMsg("Open Agency file %s", acTmpAgency);
   fdAgency = fopen(acTmpAgency, "w");
   if (!fdAgency)
   {
      LogMsg("***** Error creating temp Agency file: %s\n", acTmpAgency);
      return -4;
   }

   // Set which parser to use
   if (!_memicmp(pCnty, "SOL", 3) || !_memicmp(pCnty, "TUL", 3))
      iParseType = 1;

   // Skip header record
   pTmp = fgets((char *)&acRec[0], 1024, fdIn);
   iNewCodeIdx = 0;

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 1024, fdIn);
      if (!pTmp)
         break;

#ifdef _DEBUG
      //if (!memcmp(acRec, "002062030", 9))
      //   iRet = 0;
#endif

      // Create new Detail record
      if (iParseType == 1)
         iRet = TC_ParseTaxDist_S1(acDetail, acAgency, acRec, '|');
      else
         iRet = TC_ParseTaxDist(acDetail, acAgency, acRec, '|');
      if (!iRet)
      {
         if (pDetail->TaxAmt[0] > '0' || !bIgnoreNoValue)
         {
            // Create delimited record
            Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acDetail);
            fputs(acRec, fdDetail);

            Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgency);
            fputs(acRec, fdAgency);
            lOut++;
         }
      } else
      {
         LogMsg("---> Drop detail record %d [%.40s] (%d)", lCnt, acRec, iRet); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:      %u", lCnt);
   LogMsg("Total Detail records output:  %u", lOut);
   printf("\nTotal output records: %u", lCnt);

   // Dedup Agency file
   iRet = sortFile(acTmpAgency, pAgencyFile, "S(#1,C,A,#2,C,A) DUPOUT(#1) F(TXT) DEL(124)");

   return iRet;
}

/******************************** Load_TC() ***********************************
 *
 * Load scrape data from county tax website
 *
 * Use ???.TC to load Tax_Base, Tax_Delq
 *     ???DIST.TC to load Tax_Agency, Tax_Detail
 *     S???.S01 to load Tax_Owner
 *
 *   - CAL,DNX,IMP,MAD,MER,MNO,MON,MRN,NAP,PLA,SAC,SBD,SBT,SBX,SCL,SCR,SFX*,SJX,
 *     SLO,SMX,SOL*,SON,STA,TUL,YOL,YUB
 *
 ******************************************************************************/

int Load_TC(char *pCnty, bool bImport)
{
   char  sTaxFile[_MAX_PATH], sDistFile[_MAX_PATH], sDetailFile[_MAX_PATH], sTmp[_MAX_PATH];
   char  sAgencyFile[_MAX_PATH], sBaseFile[_MAX_PATH], sDelqFile[_MAX_PATH];
   int   iRet;

   LogMsg("Loading TC files for %s", pCnty);

   // Set flag to include tax rate in Agency table
   iRet = GetIniString(pCnty, "TaxRate", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      bInclTaxRate = true;
   else
      bInclTaxRate = false;

   // Get input file names
   GetIniString("Data", "Main_TC", "", sTmp, _MAX_PATH, acIniFile);
   sprintf(sTaxFile, sTmp, pCnty, pCnty);
   if (_access(sTaxFile, 0))
   {
      LogMsg("***** File not found: %s", sTaxFile);
      return -1;
   }

   // Save last file date to update County table
   lLastTaxFileDate = getFileDate(sTaxFile);

   // Only process if new tax file
   iRet = isNewTaxFile(sTaxFile, pCnty);
   if (iRet <= 0)
      return iRet;

   // Load Tax Dist
   iRet = GetIniString(pCnty, "TaxDist", "", sTmp, _MAX_PATH, acIniFile);
   if (iRet > 0)
      iRet = LoadTaxCodeTable(sTmp);

   GetIniString("Data", "Dist_TC", "", sTmp, _MAX_PATH, acIniFile);
   sprintf(sDistFile, sTmp, pCnty, pCnty);
   if (_access(sDistFile, 0))
   {
      LogMsg("***** File not found: %s", sDistFile);
      return -1;
   }

   // Init global
   GetPrivateProfileString(pCnty, "UseFeePrcl", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      m_bUseFeePrcl = true;
   else
      m_bUseFeePrcl = false;

   // Sort input file on APN, TAXYEAR, BILLNUM
   sprintf(sTmp, "%s\\%s\\%s_TC.dat", acTmpPath, pCnty, pCnty);
   iRet = sortFile(sTaxFile, sTmp, "S(#1,C,A,#5,C,A,#6,C,A) DEL(124)");
   strcpy(sTaxFile, sTmp);

   // Create output file names
   sprintf(sAgencyFile, sTaxOutTmpl, pCnty, pCnty, "Agency");
   sprintf(sBaseFile, sTaxOutTmpl, pCnty, pCnty, "Base");
   sprintf(sDelqFile, sTaxOutTmpl, pCnty, pCnty, "Delq");
   sprintf(sDetailFile, sTaxOutTmpl, pCnty, pCnty, "Items");

   // Load Base & Delq
   iRet = TC_LoadTaxBaseAndDelq(pCnty, sTaxFile, sBaseFile, sDelqFile, true);
   if (iRet < 0)
      return iRet;

   if (!iTaxYear)
      iTaxYear = GetPrivateProfileInt(pCnty, "TaxYear", 0, acIniFile);

   // Load Agency & Detail
   iRet = TC_LoadTaxAgencyAndDetail(pCnty, sDistFile, sAgencyFile, sDetailFile, bImport);
   if (iRet < 0)
      return iRet;

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(pCnty, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(pCnty, TAX_DETAIL);
         if (!iRet)
         {
            if (getFileSize(sDelqFile) > 10)
               iRet = doTaxImport(pCnty, TAX_DELINQUENT);
            else
            {
               iRet = doTaxPrep(pCnty, TAX_DELINQUENT);
               DeleteFile(sDelqFile);
            }
            iRet = doTaxImport(pCnty, TAX_AGENCY);

            // Create empty table
            iRet = doTaxPrep(pCnty, TAX_OWNER);
         }
      }
   } else
      iRet = 0;

   return iRet;
}

/***************************** updateTaxAgency ********************************
 *
 * Merge tax agencies from county agency list table to Tax_Agency table
 *
 * Return 0 if successful, else error
 *
 ******************************************************************************/

int updateTaxAgency(char *pCnty)
{
   char acTmp[128];
   int  iRet;

   // Update CO3_Tax_Agency table
   sprintf(acTmp, "EXEC [dbo].[spUpdateAgencies] '%s'", pCnty);
   iRet = updateTaxDB(acTmp);

   return iRet;
}

/******************************** updateDelqFlag *******************************
 *
 * Update tb_isDelq to be the same as td_isDelq.
 * if bChkRedeem=true, reset isDelq in Tax_Base to 0
 * if bChkDelq=true, set isDelq in Tax_Base to 1
 *
 ******************************************************************************/

int updateBaseDelq(char *pCnty, bool bChkRedeem, bool bChkDelq, char *pServerName)
{
   char *pSvrName, acTmp[128];
   int  iRet;

   pSvrName = pServerName;
   if (bChkRedeem)
   {
      sprintf(acTmp, "EXEC [dbo].[spResetBaseDelq] '%s'", pCnty);
      iRet = updTaxDB(acTmp, pSvrName);
      pSvrName = NULL;
   }

   if (bChkDelq)
   {
      sprintf(acTmp, "EXEC [dbo].[spSetBaseDelq] '%s'", pCnty);
      iRet = updTaxDB(acTmp, pSvrName);
   }

   return iRet;
}

int updateDelqFlag(char *pCnty, bool bChkRedeem, bool bChkDelq)
{
   char acServerName[64];
   int  iRet;

   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = updateBaseDelq(pCnty, bChkRedeem, bChkDelq, acServerName);

   iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = updateBaseDelq(pCnty, bChkRedeem, bChkDelq, acServerName);

   iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = updateBaseDelq(pCnty, bChkRedeem, bChkDelq, acServerName);


   return iRet;
}

/***************************** update_TI_BillNum ******************************
 *
 * Update BillNum on Tax_Items using data from Tax_Base
 *
 * Return 0 if successful, else error
 *
 ******************************************************************************/

int update_TI_BillNum(char *pCnty)
{
   char acServerName[64], acTmp[128];
   int  iRet;

   sprintf(acTmp, "EXEC [dbo].[spUpdateBillNum] '%s'", pCnty);
   //iRet = updateTaxDB(acTmp);

   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = updTaxDB(acTmp, acServerName);

   iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = updTaxDB(acTmp, acServerName);

   iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = updTaxDB(acTmp, acServerName);

   return iRet;
}

/****************************** LoadTaxCodeTable *****************************
 *
 * Load a delimited file into TAXAGENCY table for translation
 * File format: TaxCode|Desc|Phone|TaxRate|TaxAmt|TCFlag
 * or           TaxCode|CodeXL|Desc|Phone|TaxRate|TaxAmt|TCFlag
 *  
 * Use TaxGet*() functions to access and retrieve data.
 *
 *****************************************************************************/

int LoadTaxCodeTable(char *pXrefFile, int iCodeXL)
{
   char      acTmp[_MAX_PATH], *pTmp, *apItems[16];
   int       iCnt;
   FILE      *fd;
   TAXAGENCY *pXrefTbl;

   LogMsg("Loading TaxCode table: %s", pXrefFile);
   iNumTaxDist = 0;
   fd = fopen(pXrefFile, "r");
   if (fd)
   {
      pXrefTbl = &asTaxDist[0];
      // Skip header
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (!pTmp)
            break;

         iCnt = ParseStringIQ(acTmp, '|', 15, apItems);

         if (iCnt > 5)
         {
            strcpy(pXrefTbl->Code, apItems[0]);
            pXrefTbl->iCodeLen = strlen(apItems[0]);

            strcpy(pXrefTbl->CodeXL, apItems[iCodeXL+0]);
            strcpy(pXrefTbl->Agency, myTrim(apItems[iCodeXL+1]));
            pXrefTbl->iNameLen = strlen(apItems[iCodeXL+1]);
            strcpy(pXrefTbl->Phone, apItems[iCodeXL+2]);
            strcpy(pXrefTbl->TaxRate, apItems[iCodeXL+3]);
            strcpy(pXrefTbl->TaxAmt, apItems[iCodeXL+4]);
            strcpy(pXrefTbl->TC_Flag, apItems[iCodeXL+5]);

            iNumTaxDist++;
            pXrefTbl++;
            if (iNumTaxDist >= MAX_TAX_DISTRICT)
            {
               pXrefTbl->Code[0] = 0;
               pXrefTbl->iNameLen = 0;
               pXrefTbl->iCodeLen = 0;
               break;
            }
         } else
         {
            LogMsg0("*** Invalid entries at: %s (%d)", pTmp, iNumTaxDist);
            break;
         }
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening Tax district file %s", pXrefFile);

   LogMsg("Number of district loaded: %d", iNumTaxDist);

   return iNumTaxDist;
}

/*************************** Tax_CreateAgencyTable ***************************
 *
 * Load a delimited file into SQL table CO3_TAX_AGENCY
 * File format: TaxCode|Desc|Phone|TaxRate|TaxAmt
 * or           TaxCode|CodeXL|Desc|Phone|TaxRate|TaxAmt
 *  
 *****************************************************************************/

int Tax_CreateAgencyTable(char *pCnty, char *pXrefFile, int iCodeXL)
{
   char      acTmp[512], acAgencyFile[_MAX_PATH], acAgencyRec[512], *pTmp, *apItems[16];
   int       iCnt;
   FILE      *fd, *fdAgency;
   TAXAGENCY *pAgency = (TAXAGENCY *)&acAgencyRec[0];

   LogMsg("Loading Tax_Agency table for %s using %s", pCnty, pXrefFile);

   sprintf(acAgencyFile, sTaxOutTmpl, pCnty, pCnty, "Agency");
   LogMsg("Create Agency file %s", acAgencyFile);
   fdAgency = fopen(acAgencyFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acAgencyFile);
      return -4;
   }

   iNumTaxDist = 0;
   fd = fopen(pXrefFile, "r");
   if (fd)
   {
      // Skip header
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (!pTmp)
            break;

         iCnt = ParseString(acTmp, '|', 15, apItems);

         if (iCnt > 4)
         {
            memset(acAgencyRec, 0, sizeof(TAXAGENCY));
            strcpy(pAgency->Code, apItems[0]);
            strcpy(pAgency->Agency, myTrim(apItems[iCodeXL+1]));
            strcpy(pAgency->Phone, apItems[iCodeXL+2]);
            strcpy(pAgency->TaxRate, apItems[iCodeXL+3]);

            Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgencyRec);
            fputs(acTmp, fdAgency);

            iNumTaxDist++;
         } else
         {
            LogMsg0("*** Invalid entries at: %s (%d)", pTmp, iNumTaxDist);
            break;
         }
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening Tax district file %s", pXrefFile);

   fclose(fdAgency);

   // Import into SQL
   doTaxImport(pCnty, TAX_AGENCY);

   LogMsg("Number of district loaded: %d", iNumTaxDist);

   return iNumTaxDist;
}

/********************************* findTaxDist ********************************
 *
 * Find agency sequentially through unsorted list.
 *
 ******************************************************************************/

TAXAGENCY *findTaxDist(LPCSTR pDist, int iStart)
{
   TAXAGENCY  *pXrefTbl, *pRet=NULL;
   
   if (iStart < iNumTaxDist)
   {
      pXrefTbl = &asTaxDist[iStart];   
      for (; pXrefTbl->iNameLen > 0; pXrefTbl++)
      {
         if (!_memicmp(pDist, pXrefTbl->Agency, pXrefTbl->iNameLen))
         {
            pRet = pXrefTbl;
            break;
         }
      }
   }

   return pRet;
}

/******************************* findExactTaxDist *****************************
 *
 * Find exact agency sequentially through unsorted list.
 *
 ******************************************************************************/

TAXAGENCY *findExactTaxDist(LPCSTR pDist, int iStart)
{
   TAXAGENCY  *pXrefTbl, *pRet=NULL;
   
   if (iStart < iNumTaxDist)
   {
      pXrefTbl = &asTaxDist[iStart];   
      for (; pXrefTbl->iNameLen > 0; pXrefTbl++)
      {
         if (!_stricmp(pDist, pXrefTbl->Agency))
         {
            pRet = pXrefTbl;
            break;
         }
      }
   }

   return pRet;
}

/******************************** findTaxAgency *******************************
 *
 * Find Tax code through sorted list using binary search.
 *
 ******************************************************************************/

TAXAGENCY *findTaxAgency(LPCSTR pCode, int iStart)
{
   TAXAGENCY   *pRet=NULL;
   int         iTmp, beg, end, mid;

   if (iStart >= iNumTaxDist)
      return pRet;

   beg = iStart;
   end = iNumTaxDist-1;
   mid = (beg+end)/2;                       // Find Mid Location of Array

   while (beg <= end && (iTmp = memcmp(asTaxDist[mid].Code, pCode, asTaxDist[mid].iCodeLen)))      // Compare Item and Value of Mid
   {
      if (iTmp < 0)
         beg = mid+1;
      else
         end = mid-1;

      mid = (beg+end)/2;
   }

   if (!iTmp)
   {
      pRet = &asTaxDist[mid];
   }

   return pRet;
}

/********************************* InstDueDate ********************************
 *
 * Find installment due date for specific installment.
 *
 ******************************************************************************/

int InstDueDate(char *pResult, int iInstNum, int iRollYear)
{
   int   iDate=10, iDay, iMonth, iYear;

   if (iInstNum == 1)
   {
      iMonth = 12;
      iYear = iRollYear;
   } else if (iInstNum == 2)
   {
      iMonth = 4;
      iYear = iRollYear+1;
   } else
      return 0;

   iDay = DayOfWeek(iYear, iMonth, iDate);
   if (iDay == 7)
      iDate += 2;
   else if (iDay == 1)
      iDate += 1;
   sprintf(pResult, "%d%.2d%d", iYear, iMonth, iDate);

   iDate = atol(pResult);
   return iDate;
}

/********************************* ChkDueDate *********************************
 *
 * Check last update vs installment due date
 * Return 1 if past due date, 0 otherwise.
 *
 ******************************************************************************/

int ChkDueDate(int iInstNum, int iRollYear)
{
   int   iDate=10, iDay, iMonth, iYear, iRet;
   long  lDueDate;
   char  sDueDate[16];

   if (iInstNum == 1)
   {
      iMonth = 12;
      iYear = iRollYear;
   } else if (iInstNum == 2)
   {
      iMonth = 4;
      iYear = iRollYear+1;
   } else
      return 0;

   iDay = DayOfWeek(iYear, iMonth, iDate);
   if (iDay == 7)
      iDate += 2;
   else if (iDay == 1)
      iDate += 1;
   sprintf(sDueDate, "%d%.2d%d", iYear, iMonth, iDate);

   lDueDate = atol(sDueDate);
   if (lLastTaxFileDate > lDueDate)
      iRet = 1;
   else 
      iRet = 0;

   return iRet;
}

// pDueDate = yyyymmdd
int ChkDueDate(int iInstNum, char *pDueDate)
{
   int   iRet;
   long  lDueDate;

   lDueDate = atol(pDueDate);
   if (lLastTaxFileDate > lDueDate)
      iRet = 1;
   else 
      iRet = 0;

   return iRet;
}

/**************************** isNewTaxFile ***********************************
 *
 * Check file modification date against last tax file date in county table
 * Return >0 if input file is newer, 0 if not, <0 if error.
 *
 *****************************************************************************/

int isNewTaxFile(char *pInFile, char *pCnty, int iChkDate)
{
   char  acTmp[256], acServer[256];
   int   iRet, iFileDate;

   GetIniString("Database", "Provider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return -1;

   // Ignore date checking when debug
   GetIniString("Debug", "ChkFileDate", "Y", acTmp, 128, acIniFile);
   if (acTmp[0] == 'N')
      return 1;

   if (iChkDate > 0)
      iFileDate = iChkDate;
   else
      iFileDate = lLastTaxFileDate;

   if (!iFileDate && pInFile)
   {
      if (_access(pInFile, 0))
         return -2;

      iFileDate = getFileDate(pInFile);
   }

   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);

      sprintf(acTmp, "SELECT * FROM County WHERE CountyCode='%s'", pCnty);
      m_AdoRs.Open(hl, acTmp);

      if (m_AdoRs.next())
      {
         CString	sTmp = m_AdoRs.GetItem("LastTaxFileDate");
         m_AdoRs.Close();

         if (sTmp > "20000000")
         {
            iRet = atoi(sTmp);
            if (iRet >= iFileDate)
               iRet = 0;
         } else
            iRet = 1;
      }
   } catch(_com_error &e)
   {
      LogMsg("***** Error exec cmd: %s", acServer);
      LogMsgD("%s\n", ComError(e));
      iRet = -1;
   }

   return iRet;
}

/*********************************** OpenR01 **********************************
 *
 * Open R01 file.  If R01 file nor available, try S01
 * Input: R01 data.
 *
 ******************************************************************************/

FILE *OpenR01(char *pCnty)
{
   FILE  *fd;
   char  acR01File[_MAX_PATH], acTmp[_MAX_PATH];
   int   iRet;

   iRet = GetIniString("Data", "RawFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acR01File, acTmp, pCnty, pCnty, "R01");
   if (_access(acR01File, 0))
      sprintf(acR01File, acTmp, pCnty, pCnty, "S01");
   try 
   {
      LogMsg("Open R01 file: %s", acR01File);
      fd = fopen(acR01File, "rb");
   } catch (...)
   {
      LogMsg("***** Error opening R01 file: %s", acR01File);
      fd = NULL;
   }

   return fd;
}

/********************************* UpdateTRA **********************************
 *
 * Input: R01 data.
 *
 ******************************************************************************/

int UpdateTRA(char *pOutbuf, FILE *fd)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iRet, iLoop;
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
   {
      // Drop header
      iRet = fread(acRec, iRecLen, 1, fd);
      iRet = fread(acRec, iRecLen, 1, fd);
      pRec = acRec;
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Apn, pRec, strlen(pTaxBase->Apn));
      if (iLoop > 0)
      {
         iRet = fread(pRec, 1, iRecLen, fd);
         if (iRet != iRecLen)
         {
            fclose(fd);
            fd = NULL;
            break;
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
//      if (!memcmp(pOutbuf, "10145008", 8))
//         iRet = 0;
#endif

   memcpy(pTaxBase->TRA, pRec+OFF_TRA, 6);

   // Get next roll record
   iRet = fread(acRec, iRecLen, 1, fd);
   iApnMatch++;

   return 0;
}

