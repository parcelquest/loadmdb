#include "stdafx.h"
#include "prodlib.h"
#include "LoadDef.h"


RECINFO  asInRec[MAX_RECFLDS];
int      iNumFlds;
int      iERecSize;

/********************************** LoadDefFile ******************************
 *
 * Load record definition file.
 * Return number of entries.  0 if error.
 *
 *****************************************************************************/

int LoadDefFile(LPCSTR lpFilename)
{
   char  acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_RECFLDS];
   int   iRet=0, iTmp, iCnt, iOffset=0;
   FILE  *fd;

   iERecSize = 0;
   fd = fopen(lpFilename, "r");
   if (fd)
   {
      // Get number of fields
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iTmp = ParseString(acTmp, ',', MAX_RECCOLS, pFlds);
      iNumFlds = atoi(pFlds[0]);
      if (iTmp > 1)
         iERecSize = atoi(pFlds[1]);

      if (iNumFlds > MAX_RECFLDS)
         iNumFlds = MAX_RECFLDS;

      iCnt = 0;
      while (!feof(fd) && iCnt < MAX_RECFLDS)
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            // Skip comment line
            if (*pTmp != ';')
            {
               iTmp = ParseString(acTmp, ',', MAX_RECCOLS, pFlds);
               if (iTmp == MAX_RECCOLS)
               {
                  strcpy(asInRec[iCnt].acFldType, pFlds[0]);
                  strcpy(asInRec[iCnt].acFldName, pFlds[1]);
                  asInRec[iCnt].iOffset = atoi(pFlds[2]);      // Offset must start from 0
                  if (!iCnt)
                  {
                     if (asInRec[iCnt].iOffset == 1)
                        iOffset = -1;
                  }
                  asInRec[iCnt].iOffset += iOffset;
                  asInRec[iCnt].iFldLen = atoi(pFlds[3]);
                  iCnt++;
               } else
               {
                  //LogMsg("Bad definition file: %s", lpFilename);
               }
            }
         } else
            break;
      }

      fclose(fd);

      //if (1 != iRet)
      //   LogMsg("Definition file not found.  Please verify %s", lpFilename);
   }
   return iCnt;
}

void *getOffsetTbl()
{
   if (iNumFlds > 0)
      return (void *)&asInRec[0];
   else
      return NULL;
}

/*********************************** R_Ebc2Asc ******************************
 *
 *
 ****************************************************************************/

int R_Ebc2Asc(LPCSTR strInRec, LPSTR strOutRec)
{
   char  acTmp[1024];
   unsigned char  cTmp;
   unsigned short shTmp;
   unsigned int   iTmp, iTmp1;

   *strOutRec = 0;
   for (iTmp = 0; iTmp < iNumFlds; iTmp++)
   {
      switch (asInRec[iTmp].acFldType[0])
      {
         case 'B':   // binary
            switch (asInRec[iTmp].iFldLen)
            {
               case 1:
                  cTmp = strInRec[asInRec[iTmp].iOffset];
                  sprintf(acTmp, "%2d", (int)cTmp);
                  break;
               case 2:
                  B2I((char *)&shTmp, (char *)&strInRec[asInRec[iTmp].iOffset]);
                  sprintf(acTmp, "%.5u", shTmp);
                  break;
               case 4:
                  B2L((char *)&shTmp, (char *)&strInRec[asInRec[iTmp].iOffset]);
                  sprintf(acTmp, "%.10u", shTmp);
                  break;
               default:
                  strcpy(acTmp, "????");
            }
            strcat(strOutRec, acTmp);
            break;
         case 'C':   // ascii
            memcpy(acTmp, (char *)&strInRec[asInRec[iTmp].iOffset], asInRec[iTmp].iFldLen);
            acTmp[asInRec[iTmp].iFldLen] = 0;
            strcat(strOutRec, acTmp);
            break;
         case 'E':   // ebcdic
            ebc2asc((unsigned char *)&acTmp[0], (unsigned char *)&strInRec[asInRec[iTmp].iOffset], asInRec[iTmp].iFldLen);
            acTmp[asInRec[iTmp].iFldLen] = 0;
            strcat(strOutRec, acTmp);
            break;
         case 'P':   // Packed decimal
            pd2num(acTmp, (char *)&strInRec[asInRec[iTmp].iOffset], asInRec[iTmp].iFldLen);

            // Ignore blank value
            iTmp1 = strlen(acTmp);
            if (!memcmp(acTmp, "40404040404040", iTmp1))
               memset(acTmp, ' ', iTmp1);
            strcat(strOutRec, acTmp);
            break;
         case 'Z':   // Zone decimal
            ebc2asc((unsigned char *)&acTmp[0], (unsigned char *)&strInRec[asInRec[iTmp].iOffset], asInRec[iTmp].iFldLen-1);
            cTmp = strInRec[asInRec[iTmp].iOffset+asInRec[iTmp].iFldLen-1];
            cTmp &= 0x0F;     // Remove high nibble
            cTmp |= 0x30;     // Replace it with 30H
            acTmp[asInRec[iTmp].iFldLen-1] = cTmp;
            acTmp[asInRec[iTmp].iFldLen] = 0;
            strcat(strOutRec, acTmp);
            break;
         case 'X':   // ebcdic with exception to convert unprintable character to space
            ebc2asc((unsigned char *)&acTmp[0], (unsigned char *)&strInRec[asInRec[iTmp].iOffset], asInRec[iTmp].iFldLen);
            acTmp[asInRec[iTmp].iFldLen] = 0;
            for (iTmp1 = 0; iTmp1 < asInRec[iTmp].iFldLen; iTmp1++)
               if (acTmp[iTmp1] < ' ')
                  acTmp[iTmp1] = ' ';
            strcat(strOutRec, acTmp);
            break;
         default:    // Not used
            memset(acTmp, ' ', asInRec[iTmp].iFldLen);
            acTmp[asInRec[iTmp].iFldLen] = 0;
            strcat(strOutRec, acTmp);
            break;
      }

   }

   return 0;
}

/*********************************** F_Ebc2Asc ******************************
 *
 * Return 0 if successful.  Otherwise error.
 *
 ****************************************************************************/

int F_Ebc2Asc(LPCSTR strInfile, LPCSTR strOutfile, LPCSTR strDeffile, int iERecLen)
{
   int    iRet, iReadLen, iTmp;
   BOOL   bRet, bEof;
   char   acInbuf[4096], acOutbuf[4096];
   DWORD  nBytesRead;
   DWORD  nBytesWritten;
   long   lCnt;

   HANDLE fhIn, fhOut;

   // Load definition file
   iRet = LoadDefFile(strDeffile);
   if (iRet <1)
      return -3;

   // Open Input file
   //LogMsg("Open input file %s", strInfile);
   fhIn = CreateFile(strInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return -1;

   // Open Output file
   //LogMsg("Open output file %s", strOutfile);
   fhOut = CreateFile(strOutfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return -2;

   
   // Loop through every record and convert
   bEof = false;
   if (iERecLen > 0)
      iReadLen = iERecLen;
   else
      iReadLen = iERecSize;

   // Merge loop
   iRet = 0;
   lCnt = 0;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acInbuf, iReadLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
         break;

      if (!nBytesRead)
         break;

      // Convert data
      iTmp = R_Ebc2Asc(acInbuf, acOutbuf);
      strcat(acOutbuf, "\r\n");
      iTmp = strlen(acOutbuf);

      // Out to file
      bRet = WriteFile(fhOut, acOutbuf, iTmp, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         iRet = -4;
         break;
      }
   }
   
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);
   printf(" ...\n");
   return iRet;
}

int F_Ebc2Asc(LPCSTR strInfile, LPCSTR strOutfile, int iERecLen)
{
   int    iRet, iReadLen, iTmp;
   BOOL   bRet, bEof;
   char   acInbuf[4096], acOutbuf[4096];
   DWORD  nBytesRead;
   DWORD  nBytesWritten;
   long   lCnt;

   HANDLE fhIn, fhOut;

   // Open Input file
   fhIn = CreateFile(strInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return -1;

   // Open Output file
   fhOut = CreateFile(strOutfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return -2;

   
   // Loop through every record and convert
   bEof = false;
   if (iERecLen > 0)
      iReadLen = iERecLen;
   else
      iReadLen = iERecSize;

   // Merge loop
   iRet = 0;
   lCnt = 0;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acInbuf, iReadLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
         break;

      if (!nBytesRead)
         break;

      // Convert data
      ebc2asc((unsigned char *)&acOutbuf[0], (unsigned char *)&acInbuf[0], iReadLen);
      strcpy(&acOutbuf[iReadLen], "\r\n");
      iTmp = strlen(acOutbuf);

      // Out to file
      bRet = WriteFile(fhOut, acOutbuf, iTmp, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         iRet = -4;
         break;
      }
   }
   
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);
   printf(" ...\n");
   return iRet;
}

