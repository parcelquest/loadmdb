#ifndef _myLoadExtrn_
#define _myLoadExtrn_

// SJX
extern   XREFTBL     asDeed[];
extern   int         iNumDeeds;
extern   COUNTY_INFO myCounty;

extern   FILE  *fdSale;
extern   char  acIniFile[], acRawTmpl[], acSecuredMdb[], acSalesMdb[], acMdbProvider[], 
               acGrGrTmpl[], acESalTmpl[], acSaleFile[], acCSalFile[], acRollTbl[],  
               acSaleTbl[], acRollKey[], acSaleKey[], acLienTmpl[];
extern   int   iRecLen, iApnLen, iRollLen, iNoMatch, iBadCity, iBadSuffix, iMaxLegal;
extern   long  lRecCnt, lLastRecDate, lToday, lSaleMatch, lLDRRecCount;

// SLO
extern   char  *apTokens[];
extern   int   iTokens;
extern   char  acCharFile[], acCChrFile[], acLookupTbl[], acCharTbl[], acCharKey[];
extern   char  acPublicTbl[], acPublicKey[], acPublicFile[];
extern   long  lCharMatch, lCharSkip, lSaleSkip;
extern   FILE  *fdChar;
extern   bool  bDebug;

// STA
extern   char  acExeFile[], acSitusFile[], acRollFile[];
extern   char  acOwnerTbl[], acValueTbl[];

extern   FILE  *fdExe, *fdSitus, *fdRoll, *fdChar, *fdSale;
extern   long  lExeMatch, lSitusMatch, lOptProp8;
extern   int   iCSalLen;

extern   bool  bCreateUpd, bFixTRA, bMergeOthers, bConvSale, bLoadTax, bTaxImport;
extern   char  acTmpPath[], acProp8Tmpl[];

extern   char  gsLastCity[], gsLastZip[];

#endif