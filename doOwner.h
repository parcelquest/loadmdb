#include "RecDef.h"

int   splitOwner(char *pNames, OWNER *pOwners, int iParseType=2);
void  MergeName(char *pName1, char *pName2, char *pNames);
int   MergeName1(char *pOwner1, char *pOwner2, char *pNames);
int   MergeName2(char *pOwner1, char *pOwner2, char *pNames, char bChk);
bool  markLastName(char *pName);
void  clearMarker(char *pName);
char  *swapName(char *pInName, char *pOutName, int iFlag);
