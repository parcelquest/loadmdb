#include "stdafx.h"
#include <otsortw.h>
#include "Logs.h"

/********************************** sortFile *********************************
 *
 * Sort file1 into file2
 * Return >0 if successful
 *
 *****************************************************************************/

long sortFile(char *pFile1, char *pFile2, char *pCmd, int *iErrNum, char *pWorkDrive)
{
   char  sWorkDrive[_MAX_PATH], sCmd[256], sTmpPath[_MAX_PATH];
   int   iRet;
   long  lNumRecs;

   if (pWorkDrive && *pWorkDrive > ' ')
   {
      strcpy(sWorkDrive, pWorkDrive);
      iRet = strlen(sWorkDrive);
   } else
      iRet = GetTempPath(_MAX_PATH, sWorkDrive);

   if (iRet > 1)
   {
      if (iRet > 2 && sWorkDrive[iRet-1] != '\\')
         sprintf(sCmd, "%s WORKDRIVE(%s\\)", pCmd, sWorkDrive);
      else
         sprintf(sCmd, "%s WORKDRIVE(%s)", pCmd, sWorkDrive);
   } else
      strcpy(sCmd, pCmd);

   // Begin Sort - put file name in quote in case it has embebded space
   LogMsg("Sorting %s to %s (%s)", pFile1, pFile2, sCmd);
   if (*pFile1 == 34 || !strchr(pFile1, ' '))
      strcpy(sTmpPath, pFile1);
   else
      sprintf(sTmpPath, "\"%s\"", pFile1);

   s_1mains(sTmpPath, pFile2, sCmd, &lNumRecs, &iRet);
   if (iRet > 1)
   {
      LogMsg("***** SORTING FILE ERROR!! %s (%d)", pFile1, iRet);
      lNumRecs = 0;
   } else
      LogMsg("Sorting complete.  Number of output records: %d", lNumRecs);

   if (iErrNum)
      *iErrNum = iRet;

   return lNumRecs;
}

