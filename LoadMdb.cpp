/**************************************************************************
 *
 *  Usage: LoadMdb -C<County code> [-A] [-D] [-G] [-M] [-N] [-U[?]] [-L[?]] [-X?] [-S<n>]
 *          -A : Merge attribute file
 *          -D : Duplicate sale records from old file
 *          -G : Create GrGr file for MergeAdr
 *          -Mg: Merge GrGr file
 *          -Ms: Merge cumulative sale file
 *          -Lr: Load Lien date roll (create lien file and load roll).
 *          -La: Load assessor file (or DTW)
 *          -Ll: Load land file (for county that has sale file).
 *          -Ls: Load sale file (for county that has sale file).
 *          -Lg: Load GrGr file (county specific output)
 *          -Lz: Load zipcode file into SQL
 *          -N : Do not encode suffix and city
 *          -O : Overwrite log file
 *          -U : Update roll file using old S01 file
 *          -Ua: Update assessor file using old S01 file
 *          -Uc: Update cumulative Char file
 *          -Us: Update cumulative sale file
 *          -Ux: Update sale extracted from R0x file
 *          -X1: Extract situs from roll file.
 *          -Xa: Extract attribute from attr file or from R01 file (CHAR_REC)
 *          -Xc: Extract cumulative sale from mainframe sale file.
 *          -Xg: Extract GrGr data.
 *          -Xl: Extract lien value from lien file.
 *          -Xr: Extract region file (LAX).
 *          -Xs: Extract sale data for MergeAdr from sale file.
 *          -X8: Extract prop8 parcels.
 *          -Sn: Number of records skip (default 1)
 *
 * Notes:, 
 *
 *
 * History:
 * 01/23/2008 1.5.1    Adding code to support standard use code
 * 03/11/2008 1.5.2    Use updCmpS01R01() to produce update file for SQL.
 * 05/30/2008 1.6.0    Update county table LastRecDate, LastFileDate (rolldb), and LastGrGrDate.
 * 10/13/2008 8.4.0    Verify record count. Skip version from 8.1.1 to 8.4.
 * 12/10/2008 8.5.0    Set County.Status='W' when process a county.  This prevents 
 *                     ChkCnty program from running it in parallel.  
 * 01/03/2009 8.5.1    Do not set file date.  This will be done in ChkCnty.
 * 02/11/2010 9.3.0    Adding -X8 option to allow extract Prop8 data.
 * 02/24/2010 9.4.0    Add default sendmail flag.  This can be overide by command line option.
 * 04/08/2010 9.4.2    Add option -D1 to convert old sale format to 512-byte record.
 *                     Add gsLastCity & gsLastZip to capture zip code where missing.
 * 07/07/2010 10.0.0   Update LastRecsChg to County table
 * 07/15/2010 10.1.0   Replace -Mc with -Ms to merge cumulative sale data.
 * 11/08/2010 10.2.0   Add processing date to log file.
 * 12/15/2010 10.2.1   Remove SLO & STA from LoadMdb.
 * 12/19/2011 11.0.1   Add lLienYear, lToyear, bAutoImport, updateTaxDB(), updateSaleDB(), 
 *                     doTaxImport(), doSaleImport().  Add -Xsi & -Y option. Add option
 *                     to export sale file.
 * 03/12/2012 11.1.0   Copy doSaleImport() from LoadOne.cpp over to import GrGr data.
 * 01/02/2013 12.1.0   Update DocLink to sale history table. Replace bAutoImport with bSaleImport.
 * 07/24/2013 13.0.0   Adding option to output unknown Usecode to specific log file
 * 07/29/2013 13.1.0   Create flg file on successful.
 * 10/18/2013 13.2.0   Call loadVesting() to load vesting table.
 * 02/04/2015 14.2.0   Replace GetPrivateProfileString() with GetIniString() which will
 *                     translate [Machine] to local machine name and [CO_DATA] with CO_DATA 
 *                     token defined in INI file. 
 * 01/13/2016 15.0.0   Add fdGrGr & fdCSale for use in SaleRec.cpp
 * 03/09/2016 15.1.0   Update LDRRecCount.  Update LastFileDate.
 * 04/06/2016 15.2.0   Add -T option for loading TC file.
 * 10/07/2016 16.0.0   Use new UseCode.cpp and force UseStd="999" when UseCode is empty.
 * 03/26/2017 16.7.0   Update tax import date as well as last tax file date.  Always check for new file
 *                     before processing.
 **************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Logs.h"
#include "getopt.h"
#include "hlAdo.h"
#include "Tables.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "R01.h"
#include "RecDef.h"
#include "CountyInfo.h"
#include "doSort.h"
#include "FormatApn.h"
#include "LoadMdb.h"
#include "UseCode.h"
#include "Update.h"
#include "SaleRec.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// The one and only application object

CWinApp theApp;
using namespace std;

char  *apTokens[MAX_FLD_TOKEN];
int   iTokens;

char  acSecuredMdb[_MAX_PATH], acSalesMdb[_MAX_PATH], acMdbProvider[_MAX_PATH];
char  acRawTmpl[_MAX_PATH], acGrGrTmpl[_MAX_PATH], acESalTmpl[_MAX_PATH], acSaleTmpl[_MAX_PATH];
char  acRollFile[_MAX_PATH], acCSalFile[_MAX_PATH], acCharFile[_MAX_PATH], acCChrFile[_MAX_PATH];
char  acExeFile[_MAX_PATH], acSaleFile[_MAX_PATH], acSitusFile[_MAX_PATH], acPublicFile[_MAX_PATH];
char  acRollTbl[32], acRollKey[32], acValueTbl[32], acOwnerTbl[32], acDocPath[_MAX_PATH];
char  acSaleTbl[32], acSaleKey[32], acLienTmpl[_MAX_PATH], acProp8Tmpl[_MAX_PATH];
char  acCharTbl[32], acCharKey[32];
char  acPublicTbl[32], acPublicKey[32];
char     sTCTmpl[_MAX_PATH], sTCMTmpl[_MAX_PATH], sTaxTmpl[_MAX_PATH], sRedTmpl[_MAX_PATH],
         sTRITmpl[_MAX_PATH], sTFTmpl[_MAX_PATH], sTaxOutTmpl[_MAX_PATH],
         sImportLogTmpl[_MAX_PATH];

char  gsLastCity[SIZ_S_CITY+1], gsLastZip[SIZ_S_ZIP+1];

char  acLogFile[_MAX_PATH], acTmpPath[_MAX_PATH], acIniFile[_MAX_PATH];
char  acCntyTbl[_MAX_PATH], acLookupTbl[_MAX_PATH], acUseTbl[_MAX_PATH];
char  asSuffixTbl[MAX_SUFFIX][8];
int   iRecLen, iRollLen, iCSalLen, iApnLen, iGrGrApnLen, iMaxChgAllowed, iMaxLegal;
int   iSkip, iNoMatch, iLoadFlag;
bool  bEnCode, bUseSfxXlat, bCopySales, bOverwriteLogfile, bDebug, bClean, bSendMail;
bool  bDontUpd, bCreateUpd, bFixTRA, bMergeOthers, bConvSale, bSaleImport, bLoadTax, bTaxImport;
long  lRecCnt, lLastRecDate, lToday, lToyear, lLastFileDate, lLastGrGrDate, lLienYear, lLienDate;
long  lExeMatch, lCharMatch, lSaleMatch, lSitusMatch;
long  lExeSkip, lCharSkip, lSaleSkip, lSitusSkip, lLDRRecCount;
long  lOptProp8, lLastTaxFileDate;

XREFTBL  asDeed[MAX_DEED_ENTRIES];
int      iNumDeeds;
XREFTBL  asInst[MAX_INST_ENTRIES];
int      iNumInst;
XREFTBL  asTRA[MAX_TRA_ENTRIES];
int      iNumTRA;
FILE     *fdChar, *fdExe, *fdSitus, *fdRoll, *fdGrGr, *fdSale, *fdCSale;

COUNTY_INFO myCounty;

hlAdo    hl;
hlAdoRs  m_AdoRs;
bool     m_bConnected;

int loadSjx(int, int);
int loadSlo(int, int);
int loadSta(int, int);
int mySendMail(LPCSTR pConfig, LPSTR pSubj, LPSTR pBody, LPSTR pMailTo);

/*********************************** execSqlCmd *****************************
 *
 *
 ****************************************************************************/

int execSqlCmd(LPCTSTR strCmd)
{
   int iRet = 0;
   CString strSql = strCmd;

   try
   {
      hl.ExecuteCommand(strSql);
   }

   AdoCatch (e)
   {
      LogMsg("*** Error updating Production database: %s", ComError(e));
      iRet = -3;
   }
   return iRet;
}

int execSqlCmd(LPCTSTR strCmd, hlAdo *phDb)
{
   int iRet = 0;
   CString strSql = strCmd;

   try
   {
      phDb->ExecuteCommand(strSql);
   }

   AdoCatch (e)
   {
      LogMsg("***** Error executing command [%s] : %s", strSql, ComError(e));
      iRet = -3;
   }
   return iRet;
}

/********************************* sqlConnect *******************************
 *
 *
 ****************************************************************************/

bool sqlConnect()
{
   bool bRet = true;
   char acServer[256];

   GetIniString("Database", "SqlProvider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return bRet;

   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("%s", ComError(e));
      bRet = false;
   }

   return bRet;
}

/**********************************************************************************/

int updateSaleDB(char *strCmd)
{                 
   static bool  bConnected=false;
   static hlAdo hDB;

   char acServer[256];
   int  iRet;

   iRet = GetIniString("Database", "SaleProvider", "", acServer, 128, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing SaleProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   LogMsg("Execute command on: %s", acServer);
   LogMsg(strCmd);
   try
   {
      // open the database connection
      if (!bConnected)
         bConnected = hDB.Connect(acServer);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("%s", ComError(e));
      return -2;
   }

   // Update county profile table
   iRet = execSqlCmd(strCmd, &hDB);
   return iRet;
}

/******************************** doSaleImport ******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * iType values:
 *    1: Sale data
 *    2: Grgr data
 *    3: 
 *
 * Return 0 if success
 *
 ****************************************************************************/

int doSaleImport(char *pCnty, char *pDbName, char *pInFile, int iType)
{
   char sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], acTmp[1024];
   int  iRet=1;

   GetIniString("Data", "SaleImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);

   switch (iType)
   {
      case 1:
         LogMsg("Import Sale data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "Sales");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "Sale");
         sprintf(sTblName, "%s_Sales", pCnty);

         // Delete old log files
         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         if (_access(pInFile, 0))
         {
            LogMsg("***** Missing import file: %s", pInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spSalePrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);    

         // Update DocLink
         GetIniString("Database", "HSDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing sql script file: %s to update DocLink", sFmtFile);
         } else
         {
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }
         break;

      case 2:
         LogMsg("Import Grgr data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "Sales");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "GrGr");
         sprintf(sTblName, "%s_GrGr", pCnty);

         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         if (_access(pInFile, 0))
         {
            LogMsg("***** Missing import file: %s", pInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spGrGrPrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);

         break;

      default:
         break;
   }

   return iRet;
}

/******************************** getCountyInfo *****************************
 *
 * Retrieve info from SQL server
 *
 ****************************************************************************/

int getCountyInfo()
{
   char  acTmp[256], acServer[256];
   int   iRet;

   GetIniString("Database", "Provider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return -1;

   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);

      sprintf(acTmp, "SELECT * FROM County WHERE CountyCode='%s'", myCounty.acCntyCode);
      m_AdoRs.Open(hl, acTmp);

      if (m_AdoRs.next())
      {
         CString	sTmp = m_AdoRs.GetItem("LastRecCount");
         myCounty.iLastRecCnt = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("GrGr_Delay");
         myCounty.iGrGrDelay = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("GrGr_Freq");
         myCounty.GrGrFreq = sTmp.GetAt(0);
         sTmp = m_AdoRs.GetItem("Roll_Delay");
         myCounty.iRollDelay = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("Roll_Freq");
         myCounty.RollFreq = sTmp.GetAt(0);
         m_AdoRs.Close();
      }
      iRet = 0;
   } AdoCatch(e)
   {
      LogMsg("***** Error exec cmd: %s", acTmp);
      LogMsgD("%s\n", ComError(e));
      iRet = -1;
   }

   return iRet;
}

/***************************** updateProcessFlags ***************************
 *
 * Update LastBldDate, LastRecDate, RecCount, State
 *
 ****************************************************************************/

bool updateTable(LPCTSTR strCmd)
{
   bool bRet = false;
   char acServer[256];

   GetIniString("Database", "SqlProvider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return bRet;

   LogMsg((LPSTR)strCmd);
   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("%s", ComError(e));
      return bRet;
   }

   // Update county profile table
   int iRet;
   iRet = execSqlCmd(strCmd);
   if (!iRet)
      bRet = true;
   return bRet;
}

/******************************** LoadCountyInfo ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int LoadCountyInfo(char *pCntyCode, char *pCntyTbl)
{
   char  acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_CNTY_FLDS];
   int   iRet=0, iTmp;
   FILE  *fd;

   fd = fopen(pCntyTbl, "r");
   if (fd)
   {
      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            if (!memcmp(acTmp, pCntyCode, 3))
            {
               iTmp = ParseString(myTrim(acTmp), ',', MAX_CNTY_FLDS, pFlds);
               if (iTmp > 0)
               {
                  strcpy(myCounty.acCntyCode, pCntyCode);
                  strcpy(myCounty.acStdApnFmt, pFlds[FLD_APN_FMT]);
                  strcpy(myCounty.acSpcApnFmt[0], pFlds[FLD_SPC_FMT]);
                  strcpy(myCounty.acCase[0], pFlds[FLD_SPC_BOOK]);
                  strcpy(myCounty.acCntyID, pFlds[FLD_CNTY_ID]);
                  strcpy(myCounty.acCntyName, pFlds[FLD_CNTY_NAME]);
                  strcpy(myCounty.acYearAssd, pFlds[FLD_YR_ASSD]);
                  myCounty.iYearAssd = atoi(pFlds[FLD_YR_ASSD]);
                  iRet = atoi(myCounty.acCntyID);
                  myCounty.iCntyID = iRet;
                  sprintf(myCounty.acFipsCode, "06%.3d", iRet*2-1);
                  myCounty.iApnLen = atoi(pFlds[FLD_APN_LEN]);
                  myCounty.iBookLen = atoi(pFlds[FLD_BOOK_LEN]);
                  myCounty.iPageLen = atoi(pFlds[FLD_PAGE_LEN]);
                  myCounty.iCmpLen = atoi(pFlds[FLD_CMP_LEN]);
                  iRet = 1;
               } else
               {
                  LogMsg("Bad county table file: %s", pCntyTbl);
               }

               break;
            }
         } else
            break;
      }

      fclose(fd);

      if (1 != iRet)
         LogMsg("County not found.  Please verify %s", pCntyTbl);
   } else
      LogMsg("Error opening county table %s", pCntyTbl);

   return iRet;
}

/********************************** MergeInit ********************************
 *
 * Initialize global variables
 * Return value > 0 if successful.
 *
 *****************************************************************************/

int MergeInit(char *pCnty)
{
   char  acTmp[_MAX_PATH];
   char  acCityFile[_MAX_PATH];

   int   iRet;

   // Get raw file name
   GetIniString("Data", "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);
   iRecLen = GetPrivateProfileInt("Data", "RecSize", 1900, acIniFile);

   // Public Parcel file
   iRet = GetIniString(pCnty, "PublicFile", "", acPublicFile, _MAX_PATH, acIniFile);
   if (iRet > 5)
   {
      GetIniString(pCnty, "PublicTbl", "", acPublicTbl, 32, acIniFile);
      GetIniString(pCnty, "PublicKey", "", acPublicKey, 32, acIniFile);
   }

   // Get db provider
   GetIniString("Database", "MdbProvider", "", acMdbProvider, 128, acIniFile);

   // Get roll file name
   if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))
   {
      GetIniString(pCnty, "LienTbl", "", acRollTbl, 32, acIniFile);
      GetIniString(pCnty, "LienKey", "", acRollKey, 32, acIniFile);
      GetIniString(pCnty, "LienDB", "", acSecuredMdb, _MAX_PATH, acIniFile);
   } else
   {
      GetIniString(pCnty, "RollTbl", "", acRollTbl, 32, acIniFile);
      GetIniString(pCnty, "RollKey", "", acRollKey, 32, acIniFile);
      GetIniString(pCnty, "RollDB", "", acSecuredMdb, _MAX_PATH, acIniFile);
   }

   GetIniString(pCnty, "OwnerTbl", "", acOwnerTbl, 32, acIniFile);
   GetIniString(pCnty, "ValueTbl", "", acValueTbl, 32, acIniFile);
   GetIniString(pCnty, "SaleTbl", "", acSaleTbl, 32, acIniFile);
   GetIniString(pCnty, "CharTbl", "", acCharTbl, 32, acIniFile);
   GetIniString(pCnty, "CharKey", "", acCharKey, 32, acIniFile);

   if (_access(acSecuredMdb, 0))
   {
      LogMsg("***** ERROR: missing roll db %s.  Please check!", acTmp);
      return 0;
   }
   lLastFileDate = getFileDate(acSecuredMdb);


   // Get sale file name
   GetIniString(pCnty, "SaleDB", "", acSalesMdb, _MAX_PATH, acIniFile);
   GetIniString(pCnty, "SaleTbl", "", acSaleTbl, _MAX_PATH, acIniFile);
   GetIniString(pCnty, "SaleKey", "", acSaleKey, _MAX_PATH, acIniFile);

   // Get GrGr file name
   iRet = GetIniString(pCnty, "GrGrOut", "", acGrGrTmpl, _MAX_PATH, acIniFile);
   if (!iRet)
      GetIniString("Data", "GrGrOut", "", acGrGrTmpl, _MAX_PATH, acIniFile);

   // Sale export template
   iRet = GetIniString(pCnty, "SaleExp", "", acESalTmpl, _MAX_PATH, acIniFile);
   if (!iRet)
      GetIniString("Data", "SaleExp", "", acESalTmpl, _MAX_PATH, acIniFile);

   // Get cumulative sale file
   GetIniString(pCnty, "CumSale", "", acCSalFile, _MAX_PATH, acIniFile);
   iCSalLen = GetPrivateProfileInt(pCnty, "CSalRecSize", 0, acIniFile);

   // Get other file name
   GetIniString(pCnty, "CharFile", "", acCharFile, _MAX_PATH, acIniFile);
   GetIniString(pCnty, "CChrFile", "", acCChrFile, _MAX_PATH, acIniFile);
   GetIniString(pCnty, "ExeFile", "", acExeFile, _MAX_PATH, acIniFile);
   GetIniString(pCnty, "SaleFile", "", acSaleFile, _MAX_PATH, acIniFile);
   GetIniString(pCnty, "SitusFile", "", acSitusFile, _MAX_PATH, acIniFile);
   GetIniString(pCnty, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);

   // Load suffix table
   GetIniString(pCnty, "UseSfxDev", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
   {
      bUseSfxXlat = true;
      GetIniString("System", "SfxDevTbl", "", acTmp, _MAX_PATH, acIniFile);
   } else
   {
      bUseSfxXlat = false;
      GetIniString("System", "SuffixTbl", ".\\Suffix.txt", acTmp, _MAX_PATH, acIniFile);
   }
   iRet = LoadSuffixTbl(acTmp, bUseSfxXlat);

   // Get Lookup file name
   GetIniString("System", "LookUpTbl", "", acLookupTbl, _MAX_PATH, acIniFile);

   // Get Usecode template
   GetIniString("System", "UseTbl", "", acTmp, _MAX_PATH, acIniFile);
   iNumUseCodes = 0;
   if (acTmp[0])
   {
      sprintf(acUseTbl, acTmp, pCnty);
      if (!_access(acUseTbl, 0))
         iRet = LoadUseTbl(acUseTbl);
   }

   // Set debug flag
   GetIniString(pCnty, "Debug", "N", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bDebug = true;
   else
      bDebug = false;

   // Clean up flag: bClean
   GetIniString(pCnty, "Clean", "N", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bClean = true;
   else
      bClean = false;

   // Get county info
   GetIniString("System", "CountyTbl", ".\\CountyInfo.csv", acTmp, _MAX_PATH, acIniFile);
   printf("Loading county table: %s\n", acTmp);
   iRet = LoadCountyInfo(pCnty, acTmp);

   if (iRet == 1)
   {
      // Load city table
      GetIniString("Data", "CityFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acCityFile, acTmp, pCnty);
      printf("Loading city table: %s\n", acCityFile);
      iRet = LoadCities(acCityFile);

      if (!lLienYear)
         lLienYear = atol(myCounty.acYearAssd);
      else
         sprintf(myCounty.acYearAssd, "%d", lLienYear);

      strcpy(acTmp, myCounty.acYearAssd);
      strcat(acTmp, "0101");
      lLienDate = atol(acTmp);
   }

   // Get TmpPath
   GetIniString("System", "TmpPath", "", acTmpPath, _MAX_PATH, acIniFile);
   sprintf(acTmp, "%s\\%s", acTmpPath, myCounty.acCntyCode);
   if (_access(acTmp, 0))
      _mkdir(acTmp);

   // Lien template name
   GetIniString(pCnty, "LienOut", "", acLienTmpl, _MAX_PATH, acIniFile);
   if (acLienTmpl[0] < 'A')
      GetIniString("Data", "LienOut", "", acLienTmpl, _MAX_PATH, acIniFile);

   // Prop8 template
   GetIniString("Data", "Prop8Exp", "", acProp8Tmpl, _MAX_PATH, acIniFile);

   // Check default send mail flag
   if (!bSendMail)
   {
      GetIniString(pCnty, "SendMail", "", acTmp, _MAX_PATH, acIniFile);
      if (!acTmp[0])
         GetIniString("System", "SendMail", "", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
         bSendMail = true;
   }

   // Get max changed records allowed
   iMaxChgAllowed = GetPrivateProfileInt(pCnty, "MaxChgAllowed", 0, acIniFile);
   if (!iMaxChgAllowed)
      iMaxChgAllowed = GetPrivateProfileInt("Data", "MaxChgAllowed", 50000, acIniFile);

   // Init directions
   InitDirs();
   lLastGrGrDate = 0;
   bCreateUpd = false;

   // Check AutoImport option
   GetIniString(pCnty, "ImportSale", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetIniString("System", "ImportSale", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bSaleImport = true;
   else
      bSaleImport = false;

   GetPrivateProfileString(pCnty, "ImportTax", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "ImportTax", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bTaxImport = true;
   else
      bTaxImport = false;

   GetIniString("Data", "TaxOut", "", sTaxOutTmpl, _MAX_PATH, acIniFile);

   char acVestTbl[_MAX_PATH];
   GetIniString(pCnty, "VestingTbl", "", acVestTbl, _MAX_PATH, acIniFile);
   if (acVestTbl[0] < ' ')
      GetIniString("System", "VestingTbl", "", acVestTbl, _MAX_PATH, acIniFile);

   iRet = loadVesting(pCnty, acVestTbl);

   return iRet;
}

/************************************** Usage ********************************
 *
 *
 *****************************************************************************/

void Usage()
{
   printf("\nUsage: LoadOne -C<County code> [-A] [-D] [-G] [-M] [-N] [-L] [-U] [-X?] [-S<n>]\n");
   printf("\t-A    : Merge attribute file\n");
   printf("\t-D    : Duplicate sale records from old file\n");
   printf("\t-G    : Create GrGr file for MergeAdr\n");
   printf("\t-M[c] : Merge GrGr file or cumulative sale file\n");
   printf("\t-L    : Load Lien date roll (create lien file and load roll).\n");
   printf("\t-N[u] : Do not encode suffix and city ([u]: don't update file)\n");
   printf("\t-U    : Update roll file using old S01 file\n");
   printf("\t-Xa   : Extract attribute from attr file.\n");
   printf("\t-Xc   : Extract cumulative sale from mainframe sale file.\n");
   printf("\t-Xe   : Extract exemption file (i.e. Sta_Exe.csv.\n");
   printf("\t-Xg   : Extract GrGr data.\n");
   printf("\t-Xl   : Extract lien value from lien file.\n");
   printf("\t-Xs[i]: Extract sale data and create sale import file.\n");
   printf("\t-Sn   : Number of records skip (default 1)\n");
   exit(1);
}

/********************************** ParseCmd() *******************************
 *
 *
 *****************************************************************************/

void ParseCmd(int argc, char* argv[])
{
   char  chOpt;                // gotten option character
   char *pszParam;

   bEnCode = true;
   bCopySales = false;
   bOverwriteLogfile = false;
   bSendMail = false;
   bDontUpd = false;
   bFixTRA = false;
   bMergeOthers = false;
   bConvSale = false;
   bLoadTax = false;

   iSkip = 1;
   iLoadFlag = 0;

   if (argv[1][0] != '-')
   {
      strcpy(myCounty.acCntyCode, argv[1]);
      return;
   }

   while (1)
   {
      chOpt = GetOption(argc, argv, "C:AD:EF:GL:M:N:OS:TU:X:Y:?", &pszParam);
      if (chOpt > 1)
      {
         // chOpt is valid argument
         switch (chOpt)
         {
            case 'A':   // Merge attribute data
               iLoadFlag |= MERG_ATTR;
               break;

            case 'C':   // county code
               if (pszParam != NULL)
                  strcpy(myCounty.acCntyCode, pszParam);
               else
               {
                  printf("Missing county code\n");
                  Usage();
               }
               break;

            case 'D':   // Duplicate sale record
               if (pszParam != NULL)
               {
                  if (!strcmp(pszParam, "8"))
                     lOptProp8 = MYOPT_UDB;     // Update Prop8 flag in DB
                  if (!strcmp(pszParam, "1"))   // Convert cum sale to SCREC_REC
                     bConvSale = true;
               } else
                  bCopySales = true;            // Duplicate sale record
               break;

            case 'E':   // Email if error occurs
               bSendMail = true;
               break;

            case 'F':   // Fix data field
               if (pszParam != NULL)
               {
                  if (*pszParam == 't')
                     bFixTRA = true;
               }
               break;

            case 'G':   // Load GrGr data
               iLoadFlag |= LOAD_GRGR;
               break;

            case 'L':   // Load lien date roll
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')
                     iLoadFlag |= LOAD_SALE;
                  else if (*pszParam == 'a')
                     iLoadFlag |= LOAD_ASSR;
                  else if (*pszParam == 'r')
                     iLoadFlag |= LOAD_LIEN;
                  else if (*pszParam == 'l')
                     iLoadFlag |= LOAD_LAND;
                  else if (*pszParam == 'g')
                     iLoadFlag |= LOAD_GREC;
                  else if (*pszParam == 'c')
                     iLoadFlag |= LOAD_ATTR;       // Load or convert Characteristic/Attribute
               } else
                  iLoadFlag |= LOAD_LIEN;
               break;

            case 'X':   // Extract data
               if (pszParam != NULL)
               {
                  if (*pszParam == 'l')
                     iLoadFlag |= EXTR_LIEN;
                  else if (*pszParam == 'a')
                     iLoadFlag |= EXTR_ATTR;
                  else if (*pszParam == 'c')
                     iLoadFlag |= EXTR_CSAL;
                  else if (*pszParam == 'g')
                     iLoadFlag |= EXTR_GRGR;
                  else if (*pszParam == 'r')
                     iLoadFlag |= EXTR_ROLL;
                  else if (*pszParam == 's')
                  {
                     iLoadFlag |= EXTR_SALE;
                     if (*(pszParam+1) == 'i')
                        iLoadFlag |= EXTR_ISAL;
                  }
                  else if (*pszParam == '1')
                     iLoadFlag |= EXTR_SADR;
                  else if (*pszParam == 'x')
                     iLoadFlag |= EXTR_XSAL;
                  else if (*pszParam == '8')
                     lOptProp8 = MYOPT_EXT;     // Extract prop8 flag
                  else
                  {
                     printf("Invalid extract option %s\n", pszParam);
                     Usage();
                  }
               } else
                  Usage();
               break;
 
            case 'M':   // Merge sale or GrGr data
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')
                     iLoadFlag |= MERG_CSAL;
                  else if (*pszParam == 'o')
                     bMergeOthers = true;       // Merge other values
               } else
                  iLoadFlag |= MERG_GRGR;
               break;

            case 'N':   // Do not encode
               if (pszParam != NULL)
               {
                  if (*pszParam == 'u')                     
                     bDontUpd = true;
               } else
                  bEnCode = false;
               break;

            case 'O':   // Overwrite logfile
               bOverwriteLogfile = true;
               break;

            case 'T':   // Load tax files
               bLoadTax = true;
               break;

            case 'U':   // Update roll file
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')
                     iLoadFlag |= UPDT_SALE;
                  else if (*pszParam == 'a')
                     iLoadFlag |= UPDT_ASSR;
                  else if (*pszParam == 'c')
                     iLoadFlag |= UPDT_CHAR;
                  else if (*pszParam == 'x')
                     iLoadFlag |= UPDT_XSAL;
               } else
                  iLoadFlag |= LOAD_UPDT;
               break;

            case 'S':   // Skip records
               if (pszParam != NULL)
                  iSkip = atoi(pszParam);
               else
                  Usage();
               break;

            case 'Y':
               if (*pszParam > '0')
               {
                  lLienYear = atol(pszParam);
                  if (lLienYear > lToyear || lLienYear < 2000)
                  {
                     printf("Invalid LDR year option %s\n", pszParam);
                     Usage();
                  }
               } else
                  Usage();
               break;

            case '?':   // usage info
            default:
               Usage();
               break;
         }
      }
      if (chOpt == 0)
      {
         // end of argument list
         break;
      }
      if ((chOpt == 1) || (chOpt == -1))
      {
         // standalone param or error
         LogMsg("Argument [%s] not recognized\n", pszParam);
         break;
      }
   }
}

/********************************** main() ********************************/

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
   int   iRet=1, iTmp;
   char  acTmp[256], acLogPath[256], acToday[16], acVersion[64];
   char  acSubj[256], acBody[512];

   // initialize MFC and print and error on failure
   if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
   {
      cerr << _T("Fatal Error: MFC initialization failed") << endl;
      return -1;
   }

   LoadString(theApp.m_hInstance, IDS_APP_VERSION, acVersion, 64);
   printf("%s\n\n", acVersion);
   strcat(acVersion, " Options: ");
   for (iTmp = 1; iTmp < argc; iTmp++)
   {
      strcat(acVersion, argv[iTmp]);
      strcat(acVersion, " ");
   }

   if (argc < 2)
      Usage();

   // Parse command line
   ParseCmd(argc, argv);

   _getcwd(acIniFile, _MAX_PATH);
   strcat(acIniFile, "\\LoadMdb.ini");

   // If not found INI file in working folder, check default location
   if (_access(acIniFile, 0))
      strcpy(acIniFile, "C:\\Tools\\LoadMdb.ini");

   printf("Initializing %s\n", acIniFile);
   GetIniString("System", "LogPath", "", acLogPath, _MAX_PATH, acIniFile);

   // Get today date - yyyymmdd
   getCurDate(acToday);
   lRecCnt = 0;
   iMaxLegal=0;
   lLastRecDate = 0;
   lToday = atol(acToday);
   lToyear= atoin(acToday, 4);

   // open log file
   if (iLoadFlag & LOAD_LIEN)
   {
      if (lLienYear > 0)
         sprintf(acLogFile, "%s\\Load_%s_%d.ldr", acLogPath, myCounty.acCntyCode, lLienYear);
      else
         sprintf(acLogFile, "%s\\Load_%s_%d.ldr", acLogPath, myCounty.acCntyCode, lToyear);
      lLDRRecCount = 0;
   } else
      sprintf(acLogFile, "%s\\Load_%s_%s.log", acLogPath, myCounty.acCntyCode, acToday);

   if (bOverwriteLogfile)
      open_log(acLogFile, "w");
   else
      open_log(acLogFile, "a+");

   // Open Usecode log
   if (!openUsecodeLog(myCounty.acCntyCode, acIniFile))
      LogMsg("***** %s", getLastErrorLog());

   LogMsg("%s\n", acVersion);
   LogMsg("Loading %s", myCounty.acCntyCode);

   // Initialize process
   iTmp = MergeInit(myCounty.acCntyCode);
   if (iTmp < 1)
      return 1;

   if (iLoadFlag & LOAD_LIEN)
      LogMsg("LDR %s processing.", myCounty.acYearAssd);
   else if (iLoadFlag & LOAD_UPDT)
      LogMsg("Regular update using %s base year.", myCounty.acYearAssd);

   // Reset automation flag
   sprintf(acTmp, "UPDATE County SET Status='W' WHERE (CountyCode='%s')", myCounty.acCntyCode);
   updateTable(acTmp);
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      sprintf(acTmp, "UPDATE Products SET State='W' WHERE (Prefix='S%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Retrieve last record count
   iTmp = getCountyInfo() ;
   if (iTmp < 0)
   {
      close_log();
      return 1;
   }

   // Start processing
   if (!_stricmp(myCounty.acCntyCode, "SJX"))
      iRet = loadSjx(iLoadFlag, iSkip);
   // SLO is moved to LoadOne
   //else if (!stricmp(myCounty.acCntyCode, "SLO"))
   //   iRet = loadSlo(iLoadFlag, iSkip);
   // STA is moved to LoadMB
   //else if (!stricmp(myCounty.acCntyCode, "STA"))
   //   iRet = loadSta(iLoadFlag, iSkip);
   else
   {
      iRet = -1;
      LogMsgD("***** Invalid county code.  Please check with Sony for correct version *****");
   }

   // Set state='F' if fail, 'P' for data processed and ready for product build 
   if (iRet)
   {
      if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
      {
         sprintf(acTmp, "UPDATE Products SET State='F' WHERE (Prefix='S%s')", myCounty.acCntyCode);
         updateTable(acTmp);
      }

      // Send mail
      if (bSendMail)
      {
         sprintf(acSubj, "*** Error loading %s", myCounty.acCntyCode);
         sprintf(acBody, "%s\nSee %s for more info", getLastErrorLog(), acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
      }
   } else
   {
      if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
      {
         if (lRecCnt > (myCounty.iLastRecCnt-((myCounty.iLastRecCnt/100)*5)))
         {
            // Update Products table
            sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='S%s')", lRecCnt, myCounty.acCntyCode);
            updateTable(acTmp);

            // Update CtyProfiles
            sprintf(acTmp, "UPDATE ctyProfiles SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d,State='P' WHERE (CountyCode='%s')", acToday, lRecCnt, lLastRecDate, myCounty.acCntyCode);
            updateTable(acTmp);

            // Update county table
            char sRollFileDt[32], sTaxFileDt[32];

            sRollFileDt[0] = 0;
            sTaxFileDt[0] = 0;
            if (lLastFileDate > 0)
               sprintf(sRollFileDt, ",LastFileDate=%d", lLastFileDate);

            // LastFileDate is set by ChkCnty.  But we provide option to do it here for manual run
            if (iLoadFlag & LOAD_LIEN)
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LDRRecCount=%d,LastRecDate=%d %s %s WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLDRRecCount, lLastRecDate, sRollFileDt, sTaxFileDt, myCounty.acCntyCode);
            else if (iLoadFlag & LOAD_UPDT)
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d %s %s WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLastRecDate, sRollFileDt, sTaxFileDt, myCounty.acCntyCode);
            else 
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s %s %s WHERE (CountyCode='%s')", 
                  acToday, sRollFileDt, sTaxFileDt, myCounty.acCntyCode);
            updateTable(acTmp);
         } else if (lRecCnt > 0)
         {
            char acSubj[256], acBody[512];

            sprintf(acSubj, "*** WARNING: Number of records is too small for %s (%d) ***", myCounty.acCntyCode, lRecCnt);
            LogMsg(acSubj);
            sprintf(acBody, "Input file may be corrupted.  Please check FTP folder on User6 to verify file size or call Sony 714-247-9732");

            // Send mail
            if (bSendMail)
               mySendMail(acIniFile, acSubj, acBody, NULL);

            bDontUpd = true;
         }

         if (iMaxLegal > 0)
            LogMsg("Max legal length:           %u", iMaxLegal);
      } 
      
      if (bLoadTax && lLastTaxFileDate > 0)
      {
         if (bTaxImport)
            sprintf(acTmp, "UPDATE County SET LastTaxImpDate=%s,LastTaxFileDate=%d WHERE (CountyCode='%s')", acToday, lLastTaxFileDate, myCounty.acCntyCode);
         else
            sprintf(acTmp, "UPDATE County SET LastTaxFileDate=%d WHERE (CountyCode='%s')", lLastTaxFileDate, myCounty.acCntyCode);
         updateTable(acTmp);
      }
   }

   // Generate update file
   if (!iRet && !bDontUpd && bCreateUpd && !(iLoadFlag & LOAD_LIEN))
   {
      iRet = chkS01R01(myCounty.acCntyCode, acRawTmpl, iRecLen, iApnLen);
      if (iRet >= 0)
      {
         // Store number of recs changed to County table
         sprintf(acTmp, "UPDATE County SET LastRecsChg=%d WHERE (CountyCode='%s')", iRet, myCounty.acCntyCode);
         updateTable(acTmp);

         // If number of changed records greater than allowed, we have to import manually
         if (iRet > iMaxChgAllowed)
         {
            // Email production
            LogMsg("*** WARNING: Number of changed records is too big for %s (%d) ***", myCounty.acCntyCode, iRet);
            LogMsg("***          Please run CDDEXTR -Q -TWEBIMPORT -C%s ***", myCounty.acCntyCode);

            GetIniString("Mail", "MailChgTo", "", acTmp, 256, acIniFile);
            LogMsg("Send email to %s", acTmp);

            sprintf(acSubj, "%s - %d recs changed.  Use CddExtr to update SQL server.", myCounty.acCntyCode, iRet);
            sprintf(acBody, "Msg from %s.\nSee log file Load_%s.log for more info.", acVersion, myCounty.acCntyCode);
            mySendMail(acIniFile, acSubj, acBody, acTmp);
         }

         iRet = 0;
      }
   }

   // Reset county status
   sprintf(acTmp, "UPDATE County SET Status='R' WHERE (CountyCode='%s')", myCounty.acCntyCode);
   updateTable(acTmp);

   // Create flag file
   if (CreateFlgFile(myCounty.acCntyCode, acIniFile))
      LogMsg("***** Error creating flag file *****");

   // Import sale file
   if (iLoadFlag & EXTR_ISAL)
   {
      char sDbName[64], acOutFile[_MAX_PATH];

      if (iLoadFlag & LOAD_LIEN)
         sprintf(sDbName, "LDR%d", lLienYear);
      else
         sprintf(sDbName, "UPD%d", lLienYear);

      GetIniString("Data", "SqlSalesFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, sDbName, myCounty.acCntyCode);
      iTmp = createSaleImport(myCounty.acCntyCode, acCSalFile, acOutFile, 1, false);
      if (iTmp > 0 && bSaleImport)
         iTmp = doSaleImport(myCounty.acCntyCode, sDbName, acOutFile, 1);

      // send email
      if (iTmp && bSendMail)
      {
         sprintf(acSubj, "*** %s - Error importing sale file.", myCounty.acCntyCode);
         sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
      }
   }

   // Clean up memory allocation
   if (iNumUseCodes > 0 && pUseTable)
      delete pUseTable;

   // Close log files
   closeUsecodeLog();
   close_log();

   return iRet;
}


