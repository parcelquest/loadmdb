/**************************************************************************
 *
 * Notes:
 * To load lien date data:
 *    - Rename SSJX.R01 file to SSJX.O01
 *    - Run loadMdb -CSJX -L -Xsi -O -Xl [-X8]
 *    - Williamson/HomeSite amt is not taxable
 *
 * To load update roll:
 *    - Run loadMdb -CSJX -U -Xsi [-X8]
 *
 * 07/15/2007 1.4.2     Fix LDR loading for 2007
 * 03/11/2008 1.5.2     Use standard function to update usecode
 * 01/22/2009 8.5.2     Fix TRA
 * 04/17/2009 8.5.3     Add Sjx_ExtrLien() and option to merge other values.
 *                      Change the way to calculate Other_Val.
 * 07/16/2009 9.1.1     Fix HO Exemption bug.  We should use SEC_HO_EX_AMT to set 
 *                      HO flag.  The SEC_OTHER_EX_CD is not specifically set for HO.
 * 07/21/2009 9.1.2     Remove vertical bar from legal.
 * 10/08/2009 9.1.3     Add new field SEC_BATHROOM_HALF
 * 02/11/2010 9.3.0     Add Sjx_ExtractProp8() and modify Sjx_MergeRoll() & Sjx_CreateLienRec()
 *                      to extract and update prop8 flag.
 * 04/08/2010 9.4.2     Add Sjx_CreateSale() to extract SALEMST and update SJX_SALE.SLS
 *                      Convert SALE_REC to SCSAL_REC and Sale_Exp.sls to Sjx_Sale.sls
 * 08/17/2010 10.1.1    Add TAX_CODE. 
 * 08/24/2010           Populate FULL_EXEMPT flag, force EXE_TOTAL to be <= GROSS.
 * 09/15/2010 10.1.2    Modify Sjx_MergeAdr() to fix street name with embeded unit number.
 * 12/10/2010 10.2.1    Modify Sjx_MergeRoll() to include PP_PEN_AMT to OTHER_VAL to make
 *                      the GROSS_AMT similar to SJX posted on its website.  
 *                      Also add PP_PEN_AMT to R01 record.
 * 02/25/2011 10.3.0    Exclude HomeSite value from total assessed.  This is done to match
 *                      with the county total.
 * 07/05/2011 11.0.0    Add S_HSENO. No range str number found.
 * 12/19/2011 11.0.1    Update AltApn using SEC_MH_REF_APN.
 * 12/07/2012 12.0.1    Use FixCumSale() to fix DocType and use special Sjx_DeedXref.csv.
 * 05/15/2013 12.2.1    Fix bug in Sjx_CreateSale() by making sure string not empty before use.
 * 07/24/2013 13.0.0    Output unknown Usecode to specific log file
 * 10/15/2013 13.1.1    Use updateVesting() to update Vesting and Etal flag.
 * 11/15/2013 13.2.1    Add new field SEC_NO_UNITS starting 11/15/2013
 * 06/27/2014 14.0.0    Add comment and log msg. Sync Utils.cpp and SaleRec.cpp
 * 04/06/2016 15.2.0    Loading TC file.
 * 10/07/2016 16.0.0    Use new UseCode.cpp and force UseStd="999" when UseCode is empty.
 * 12/28/2016 16.2.1    Modify Sjx_CreateSale() to set MultiSale flag
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "hlAdo.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "Tables.h"
#include "doOwner.h"
#include "doSort.h"
#include "formatApn.h"
#include "SaleRec.h"

#include "LoadMdb.h"
#include "UseCode.h"
#include "LMExtrn.h"
#include "PQ.h"
#include "MergeSjx.h"
#include "Tax.h"

static   int      iNumFlds;
static   bool     bUnits;
static   hlAdo    hSecMstr, hSaleMstr;
static   hlAdoRs  rsSecMstr, rsSaleMstr;

/********************************* Sjx_FmtDoc ********************************
 *
 *   1) UNREC-02 19651212 = UNREC-02 19651212
 *   2) A205-572 19111212 = A205-572 19111212
 *   3) 90044613 19900504 = 44613 19900504
 *   4) 3514-374 19710415 = 3514-374 19710415
 *
 *****************************************************************************/

void Sjx_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pInDoc, char *pInDate)
{
/*
   char  *pTmp;

  if (*pInDoc < '0')
   {
      *pOutDoc = 0;
      memset(pOutDate, ' ', SIZ_SALE1_DT);
   } else if (!memcmp(pInDoc, (char *)(pInDate+2), 2))
   {
      pTmp = pInDoc+2;
      while (*pTmp == '0')
         pTmp++;
      strcpy(pOutDoc, pTmp);
      strcpy(pOutDate, pInDate);
   } else
   {
      strcpy(pOutDoc, pInDoc);
      strcpy(pOutDate, pInDate);
   }
*/
   strcpy(pOutDoc, pInDoc);
   strcpy(pOutDate, pInDate);
   blankPad(pOutDoc, SIZ_SALE1_DOC);
}

void Sjx_FmtRecDoc(char *pOutbuf)
{
   char     acInDoc[16], acInDate[16], acDoc[16], acDate[16];
   int      iTmp;
   CString strFld;

   strFld = rsSecMstr.GetItem("SEC_OR_1");
   if (strFld.GetLength() < 4)
      return;

   strcpy(acInDoc, strFld);
   strcpy(acInDate, rsSecMstr.GetItem("SEC_RD_1"));

   Sjx_Fmt1Doc(acDoc, acDate, acInDoc, acInDate);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE1) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

   strFld = rsSecMstr.GetItem("SEC_OR_1_TYPE");
   if (strFld.GetLength() >= 2)
   {
      //int XrefCode2Idx(XREFTBL *pTable, char *pCode, int iMaxEntries)
      iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], strFld.GetBuffer(0), iNumDeeds);
      if (iTmp > 0)
      {
         iTmp = sprintf(acDoc, "%d", iTmp);
         memcpy(pOutbuf+OFF_SALE1_DOCTYPE, acDoc, iTmp);
      }
   }

   // Advance to Rec #2
   strFld = rsSecMstr.GetItem("SEC_OR_2");
   if (strFld.GetLength() < 4)
      return;

   strcpy(acInDoc, strFld);
   strcpy(acInDate, rsSecMstr.GetItem("SEC_RD_2"));

   Sjx_Fmt1Doc(acDoc, acDate, acInDoc, acInDate);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE2) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE1_DOC);

   strFld = rsSecMstr.GetItem("SEC_OR_2_TYPE");
   if (strFld.GetLength() >= 2)
   {
      //int XrefCode2Idx(XREFTBL *pTable, char *pCode, int iMaxEntries)
      iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], strFld.GetBuffer(0), iNumDeeds);
      if (iTmp > 0)
      {
         iTmp = sprintf(acDoc, "%d", iTmp);
         memcpy(pOutbuf+OFF_SALE2_DOCTYPE, acDoc, iTmp);
      }
   }

   // Advance to Rec #3
   strFld = rsSecMstr.GetItem("SEC_OR_3");
   if (strFld.GetLength() < 4)
      return;

   strcpy(acInDoc, strFld);
   strcpy(acInDate, rsSecMstr.GetItem("SEC_RD_3"));

   Sjx_Fmt1Doc(acDoc, acDate, acInDoc, acInDate);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE3) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE1_DOC);

   strFld = rsSecMstr.GetItem("SEC_OR_3_TYPE");
   if (strFld.GetLength() >= 2)
   {
      //int XrefCode2Idx(XREFTBL *pTable, char *pCode, int iMaxEntries)
      iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], strFld.GetBuffer(0), iNumDeeds);
      if (iTmp > 0)
      {
         iTmp = sprintf(acDoc, "%d", iTmp);
         memcpy(pOutbuf+OFF_SALE3_DOCTYPE, acDoc, iTmp);
      }
   }
}

/******************************** Sjx_MergeXfer ******************************
 *
 * Update transfer if needed
 *
 *****************************************************************************/

void Sjx_MergeXfer(char *pOutbuf)
{
   char     acInDoc[16], acInDate[16], acTmp[32];
   int      iTmp, lCurXferDate, lLastXferDate;
   CString  strFld;

   strFld = rsSecMstr.GetItem("SEC_OR_1");
   if (strFld.GetLength() < 4)
      return;

   strcpy(acInDoc, strFld);
   strcpy(acInDate, rsSecMstr.GetItem("SEC_RD_1"));
   lLastXferDate = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   lCurXferDate  = atol(acInDate);
   
   if (lCurXferDate > lLastXferDate)
   {
      // Reformat DocNum
      iTmp = strlen(acInDoc);
      if (iTmp <= 7 && acInDoc[0] <= '9')
      {
         memset(acTmp, '0', 8-iTmp);
         strcpy((char *)&acTmp[8-iTmp], acInDoc);
         strcpy(acInDoc, acTmp);
      }

      // Update sale1 & transfer
      memcpy(pOutbuf+OFF_TRANSFER_DT, acInDate, SIZ_TRANSFER_DT);
      blankPad(acInDoc, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acInDoc, SIZ_TRANSFER_DOC);
   }
}

/******************************** Sjx_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sjx_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acTmp[128];
   char  *pTmp;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   strcpy(acTmp, pNames);
   iTmp = blankRem(acTmp);
   memcpy(pOutbuf+OFF_NAME1, acTmp, (iTmp < SIZ_NAME1 ? iTmp:SIZ_NAME1));

   // Update vesting
   updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   if (pTmp=strstr(acTmp, " COTRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " LF EST"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " LIFE EST"))
      *pTmp = 0;

   // Now parse owners
   splitOwner(acTmp, &myOwner, 0);

   iTmp = strlen(myOwner.acSwapName);
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, (iTmp < SIZ_NAME_SWAP ? iTmp:SIZ_NAME_SWAP));
}

/********************************* Sjx_MergeAdr ******************************
 *
 * For this county, blank both situs and mailing address before update.  If 
 * no situs and home owner occupied, use mailing for situs if it is not PO.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sjx_MergeAdr(char *pOutbuf)
{
   char     *pTmp, acTmp[256], acTmp1[32], acAddr1[64], acAddr2[64];
   char     acStr[52], acUnit[32], acCityCode[32];
   int      iTmp, iStrNo, lZip;
   ADR_REC  sMailAdr;
   CString  strFld;

   // Clear old addresses
   removeSitus(pOutbuf);
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, rsSecMstr.GetItem("SEC_STREET_ADDR"));

   // Parsing mail address
   if (memcmp(acAddr1, "     ", 5))
   {
      iStrNo = atoi(acAddr1);
      pTmp = acAddr1;
      if (iStrNo > 0)
      {
         while (pTmp = strchr(pTmp, ' '))
         {
            pTmp++;
            if (*pTmp >= '1')
               break;
            if (!memcmp(pTmp+1, "TH", 2) || 
                !memcmp(pTmp+1, "ST", 2) || 
                !memcmp(pTmp+1, "RD", 2))
               break;
         }

         // Prevent bad address
         if (pTmp)
            parseAdr1N(&sMailAdr, pTmp);
         sprintf(sMailAdr.strNum, "%d", iStrNo);
         sMailAdr.lStrNum = iStrNo;
      } else
         strcpy(sMailAdr.strName, acAddr1);
      
      if (sMailAdr.lStrNum > 0)
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      else
      {
         iTmp = SIZ_M_STRNUM;
         strcpy(acTmp, BLANK32);
      }
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      strcpy(acAddr2, rsSecMstr.GetItem("SEC_CITY"));
      memcpy(pOutbuf+OFF_M_CITY, acAddr2, strlen(acAddr2));

      strcpy(acTmp, rsSecMstr.GetItem("SEC_STATE"));
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_M_ST, acTmp, SIZ_M_ST);
         strcat(acAddr2, " ");
         strcat(acAddr2, acTmp);
      }
   
      // Zipcode
      strcpy(acTmp, rsSecMstr.GetItem("SEC_ZIP_CODE"));
      if (acTmp[0] > ' ')
      {
         iTmp = atoi(acTmp);
         sprintf(sMailAdr.Zip, "%0.5d", iTmp);
         memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
         strcat(acAddr2, " ");
         strcat(acAddr2, sMailAdr.Zip);

         // Zip4
         strcpy(acTmp, rsSecMstr.GetItem("SEC_ZIP_CODE_SUB"));
         if (acTmp[0] > ' ')
         {
            iTmp = atoi(acTmp);
            sprintf(sMailAdr.Zip4, "%0.4d", iTmp);

            memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
            strcat(acAddr2, "-");
            strcat(acAddr2, sMailAdr.Zip4);
         }
      }

      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));
   }

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   acStr[0] = 0;
   acUnit[0] = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "03334022", 8))
   //   iTmp = 0;
#endif

   strcpy(acAddr1, rsSecMstr.GetItem("SEC_SITUS_NO"));
   iStrNo = atoi(acAddr1);
   if (iStrNo > 0)
   {
      // Save original HSENO
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));

      iTmp = sprintf(acTmp, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);

      strFld = rsSecMstr.GetItem("SEC_SITUS_DIR");
      if (!strFld.IsEmpty())
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, strFld);
         memcpy(pOutbuf+OFF_S_DIR, strFld, strFld.GetLength());
      }

      strFld = rsSecMstr.GetItem("SEC_SITUS_STREET");
      if (!strFld.IsEmpty())
      {
         if ((iTmp = strFld.Find(" UNIT ", 2)) > 2)
         {
            strcpy(acStr, strFld.Left(iTmp));
            strcpy(acTmp1, strFld.Mid(iTmp+6));
            if (acTmp1[0] == '#')
               strcpy(acUnit, acTmp1);
            else
               sprintf(acUnit, "#%s", acTmp1);
         } else if ((iTmp = strFld.Find(" STE ", 2)) > 2)
         {
            strcpy(acStr, strFld.Left(iTmp));
            strcpy(acTmp1, strFld.Mid(iTmp+5));
            if (acTmp1[0] == '#')
               strcpy(acUnit, acTmp1);
            else
               sprintf(acUnit, "#%s", acTmp1);
         } else if ((iTmp = strFld.FindOneOf("-#")) > 1)
         {
            iTmp = strFld.FindOneOf("-#");
            strcpy(acStr, strFld.Left(iTmp));
            strcpy(acUnit, strFld.Mid(iTmp));
            if (acUnit[0] == '-' && isdigit(acStr[iTmp-1]))
            {
               // 033-340-22: HAM 4-D 
               pTmp = strrchr(acStr, ' ');
               if (pTmp)
               {
                  sprintf(acTmp1, "#%s%s", pTmp+1, acUnit);
                  strcpy(acUnit, acTmp1);
                  *pTmp = 0;
               }
            } else
            {
               iTmp = atol(strFld.Mid(iTmp+1));
               if (iTmp > 0)
                  sprintf(acUnit, "#%d", iTmp);
               else 
               {
                  if (strlen(acUnit) <= SIZ_S_UNITNO)
                  {
                     if (acUnit[0] == '-')
                     {
                        // HAM - B
                        if (acUnit[1] == ' ')
                           strcpy(&acUnit[1], &acUnit[2]);
                        acUnit[0] = '#';
                     }
                  } else if (acUnit[0] == '#')
                     acUnit[SIZ_S_UNITNO] = 0;
                  else
                  {
                     acUnit[0] = 0;
                     strcpy(acStr, strFld);
                  }
               }
            }
         } else
         {
            // Check for unit# in the last token
            // 09819014
            strcpy(acStr, strFld);

            if (pTmp = strrchr(acStr, ' '))
            {
#ifdef _DEBUG
               //if (!memcmp(pOutbuf, "098190", 6))
               //   iTmp = 0;
#endif

               iTmp = atol(pTmp);
               if (iTmp > 0 
                  && (!strstr(acStr, "HWY ") 
                  && !strstr(acStr, "RT ")
                  && !strstr(acStr, "ROUTE ") 
                  && memcmp(acStr, "HIGHWAY", 7)))
               {
                  if (!strchr(pTmp, '/'))
                  {
                     // 09819017: BEN HOLT 17
                     *pTmp = 0;
                     strcpy(&acUnit[1], pTmp+1);
                     acUnit[0] = '#';
                  }
               }
            }
         }

         strcat(acAddr1, " ");
         strcat(acAddr1, acStr);
         memcpy(pOutbuf+OFF_S_STREET, acStr, strlen(acStr));
      }

      strFld = rsSecMstr.GetItem("SEC_SITUS_TYPE");
      if (!strFld.IsEmpty())
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, strFld);

         iTmp = GetSfxCodeX(strFld.GetBuffer(0), acTmp);
         if (iTmp > 0)
         {
            Sfx2Code(acTmp, acTmp1);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp1, SIZ_S_SUFF);
         }
      }

      if (acUnit[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, acUnit);
         memcpy(pOutbuf+OFF_S_UNITNO, acUnit, strlen(acUnit));
      }
   } else
   {
      strFld = rsSecMstr.GetItem("SEC_SITUS_STREET");
      if (!strFld.IsEmpty())
      {
         strcpy(acAddr1, strFld);
         memcpy(pOutbuf+OFF_S_STREET, strFld, strFld.GetLength());
      }
   }

   strFld = rsSecMstr.GetItem("SEC_SITUS_CITY");
   if (!strFld.IsEmpty())
   {
      strcpy(acTmp, strFld);
      if (acTmp[2] > ' ')
      {
         Abbr2City((char *)&acTmp[2], acAddr2);
         if (acAddr2[0] > ' ')
         {
            strcat(acAddr2, " CA");

            Abbr2Code((char *)&acTmp[2], acCityCode);
            memcpy(pOutbuf+OFF_S_CITY, acCityCode, strlen(acCityCode));
         } else
            strcpy(acAddr2, "CA");
      }
   } else
      strcpy(acAddr2, "CA");
   memcpy(pOutbuf+OFF_S_ST, "CA", SIZ_S_ST);
   
   // Look up for zipcode
   lZip = atol(sMailAdr.Zip);
   if (iStrNo > 0 && iStrNo == sMailAdr.lStrNum && lZip > 90000)
   {
      // With the same street number, they are more likely the same address
      memcpy(pOutbuf+OFF_S_ZIP, sMailAdr.Zip, SIZ_S_ZIP);
      strcat(acAddr2, " ");
      strcat(acAddr2, sMailAdr.Zip);

      if (sMailAdr.Zip4[0] >= '0')
      {
         memcpy(pOutbuf+OFF_S_ZIP4, sMailAdr.Zip4, SIZ_S_ZIP4);
         strcat(acAddr2, "-");
         strcat(acAddr2, sMailAdr.Zip4);
      }

   }

   // Blank situs and HO occupied
   City2Code(sMailAdr.City, acCityCode);
   if (acAddr1[0] < '1' && *(pOutbuf+OFF_HO_FL) == '1' && 
      sMailAdr.lStrNum > 0 && acCityCode[0] > ' ')
   {
      // Copy mail to situs
      memcpy(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, (SIZ_M_STRNUM+SIZ_M_STR_SUB+SIZ_M_DIR+SIZ_M_STREET));
      Sfx2Code(sMailAdr.strSfx, acTmp1);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp1, SIZ_S_SUFF);
      memcpy(pOutbuf+OFF_S_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

      memcpy(pOutbuf+OFF_S_CITY, acCityCode, strlen(acCityCode));
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      
      memcpy(pOutbuf+OFF_S_ADDR_D, pOutbuf+OFF_M_ADDR_D, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, pOutbuf+OFF_M_CTY_ST_D, SIZ_M_CTY_ST_D);
   } else
   {
      iTmp = blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, strlen(acAddr2));
   }
}

/********************************* Sjx_MergeMAdr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************

void Sjx_MergeMAdr(char *pOutbuf)
{
   char     *pTmp, acTmp[256], acAddr1[64], acAddr2[64];
   int      iTmp, iStrNo;
   ADR_REC  sMailAdr;
   CString  strFld;

   // Clear old Mailing
   memset(pOutbuf+OFF_M_STRNUM, ' ', SIZ_M_ADDR);
   memset(pOutbuf+OFF_M_ADDR_D, ' ', SIZ_M_ADDRB_D);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, rsSecMstr.GetItem("SEC_STREET_ADDR"));

   // Parsing mail address
   if (memcmp(acAddr1, "     ", 5))
   {
      iStrNo = atoi(acAddr1);
      pTmp = acAddr1;
      if (iStrNo > 0)
      {
         while (pTmp = strchr(pTmp, ' '))
         {
            pTmp++;
            if (*pTmp >= '1')
               break;
            if (!memcmp(pTmp+1, "TH", 2) || 
                !memcmp(pTmp+1, "ST", 2) || 
                !memcmp(pTmp+1, "RD", 2))
               break;
         }

         parseAdr1N(&sMailAdr, pTmp);
         sprintf(sMailAdr.strNum, "%d", iStrNo);
         sMailAdr.lStrNum = iStrNo;
      } else
         strcpy(sMailAdr.strName, acAddr1);
      
      if (sMailAdr.lStrNum > 0)
         sprintf(acTmp, "%*d", SIZ_M_STRNUM, sMailAdr.lStrNum);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      strcpy(acAddr2, rsSecMstr.GetItem("SEC_CITY"));
      memcpy(pOutbuf+OFF_M_CITY, acAddr2, strlen(acAddr2));

      strcpy(acTmp, rsSecMstr.GetItem("SEC_STATE"));
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_M_ST, acTmp, SIZ_M_ST);
         strcat(acAddr2, " ");
         strcat(acAddr2, acTmp);
      }
   
      strcpy(acTmp, rsSecMstr.GetItem("SEC_ZIP_CODE"));
      if (acTmp[0] > ' ')
      {
         iTmp = atoi(acTmp);
         sprintf(sMailAdr.Zip, "%0.5d", iTmp);
         memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
         strcat(acAddr2, " ");
         strcat(acAddr2, sMailAdr.Zip);
      }

      strcpy(acTmp, rsSecMstr.GetItem("SEC_ZIP_CODE_SUB"));
      if (acTmp[0] > ' ')
      {
         iTmp = atoi(acTmp);
         sprintf(sMailAdr.Zip4, "%0.4d", iTmp);

         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
         strcat(acAddr2, "-");
         strcat(acAddr2, sMailAdr.Zip4);
      }
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));

   }
}

/******************************** Sjx_XlatUseCode ****************************
 *
 * Return pointer to the correct use code.
 *
 *****************************************************************************

char *Sjx_XlatUseCode(char *pCntyUse)
{
   int   iTmp;

   iTmp = 0;
   while (Sjx_UseTbl3[iTmp].acCntyUse[0])
      if (!memcmp(Sjx_UseTbl3[iTmp].acCntyUse, pCntyUse, 3))
         return (char *)&Sjx_UseTbl3[iTmp].acStdUse[0];
      else
         iTmp++;

   iTmp = 0;
   while (Sjx_UseTbl2[iTmp].acCntyUse[0])
      if (!memcmp(Sjx_UseTbl2[iTmp].acCntyUse, pCntyUse, 2))
         return (char *)&Sjx_UseTbl2[iTmp].acStdUse[0];
      else
         iTmp++;

   iTmp = 0;
   while (Sjx_UseTbl1[iTmp].acCntyUse[0])
      if (Sjx_UseTbl1[iTmp].acCntyUse[0] == *pCntyUse)
         return (char *)&Sjx_UseTbl1[iTmp].acStdUse[0];
      else
         iTmp++;

   return NULL;
}

/********************************* Sjx_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************

int Sjx_MergeRoll(char *pOutbuf, int iFlag)
{
   char     acTmp[256];
   long     lTmp; 
   int      iRet, iTmp, iYrAssd;
   CString  strFld, strTmp;

   iYrAssd = atoi(myCounty.acYearAssd);
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      strFld = rsSecMstr.GetItem("SEC_APN");
      iTmp = strFld.GetLength();
      if (iTmp < iApnLen)
      {
         iTmp = sprintf(acTmp, "%.*s%s", iApnLen-iTmp, "000", strFld);
         strFld = acTmp;
      }  
      memcpy(pOutbuf, strFld, iTmp);

      // Format APN
      iRet = formatApn(strFld.GetBuffer(0), acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(strFld.GetBuffer(0), acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // County code and Parcel Status
      memcpy(pOutbuf+OFF_CO_NUM, "39SJXA", 6);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // HO Exempt
      strFld = rsSecMstr.GetItem("SEC_OTHER_EX_CD");
      if (strFld == "0")
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      else
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'

      // Exempt amt
      if (strFld == "1")               // Home owner exempt
         strFld = rsSecMstr.GetItem("SEC_HO_EX_AMT");
      else
         strFld = rsSecMstr.GetItem("SEC_OTHER_EX_AMT");
      lTmp = atol(strFld);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Land
      strFld = rsSecMstr.GetItem("SEC_LAND_AMT");
      long lLand = atoi(strFld);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      strFld = rsSecMstr.GetItem("SEC_TOTAL_IMPR");
      long lImpr = atoi(strFld);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH...
      long lPers = atol(rsSecMstr.GetItem("SEC_PP_AMT"));
      long lPen = atol(rsSecMstr.GetItem("SEC_PP_PEN_AMT"));
      lTmp = lPen + lPers;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // TRA
      strFld = rsSecMstr.GetItem("SEC_TRA");
      memcpy(pOutbuf+OFF_TRA, strFld, strFld.GetLength());
   }

   // AG preserved

   // Legal - SEC_NARRATIVE
   strFld = rsSecMstr.GetItem("SEC_NARRATIVE");
   memcpy(pOutbuf+OFF_LEGAL, strFld, strFld.GetLength());

   // Owner
   strFld = rsSecMstr.GetItem("SEC_NAME_1");
   Sjx_MergeOwner(pOutbuf, strFld.GetBuffer(0));

   // Situs & Mailing addr
   Sjx_MergeAdr(pOutbuf);

   // Care of
   strFld = rsSecMstr.GetItem("SEC_CARE_OF");
   iTmp = strFld.GetLength();
   if (iTmp > SIZ_CARE_OF)
      iTmp = SIZ_CARE_OF;
   memcpy(pOutbuf+OFF_CARE_OF, strFld, iTmp);

   // UseCode
   strFld = rsSecMstr.GetItem("SEC_USE_CD");
   iTmp = atoi(strFld);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%0.3d", iTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, 3);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 3);
   }

   // BldgArea
   strFld = rsSecMstr.GetItem("SEC_TOTAL_LIVING");
   long lBldgArea = atol(strFld);
   if (lBldgArea > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgArea);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Acres
   strFld = rsSecMstr.GetItem("SEC_LAND_SIZE");
   unsigned long lLandSize = atol(strFld);
   unsigned long lAcres=0;
   if (lLandSize > 0)
   {
      // Land size is determined by Unit of size: 1=SQFT, 2=Acres
      strFld = rsSecMstr.GetItem("SEC_UNIT_OF_SIZE");
      iTmp = atoi(strFld);

      // For temporary we use BldgArea if SQFT is too small
      if (iTmp == 1 && lLandSize < 100 && lBldgArea > 100)
         lLandSize = lBldgArea;

      if (iTmp == 1)
      {
         lAcres = (lLandSize*1000)/SQFT_PER_ACRE;
      } else if (iTmp == 2)
      {
         lAcres = lLandSize*10;                 // to keep 3 decimal digit
         lLandSize = ((double)lLandSize * SQFT_PER_ACRE)/100;
      } 
#ifdef _DEBUG
      //if (lAcres < 0 || lLandSize < 0)
      //   iTmp = 0;
#endif
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lLandSize);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lAcres);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // YrBlt - SEC_RES_YR_BLT
   strFld = rsSecMstr.GetItem("SEC_RES_YR_BLT");
   iTmp = atoi(strFld);
   if (iTmp > 1800 && iTmp <= iYrAssd)
      memcpy(pOutbuf+OFF_YR_BLT, strFld, SIZ_YR_BLT);

   // Stories - SEC_RES_STORIES
   strFld = rsSecMstr.GetItem("SEC_RES_STORIES");
   iTmp = atoi(strFld);
   if (iTmp > 0)
   {
      //sprintf(acTmp, "%d", iTmp*10);
      sprintf(acTmp, "%d.0  ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Beds - SEC_RES_BEDROOM
   strFld = rsSecMstr.GetItem("SEC_RES_BEDROOM");
   iTmp = atoi(strFld);
   if (iTmp > 0 && iTmp < 100)
      memcpy(pOutbuf+OFF_BEDS, strFld, strFld.GetLength());

   // Baths - SEC_RES_BATHROOM
   strFld = rsSecMstr.GetItem("SEC_RES_BATHROOM");
   iTmp = atoi(strFld);
   //if (iTmp > 0 && iTmp < 100)
   if (iTmp > 0)
      memcpy(pOutbuf+OFF_BATH_F, strFld, strFld.GetLength());

   // Fire place - SEC_RES_FIREPLACE
   strFld = rsSecMstr.GetItem("SEC_RES_FIREPLACE");
   iTmp = atoi(strFld);
   if (iTmp > 0)
      memcpy(pOutbuf+OFF_FIRE_PL, strFld, strFld.GetLength());

   // Bldg class/shape - SEC_CLASS_SHAPE

   // A/C - SEC_AC_1_2 (Y/N)
   // This field may need translation if PQLookup is used
   strFld = rsSecMstr.GetItem("SEC_AC_1_2");
   if (strFld == "Y")
      *(pOutbuf+OFF_AIR_COND) = 'C';

   // Pool/Spa - SEC_POOL_SPA (P=Pool/PS=Pool/Spa/N/DB=In-ground/S=Spa/VP=Vynil/VS=Spa)
   strFld = rsSecMstr.GetItem("SEC_POOL_SPA");
   strcpy(acTmp, strFld);
   if (acTmp[0] == 'P' && acTmp[1] == 'S')
      *(pOutbuf+OFF_POOL) = 'C';       // Pool/Spa
   else if (acTmp[1] == 'B' || acTmp[1] == 'S')
      *(pOutbuf+OFF_POOL) = acTmp[1];
   else if (acTmp[0] == 'P' || acTmp[0] == 'S' || acTmp[0] == 'V')
      *(pOutbuf+OFF_POOL) = acTmp[0];

   // Garage - SEC_RES_GARAGE 
   // N=None/CN=None/CP=Carport/1G=I,1/2G=I,2=/3G=I,3
   strFld = rsSecMstr.GetItem("SEC_RES_GARAGE");
   strcpy(acTmp, strFld);
   if (acTmp[1] == 'G')
   {
      *(pOutbuf+OFF_PARK_TYPE) = 'I';     // attached garage
      *(pOutbuf+OFF_PARK_SPACE) = acTmp[0];
   } else if (strFld == "CP")
   {
      *(pOutbuf+OFF_PARK_TYPE) = 'C';
   } else
   {
      iTmp = atoi(strFld);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d", iTmp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, strlen(acTmp));
      }
   }

   // Recorded Doc - SEC_OR_1 (drop first 2 digits if matching two-digit year
   // of RD),SEC_RD_1,SEC_OR_1_TYPE
   // Don't call this function on monthly update because it will wipe out all
   // cumulative sale.  All new sales should be in SaleMst file already
   if (iFlag & CREATE_R01)
      Sjx_FmtRecDoc(pOutbuf);

   return 0;
}

/********************************* Sjx_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sjx_MergeRoll(char *pOutbuf, int iFlag)
{
   char     acTmp[512], acTmp1[256];
   long     lTmp, lHO_Exe, lOth_Exe; 
   int      iRet, iTmp;
   CString  strFld, strTmp, strAPN;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      strAPN = rsSecMstr.GetItem(acRollKey);
      iTmp = strAPN.GetLength();
      if (iTmp < iApnLen)
      {
         iTmp = sprintf(acTmp, "%.*s%s", iApnLen-iTmp, "000", strAPN);
         strAPN = acTmp;
      }  
      memcpy(pOutbuf, strAPN, iTmp);

      // Format APN
      iRet = formatApn(strAPN.GetBuffer(0), acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(strAPN.GetBuffer(0), acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code and Parcel Status
      memcpy(pOutbuf+OFF_CO_NUM, "39SJXA", 6);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      strFld = rsSecMstr.GetItem("SEC_LAND_AMT");
      long lLand = atoi(strFld);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      //strFld = rsSecMstr.GetItem("SEC_TOTAL_IMPR");
      strFld = rsSecMstr.GetItem("SEC_STRUCT_AMT");
      long lImpr = atoi(strFld);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: Fixture, PersProp, Homesite, TreeVines
      long lFixt = atol(rsSecMstr.GetItem("SEC_FE_AMT"));
      long lPP   = atol(rsSecMstr.GetItem("SEC_PP_AMT"));
      long lPen  = atol(rsSecMstr.GetItem("SEC_PP_PEN_AMT"));
      long lHS   = atol(rsSecMstr.GetItem("WM_HOMESITE_VALUE"));
      long lTV   = atol(rsSecMstr.GetItem("SEC_TV_AMT"));
      lTmp = lPP + lTV + lFixt +lPen;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_OTH_VALUE);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTH_VALUE);
         }
         if (lHS > 0)
         {
            sprintf(acTmp, "%d         ", lHS);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_OTH_VALUE);
         }
         if (lTV > 0)
         {
            sprintf(acTmp, "%d         ", lTV);
            memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_OTH_VALUE);
         }
         if (lPen > 0)
         {
            sprintf(acTmp, "%d         ", lPen);
            memcpy(pOutbuf+OFF_PENALTY_AMT, acTmp, SIZ_OTH_VALUE);
         }
      }

      // Gross total
      long lGross = lTmp+lLand+lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lGross);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exempt amt
      strFld = rsSecMstr.GetItem("SEC_HO_EX_AMT");
      lHO_Exe = atol(strFld);
      strFld = rsSecMstr.GetItem("SEC_OTHER_EX_AMT");
      lOth_Exe = atol(strFld);
      lTmp = lHO_Exe + lOth_Exe;
      if (lTmp > 0)
      {
         // Total exempt cannot be greater than total assessed
         if (lTmp > lGross)
         {
            LogMsg("* Overwrite EXE_TOTAL of %d with %d.  APN=%s", lTmp, lGross, strAPN);
            lTmp = lGross;
         }

         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

         if (lHO_Exe > 0)
            *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'

         // Exe Code
         strFld = rsSecMstr.GetItem("SEC_OTHER_EX_CD");
         if (!strFld.IsEmpty())
            memcpy(pOutbuf+OFF_EXE_CD1, strFld, strFld.GetLength());
      }
   }

   // TRA
   strFld = rsSecMstr.GetItem("SEC_TRA");
   lTmp = atol(strFld);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Alt Apn
   strAPN = rsSecMstr.GetItem("SEC_MH_REF_APN");
   iTmp = strAPN.GetLength();
   if (iTmp > 3 && iTmp < iApnLen)
   {
      iTmp = sprintf(acTmp, "%.*s%s", iApnLen-iTmp, "000", strAPN);
      strAPN = acTmp;
   }  
   if (iTmp == iApnLen)
      memcpy(pOutbuf+OFF_ALT_APN, strAPN, iApnLen);
   else
      memcpy(pOutbuf+OFF_ALT_APN, pOutbuf, iApnLen);

   // Legal - SEC_NARRATIVE
   strFld = rsSecMstr.GetItem("SEC_NARRATIVE");
   strFld.Replace('|', ' ');        // Remove vertical bar
   strcpy(acTmp, strFld);
   if (acTmp[0] > ' ')
   {
      iTmp = updateLegal(pOutbuf, acTmp);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;
   }

   // Owner
   strFld = rsSecMstr.GetItem("SEC_NAME_1");
   Sjx_MergeOwner(pOutbuf, strFld.GetBuffer(0));

   // Situs & Mailing addr
   Sjx_MergeAdr(pOutbuf);

   // Care of
   strFld = rsSecMstr.GetItem("SEC_CARE_OF");
   iTmp = strFld.GetLength();
   updateCareOf(pOutbuf, strFld.GetBuffer(0), iTmp);

   // UseCode
   strFld = rsSecMstr.GetItem("SEC_USE_CD");
   iTmp = atoi(strFld);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%0.3d", iTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, 3);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 3, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // BldgArea
   strFld = rsSecMstr.GetItem("SEC_TOTAL_LIVING");
   long lBldgArea = atol(strFld);
   if (lBldgArea > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgArea);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Acres
   strFld = rsSecMstr.GetItem("SEC_LAND_SIZE");
   unsigned long lLandSize = atol(strFld);
   unsigned long lAcres=0;
   if (lLandSize > 0)
   {
      // Land size is determined by Unit of size: 1=SQFT, 2=Acres
      strFld = rsSecMstr.GetItem("SEC_UNIT_OF_SIZE");
      iTmp = atoi(strFld);

      // For temporary we use BldgArea if SQFT is too small
      if (iTmp == 1 && lLandSize < 100 && lBldgArea > 100)
         lLandSize = lBldgArea;

      if (iTmp == 1)
      {
         lAcres = (lLandSize*1000)/SQFT_PER_ACRE;
      } else if (iTmp == 2)
      {
         lAcres = lLandSize*10;                 // to keep 3 decimal digit
         lLandSize = (long)(((double)lLandSize * SQFT_PER_ACRE)/100);
      } 
#ifdef _DEBUG
      //if (lAcres < 0 || lLandSize < 0)
      //   iTmp = 0;
#endif
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lLandSize);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lAcres);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // YrBlt - SEC_RES_YR_BLT
   strFld = rsSecMstr.GetItem("SEC_RES_YR_BLT");
   iTmp = atoi(strFld);
   if (iTmp > 1800 && iTmp <= myCounty.iYearAssd)
      memcpy(pOutbuf+OFF_YR_BLT, strFld, SIZ_YR_BLT);

   // Stories - SEC_RES_STORIES
   strFld = rsSecMstr.GetItem("SEC_RES_STORIES");
   iTmp = atoi(strFld);
   if (iTmp > 0)
   {
      //sprintf(acTmp, "%d", iTmp*10);
      sprintf(acTmp, "%d.0  ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Beds - SEC_RES_BEDROOM
   strFld = rsSecMstr.GetItem("SEC_RES_BEDROOM");
   iTmp = atoi(strFld);
   if (iTmp > 0 && iTmp < 100)
      memcpy(pOutbuf+OFF_BEDS, strFld, strFld.GetLength());

   // Full Baths - SEC_RES_BATHROOM
   strFld = rsSecMstr.GetItem("SEC_RES_BATHROOM");
   iTmp = atoi(strFld);
   if (iTmp > 0)
   {
      if (iTmp > 99)
         memcpy(pOutbuf+OFF_BATH_F, "99", 2);
      else
         memcpy(pOutbuf+OFF_BATH_F, strFld, strFld.GetLength());
   }

   // Half Baths - SEC_BATHROOM_HALF
   try
   {
      strFld = rsSecMstr.GetItem("SEC_BATHROOM_HALF");
      if (!strFld.IsEmpty())
      {
         iTmp = atoi(strFld);
         if (iTmp > 0)
         {
            if (iTmp > 99)
               memcpy(pOutbuf+OFF_BATH_H, "99", 2);
            else
               memcpy(pOutbuf+OFF_BATH_H, strFld, strFld.GetLength());
         }
      }
   } AdoCatch (e)
   {
      // Ignore this error
      iTmp = 0;      
      LogMsg("*** Bad data on SEC_BATHROOM_HALF.  Err = %s", e.ErrorMessage());
   }

   // Fire place - SEC_RES_FIREPLACE
   strFld = rsSecMstr.GetItem("SEC_RES_FIREPLACE");
   iTmp = atoi(strFld);
   if (iTmp > 0)
      memcpy(pOutbuf+OFF_FIRE_PL, strFld, strFld.GetLength());

   // Bldg class/shape - SEC_CLASS_SHAPE

   // A/C - SEC_AC_1_2 (Y/N)
   // This field may need translation if PQLookup is used
   strFld = rsSecMstr.GetItem("SEC_AC_1_2");
   if (strFld == "Y")
      *(pOutbuf+OFF_AIR_COND) = 'C';   // Central
   else if (strFld == "N")
      *(pOutbuf+OFF_AIR_COND) = 'N';   // None

   // Pool/Spa - SEC_POOL_SPA (P=Pool/PS=Pool/Spa/N/DB=In-ground/S=Spa/VP=Vynil/VS=Spa)
   strFld = rsSecMstr.GetItem("SEC_POOL_SPA");
   strcpy(acTmp, strFld);
   if (acTmp[0] == 'P' && acTmp[1] == 'S')
      *(pOutbuf+OFF_POOL) = 'C';       // Pool/Spa
   else if (acTmp[1] == 'B' || acTmp[1] == 'S')
      *(pOutbuf+OFF_POOL) = acTmp[1];
   else if (acTmp[0] == 'P' || acTmp[0] == 'S' || acTmp[0] == 'V')
      *(pOutbuf+OFF_POOL) = acTmp[0];

   // Garage - SEC_RES_GARAGE 
   // N=None/CN=None/CP=Carport/1G=I,1/2G=I,2=/3G=I,3
   strFld = rsSecMstr.GetItem("SEC_RES_GARAGE");
   strcpy(acTmp, strFld);
   if (acTmp[1] == 'G')
   {
      *(pOutbuf+OFF_PARK_TYPE) = 'I';     // attached garage
      *(pOutbuf+OFF_PARK_SPACE) = acTmp[0];
   } else if (strFld == "CP")
   {
      *(pOutbuf+OFF_PARK_TYPE) = 'C';
   } else
   {
      iTmp = atoi(strFld);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d", iTmp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, strlen(acTmp));
      }
   }

   // Recorded Doc - SEC_OR_1 (drop first 2 digits if matching two-digit year
   // of RD),SEC_RD_1,SEC_OR_1_TYPE
   // Don't call this function on monthly update because it will wipe out all
   // cumulative sale.  All new sales should be in SaleMst file already
   //if (iFlag & CREATE_R01)
   //   Sjx_FmtRecDoc(pOutbuf);

   // Update transfer
   Sjx_MergeXfer(pOutbuf);

   // Set prop8 flag
   strFld = rsSecMstr.GetItem("SEC_REVW_YR");
   lTmp = atol(strFld);
   if (lTmp >= myCounty.iYearAssd)
   {
      strFld = rsSecMstr.GetItem("SEC_REVW_CODE");
      lTmp = atol(strFld);
      if (lTmp == 65 || lTmp == 75 || lTmp == 79 || lTmp == 80 || lTmp == 85)
         *(pOutbuf+OFF_PROP8_FLG) = 'Y';
   }

   // Tax code - Taxability - 1-byte
   strFld = rsSecMstr.GetItem("SEC_TAXABILITY");
   if (strFld.GetAt(0) > '0')
   {
      *(pOutbuf+OFF_TAX_CODE) = strFld.GetAt(0);

      // Full exempt - government properties
      if ('2' == strFld.GetAt(0))
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }

   // Units - SEC_NO_UNITS
   if (bUnits)
   {
      strFld = rsSecMstr.GetItem("SEC_NO_UNITS");
      iTmp = atoi(strFld);
      if (iTmp > 0 && iTmp < 1000)
         memcpy(pOutbuf+OFF_UNITS, strFld, strFld.GetLength());
   }

   return 0;
}

/******************************** Sjx_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Sjx_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   int      iRet=0, iTmp;
   SALE_REC *pSale;

   // Get rec
   if (!pRec)
      pRec = fgets(acRec, 512, fdSale);

   do 
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
         pRec = fgets(acRec, 512, fdSale);
   } while (iTmp > 0);

   if (iTmp)
      return 1;

   pSale = (SALE_REC *)&acRec[0];
   do
   {
      //iTmp = 0;
      //while (pSale->acDocNum[iTmp] == ' ' && iTmp < 7)
      //   pSale->acDocNum[iTmp++] = '0';

      iRet = MergeSale1(pSale, pOutbuf, true);

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, pSale->acApn, iApnLen));

   lSaleMatch++;

   return 0;
}

/***************************** CreateSjxSale *********************************
 *
 * Extract sale data from SALEMST.MDB and output to SALE_EXP.DAT
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int CreateSjxSale(int iFlag)
{
   SALE_REC SaleRec;
   char     acOutFile[_MAX_PATH], acSaleOut[_MAX_PATH];
   char     acTmp[256], acTmp1[32];

   FILE     *fdOut;

   int      iTmp;
   long     lCnt=0, lSApn, lTmp;
   CString  strFld;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open sale file
   LogMsg("Open Sale table %s in %s", acSaleTbl, acSalesMdb);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, acSalesMdb);

      bool bRet = hSaleMstr.Connect(acTmp);
      if (bRet)
      {
         sprintf(acTmp, "SELECT * FROM %s", acSaleTbl);
         LogMsg("%s", acTmp);
         rsSaleMstr.Open(hSaleMstr, acTmp);
      }
   }
   AdoCatch(e)
   {
      LogMsg("***** Error open %s db: %s", acSalesMdb, ComError(e));
      return -2;
   }

   // Open Output file
   sprintf(acSaleOut, acESalTmpl, "SJX", "TMP");
   sprintf(acOutFile, acESalTmpl, "SJX", "DAT");

   LogMsg("Open output file %s", acSaleOut);
   fdOut = fopen(acSaleOut, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating GrGr output file: %s\n", acSaleOut);
      return -2;
   }

   // Loop through record set
   while (rsSaleMstr.next())
   {  
      memset((void *)&SaleRec, ' ', sizeof(SALE_REC));

      strFld = rsSaleMstr.GetItem(acSaleKey);
      lSApn = atol(strFld);

      // APN
      strcpy(acTmp, rsSaleMstr.GetItem("SALE_APN"));
      iTmp = strlen(acTmp);
      if (iTmp < iApnLen)
         iTmp = sprintf(acTmp, "%.*s%s", iApnLen-iTmp, "0000", strFld);

      memcpy(SaleRec.acApn, acTmp, iTmp);

      // Sale date
      strcpy(acTmp, rsSaleMstr.GetItem("SALE_DATE"));
      iTmp = sprintf(acTmp1, "%.4s%.2s%.2s", &acTmp[4], acTmp, &acTmp[2]);
      memcpy(SaleRec.acDocDate, acTmp1, iTmp);
      lTmp = atol(acTmp1);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;

      // Doc# - Drop first 2 digit if it is year
      strFld = rsSaleMstr.GetItem("SALE_OR");

      // Skip test record
      if (strFld.Left(4) == "TEST")
         continue;

      // Reformat DocNum
      iTmp = strFld.GetLength();
      if (iTmp <= 7 && strFld.GetAt(0) <= '9')
      {
         memset(acTmp, '0', 8-iTmp);
         strcpy((char *)&acTmp[8-iTmp], strFld);
         memcpy(SaleRec.acDocNum, acTmp, 8);
      } else
         memcpy(SaleRec.acDocNum, strFld, strFld.GetLength());

      // Sale type may need translation
      strFld = rsSaleMstr.GetItem("SALE_TYPE");
      //memcpy(SaleRec.acDocType, strFld, strFld.GetLength());
      
      iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], strFld, iNumDeeds);
      if (iTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", iTmp);
         memcpy(SaleRec.acDocType, acTmp, iTmp);
      }
      
      // Use Stamp Amt to calculate sale price
      strFld = rsSaleMstr.GetItem("SALE_STAMP_AMT");
      lTmp = (long)(atof(strFld)*SALE_FACTOR);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%*ld", SALE_SIZ_SALEPRICE, lTmp);
         memcpy(SaleRec.acSalePrice, acTmp, iTmp);
      }

      strFld = rsSaleMstr.GetItem("SALE_CODE");
      memcpy(SaleRec.acSaleCode, strFld, strFld.GetLength());

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   rsSaleMstr.Close();
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   // Sort output file and dedup
   LogMsg("Sorting %s to %s.", acSaleOut, acOutFile);

   // Sort on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
   lCnt = sortFile(acSaleOut, acOutFile, acTmp);

   // Update accumulated sale file
   if (lCnt > 0)
   {
      sprintf(acOutFile, acESalTmpl, "SJX", "sls");      // accummulated sale file
      LogMsg("Append %s to %s.", acSaleOut, acOutFile);
      iTmp = appendTxtFile(acSaleOut, acOutFile);
   } else
      iTmp = -1;

   LogMsg("Create Sale export completed.");
   return iTmp;
}

/***************************** Sjx_CreateSale ********************************
 *
 * Extract sale data from SALEMST.MDB and output to SJX_SALE.DAT
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sjx_CreateSale(void)
{
   SCSAL_REC SaleRec;
   char      acOutFile[_MAX_PATH], acSaleOut[_MAX_PATH];
   char      acTmp[256], acTmp1[32];
   int       iTmp;
   long      lCnt=0, lSApn, lTmp, lPrice;
   CString   strFld;
   FILE      *fdOut;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open sale file
   LogMsg("Open Sale table %s in %s", acSaleTbl, acSalesMdb);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, acSalesMdb);

      bool bRet = hSaleMstr.Connect(acTmp);
      if (bRet)
      {
         sprintf(acTmp, "SELECT * FROM %s", acSaleTbl);
         LogMsg("%s", acTmp);
         rsSaleMstr.Open(hSaleMstr, acTmp);
      }
   }
   AdoCatch(e)
   {
      LogMsg("***** Error open %s db: %s", acSalesMdb, ComError(e));
      return -2;
   }

   // Open Output file
   sprintf(acSaleOut, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");

   LogMsg("Open output file %s", acSaleOut);
   fdOut = fopen(acSaleOut, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acSaleOut);
      return -2;
   }

   // Loop through record set
   while (rsSaleMstr.next())
   {  
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      strFld = rsSaleMstr.GetItem(acSaleKey);
      lSApn = atol(strFld);

      // APN
      strcpy(acTmp, rsSaleMstr.GetItem("SALE_APN"));
      iTmp = strlen(acTmp);
      if (iTmp < iApnLen)
         iTmp = sprintf(acTmp, "%.*s%s", iApnLen-iTmp, "0000", strFld);

      memcpy(SaleRec.Apn, acTmp, iTmp);

      // Sale date
      strcpy(acTmp, rsSaleMstr.GetItem("SALE_DATE"));
      iTmp = sprintf(acTmp1, "%.4s%.2s%.2s", &acTmp[4], acTmp, &acTmp[2]);
      memcpy(SaleRec.DocDate, acTmp1, iTmp);
      lTmp = atol(acTmp1);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;

      // Doc# - Drop first 2 digit if it is year
      strFld = rsSaleMstr.GetItem("SALE_OR");

      // Skip test record
      if (strFld.Left(4) == "TEST")
         continue;

      // Reformat DocNum
      iTmp = strFld.GetLength();
      if (iTmp <= 7 && strFld.GetAt(0) <= '9')
      {
         memset(acTmp, '0', 8-iTmp);
         strcpy((char *)&acTmp[8-iTmp], strFld);
         memcpy(SaleRec.DocNum, acTmp, 8);
      } else
         memcpy(SaleRec.DocNum, strFld, strFld.GetLength());

      // Use Stamp Amt to calculate sale price
      strFld = rsSaleMstr.GetItem("SALE_STAMP_AMT");
      lPrice = (long)(atof(strFld)*SALE_FACTOR);
      if (lPrice > 0)
      {
         iTmp = sprintf(acTmp, "%*ld", SALE_SIZ_SALEPRICE, lPrice);
         memcpy(SaleRec.SalePrice, acTmp, iTmp);
         memcpy(SaleRec.StampAmt, strFld, strFld.GetLength());
      }

      // Sale type may need translation
      strFld = rsSaleMstr.GetItem("SALE_TYPE"); 
      if (!strFld.IsEmpty())
      {
         iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], strFld, iNumDeeds);
         if (iTmp > 0)
         {
            if (strFld.Left(2) == "MS")
            {
               if (!lPrice)
                  SaleRec.NoneSale_Flg = 'Y';
               else
               {
                  iTmp = 67;  // Tax Deed
                  LogMsg("MS->Tax Deed : %.12s", SaleRec.DocNum);
               }
            }
            iTmp = sprintf(acTmp, "%d   ", iTmp);
            memcpy(SaleRec.DocType, acTmp, iTmp);
         } else
            LogMsg("*** Unknown Deed Type %s", strFld);
      }
      
      // Sale code
      strFld = rsSaleMstr.GetItem("SALE_CODE");
      if (lPrice > 0 && !strFld.IsEmpty() && isalpha(strFld.GetAt(0)))
         SaleRec.SaleCode[0] = strFld.GetAt(0);

      // % xfer
      strFld = rsSaleMstr.GetItem("SALE_PCT_XFER");
      memcpy(SaleRec.PctXfer, strFld, strFld.GetLength());
      iTmp = atol(strFld);
      if (iTmp < 100)
         SaleRec.MultiSale_Flg = 'Y';

      // NBH code
      strFld = rsSaleMstr.GetItem("SALE_NEIGH_CD");
      memcpy(SaleRec.Nbh_Code, strFld, strFld.GetLength());

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   rsSaleMstr.Close();
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   // Sort output file and dedup
   LogMsg("Sorting %s to %s.", acSaleOut, acOutFile);

   // Sort on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) F(TXT) DUPO(1,34) ");
   lCnt = sortFile(acSaleOut, acOutFile, acTmp);

   // Update accumulated sale file
   if (lCnt > 0)
   {
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");      // accummulated sale file
      LogMsg("Append %s to %s.", acSaleOut, acOutFile);
      iTmp = appendTxtFile(acSaleOut, acOutFile);
      if (!iTmp)
      {
         iTmp = SetMultiSale(acOutFile, acTmp, myCounty.acCntyCode);

         // Resort to dedup SLS file
         //sprintf(acSaleOut, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Tmp");
         //lCnt = sortFile(acOutFile, acSaleOut, acTmp);
         //if (lCnt > 0)
         //{
         //   iTmp = remove(acOutFile);
         //   if (!iTmp)
         //      iTmp = rename(acSaleOut, acOutFile);
         //   else
         //      LogMsg("***** Error removing %s.  Please rerun with -Xs option", acOutFile);
         //}
      }
   } else
      iTmp = -1;

   LogMsg("Create Sale export completed.");
   return iTmp;
}

/********************************* Sjx_Load_LDR *****************************
 *
 *
 ****************************************************************************/

int Sjx_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     acBuf[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;

   int      iRet, iTmp;
   long     lCnt, lRApn;
   DWORD    nBytesWritten;
   BOOL     bRet;
   CString  strRApn, strSApn;

   LogMsg("Loading LDR for SJX");
   sprintf(acOutFile, acRawTmpl, "SJX", "SJX", "R01");

   // Open roll file
   LogMsg0("Open Roll file %s", acSecuredMdb);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, acSecuredMdb);

      bRet = hSecMstr.Connect(acTmp);
      if (bRet)
      {
         sprintf(acTmp, "SELECT * FROM %s ORDER BY %s", acRollTbl, acRollKey);
         LogMsg("%s", acTmp);
         rsSecMstr.Open(hSecMstr, acTmp);
      }
   }
   AdoCatch(e)
   {
      LogMsg("***** Error open SecMstr db: %s", ComError(e));
      return -1;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   lLDRRecCount=iRet=iNoMatch=iBadCity=iBadSuffix=lCnt=0;

   // Loop through and bill every one of them
   while (rsSecMstr.next())
   {
      strRApn = rsSecMstr.GetItem(acRollKey);
      lRApn = atol(strRApn);

      if (lRApn == 99999)
         continue;
      lLDRRecCount++;

      // Create new R01 record
      iTmp = Sjx_MergeRoll(acBuf, CREATE_R01|CREATE_LIEN);
#ifdef _DEBUG
      //if (!memcmp(acBuf, "00516011", 8))
      //   iTmp = 0;
#endif

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error writing to output file at record %d\n", lCnt);
         iRet = WRITE_ERR;
         break;
      }
   }


   // Close record set
   rsSecMstr.Close();
   
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/********************************* Sjx_Load_Roll ******************************
 *
 *
 ******************************************************************************/

int Sjx_Load_Roll(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bCont;
   long     lRet=0, lCnt=0, lRApn, lS01Apn;
   CString  strRApn;

   LogMsg("Loading roll update for SJX");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acSecuredMdb);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, acSecuredMdb);

      bRet = hSecMstr.Connect(acTmp);
      if (bRet)
      {
         sprintf(acTmp, "SELECT * FROM %s ORDER BY %s", acRollTbl, acRollKey);
         LogMsg("%s", acTmp);
         rsSecMstr.Open(hSecMstr, acTmp);
         bCont = rsSecMstr.next();
         strRApn = rsSecMstr.GetItem(acRollKey);
         lRApn = atol(strRApn);
      }
   } AdoCatch(e)
   {
      LogMsg("***** Error open SecMstr db: %s", ComError(e));
      return -1;
   }

   // Check if SEC_NO_UNITS field exist
   try
   {
      strRApn = rsSecMstr.GetItem("SEC_NO_UNITS");
      bUnits = true;
   } AdoCatch(e)
   {
      bUnits = false;
      LogMsg("*** Bad data on SEC_NO_UNITS: %s", ComError(e));
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error in opening input file: %s", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error in creating output file: %s", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (bCont)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;         // EOF

      lS01Apn = atoin(acBuf, iApnLen);

Sjx_Reload_Roll:
      
      // Check roll record
      if (lS01Apn == lRApn)
      {
         // Merge roll data
         iRet = Sjx_MergeRoll(acBuf, UPDATE_R01);
         iRollUpd++;

         // Read next roll record
         bCont = rsSecMstr.next();
         if (bCont)
         {
            strRApn = rsSecMstr.GetItem(acRollKey);
            lRApn = atol(strRApn);
         }
      } else if (lS01Apn > lRApn)       // Roll not match, new roll record
      {
         LogMsg0("*** New roll record : %0.*d (%d) ***", iApnLen, lRApn, lCnt);

         // Create new R01 record
         iRet = Sjx_MergeRoll(acRec, CREATE_R01|CLEAR_R01);
         iNewRec++;

         iTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (iTmp > lLastRecDate && lLastRecDate < lToday)
            lLastRecDate = iTmp;

         // Write to output file
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Read next roll record
         bCont = rsSecMstr.next();
         if (bCont)
         {
            strRApn = rsSecMstr.GetItem(acRollKey);
            lRApn = atol(strRApn);
            goto  Sjx_Reload_Roll;
         }
      } else
      {
         // Record may be retired - drop it
         LogMsg0("*** Roll not match (retired record?) : R01->%.*s ***", iApnLen, acBuf);
         iRetiredRec++;
         continue;
      }

      iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (iTmp > lLastRecDate && lLastRecDate < lToday)
         lLastRecDate = iTmp;

      // Write to output file
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close record set
   rsSecMstr.Close();

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************** ConvertSale ******************************
 *
 * Convert history sale to SALE_REC format.
 *
 ******************************************************************************/

int ConvertSale(void)
{
   char     acBuf[512], acTmp[32];
   char     *pTmp;

   FILE     *fdCSale;
   SALE_REC *pSale = (SALE_REC *)&acBuf[0];

   long     lCnt=0, iTmp;

   // Open cumulative file
   LogMsg("Open cumulative sale file %s", acCSalFile);
   fdCSale = fopen(acCSalFile, "r");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acCSalFile);
      return 2;
   }

   LogMsg("Open output sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "w");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acSaleFile);
      return 2;
   }

   while (!feof(fdCSale))
   {
      if (!(pTmp = fgets(acBuf, 512, fdCSale)))
         break;
      if (pTmp = strchr(acBuf, 10))
         *pTmp = 0;
      blankPad(acBuf, sizeof(SALE_REC));

      // Reformat DocNum
      if (pSale->acDocNum[0] == ' ')
      {
         iTmp = atoin(pSale->acDocNum, 8);
         if (iTmp > 0)
         {
            sprintf(acTmp, "%.8d", iTmp);
            memcpy(pSale->acDocNum, acTmp, 8);
         }
      }

      pSale->CRLF[0] = '\n';
      pSale->CRLF[1] = 0;
      
      // Write to output file
      fputs(acBuf, fdSale);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   fclose(fdSale);
   fclose(fdCSale);

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/**************************** Sjx_CreateLienRec *****************************
 *
 * Format lien extract record.  
 * If APN is 99999, ignore it and return 1.  Otherwise return 0.
 *
 ****************************************************************************/

int Sjx_CreateLienRec(char *pOutbuf)
{
   int      lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;
   CString  strFld;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   strFld = rsSecMstr.GetItem(acRollKey);
   if (strFld == "99999")
      return 1;

   lTmp = strFld.GetLength();
   if (lTmp < myCounty.iApnLen)
   {
      lTmp = sprintf(acTmp, "%.*s%s", myCounty.iApnLen-lTmp, "000", strFld);
      strFld = acTmp;
   }
   memcpy(pLienExtr->acApn, strFld, lTmp);

   // TRA
   strFld = rsSecMstr.GetItem("SEC_TRA");
   lTmp = atoi(strFld);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // HO Exempt
   pLienExtr->acHO[0] = '2';      // 'N'

   // Exempt amt
   strFld = rsSecMstr.GetItem("SEC_HO_EX_AMT");
   long lHO_Exe = atol(strFld);
   strFld = rsSecMstr.GetItem("SEC_OTHER_EX_AMT");
   long lOth_Exe = atol(strFld);
   lTmp = lHO_Exe + lOth_Exe;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);

      if (lHO_Exe > 0)
         pLienExtr->acHO[0] = '1';      // 'Y'

      // Exe Code
      strFld = rsSecMstr.GetItem("SEC_OTHER_EX_CD");
      if (!strFld.IsEmpty())
         memcpy(pLienExtr->acExCode, strFld, strFld.GetLength());
   }

   // Land
   strFld = rsSecMstr.GetItem("SEC_LAND_AMT");
   long lLand = atoi(strFld);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   strFld = rsSecMstr.GetItem("SEC_STRUCT_AMT");
   long lImpr = atoi(strFld);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, Homesite, TreeVines
   long lFixt = atol(rsSecMstr.GetItem("SEC_FE_AMT"));
   long lPP   = atol(rsSecMstr.GetItem("SEC_PP_AMT"));
   long lHS   = atol(rsSecMstr.GetItem("WM_HOMESITE_VALUE"));
   long lTV   = atol(rsSecMstr.GetItem("SEC_TV_AMT"));
   lTmp = lPP + lTV + lFixt;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lHS > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lHS);
         memcpy(pLienExtr->extra.Sjx.HomeSite, acTmp, SIZ_LIEN_FIXT);
      }
      if (lTV > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTV);
         memcpy(pLienExtr->extra.Sjx.Tree, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Set prop8 flag
   lTmp = atol(rsSecMstr.GetItem("SEC_REVW_YR"));
   if (lTmp >= myCounty.iYearAssd)
   {
      lTmp = atol(rsSecMstr.GetItem("SEC_REVW_CODE"));
      if (lTmp == 65 || lTmp == 75 || lTmp == 79 || lTmp == 80 || lTmp == 85)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
   }


   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************** Sjx_ExtrLien ******************************
 *
 * Extract lien data from ???_lien.csv
 *
 ****************************************************************************/

int Sjx_ExtrLien(LPCSTR pCnty)
{
   char     acBuf[1024];
   char     acOutFile[_MAX_PATH], acTmp[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   bool     bRet;
   CString  strRApn;
   FILE     *fdLienExt;

   LogMsgD("\nExtract lien roll for %s", pCnty);

   // Open roll Table
   LogMsg("Open Roll table %s", acRollTbl);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, acSecuredMdb);

      bRet = hSecMstr.Connect(acTmp);
      if (bRet)
      {
         sprintf(acTmp, "SELECT * FROM %s ORDER BY %s", acRollTbl, acRollKey);
         LogMsg("%s", acTmp);
         rsSecMstr.Open(hSecMstr, acTmp);
      }
   } AdoCatch(e)
   {
      LogMsg("***** Error opening %s: %s", acSecuredMdb, ComError(e));
      return -1;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   iRet = 0;
   while (rsSecMstr.next())
   {
      strRApn = rsSecMstr.GetItem(acRollKey);

      if (strRApn.IsEmpty())
         break;

#ifdef _DEBUG
      //if (strRApn == "021091001000")
      //   iTmp = 0;
#endif

      // Create new record
      iRet = Sjx_CreateLienRec(acBuf);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close record set
   rsSecMstr.Close();

   if (fdLienExt)
      fclose(fdLienExt);

   LogMsgD("\nTotal output records          : %u", iNewRec);
   LogMsgD("Total output records processed: %u", lCnt);

   return 0;
}

/******************************** Sjx_ExtrLien ******************************
 *
 * Extract lien data from ???_lien.csv
 *
 ****************************************************************************/

int Sjx_ExtractProp8(LPCSTR pProp8File)
{
   char     acOutFile[_MAX_PATH], acTmp[_MAX_PATH];
   int      iNewRec=0, lCnt=0, iTmp;
   bool     bRet;
   CString  strRApn;
   FILE     *fdExt;

   LogMsgD("Extract prop8  for %s", myCounty.acCntyCode);
   LogMsg("Open prop8 file %s", acSecuredMdb);

   // Open roll Table
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, pProp8File);
      LogMsg("Open Roll table %s", acTmp);

      bRet = hSecMstr.Connect(acTmp);
      if (bRet)
      {
         sprintf(acTmp, "SELECT %s FROM %s WHERE sec_revw_code in ('65','75','79','80','85') AND sec_revw_yr='%s' ORDER BY %s", acRollKey, acRollTbl, myCounty.acYearAssd, acRollKey);
         LogMsg("%s", acTmp);
         rsSecMstr.Open(hSecMstr, acTmp);
      }
   } AdoCatch(e)
   {
      LogMsg("***** Error opening %s: %s", acSecuredMdb, ComError(e));
      return -1;
   }

   // Create prop8 extract
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd, "P8");
   LogMsg("Open output file %s", acOutFile);
   fdExt = fopen(acOutFile, "w");
   if (fdExt == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output header
   fputs("APN\n", fdExt);

   while (rsSecMstr.next())
   {
      strRApn = rsSecMstr.GetItem(acRollKey);

      if (strRApn.IsEmpty())
         break;

#ifdef _DEBUG
      //if (strRApn == "021091001000")
      //   iTmp = 0;
#endif

      if (strRApn != "99999")
      {
         iTmp = strRApn.GetLength();
         if (iTmp < myCounty.iApnLen)
            sprintf(acTmp, "%.*s%s\n", myCounty.iApnLen-iTmp, "000", strRApn);
         else
            sprintf(acTmp, "%s\n", strRApn);
            
         // Write to output
         fputs(acTmp, fdExt);
         iNewRec++;
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close record set
   rsSecMstr.Close();

   if (fdExt)
      fclose(fdExt);

   LogMsgD("\nTotal output records          : %u", iNewRec);
   LogMsgD("Total records processed       : %u", lCnt);

   return 0;
}

/*********************************** loadSjx ********************************
 *
 * To load lien date data:
 *    -CSJX -L -Xl -Ms -X8
 * To load update roll:
 *    -CSJX -U -Xs
 *
 ****************************************************************************/

int loadSjx(int iLoadFlag, int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   // Load Deed xref table
   iRet = GetIniString(myCounty.acCntyCode, "DeedXref", "", acTmp, _MAX_PATH, acIniFile);
   if (iRet == 0)
      GetIniString("System", "DeedXref", "", acTmp, _MAX_PATH, acIniFile);
   iNumDeeds = LoadXrefTable(acTmp, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

   if (bLoadTax)
   {
      TC_SetDateFmt(MM_DD_YYYY_1);
      iRet = Load_TC(myCounty.acCntyCode, bTaxImport);
   }

   if (!iLoadFlag)
      return iRet;

   // Convert sale data
   // One time use only
   //if (bConvSale)
   //{
      // Convert old cum sale format to standard cumsale format
      // Prepad DocNum with 0 - Use type 3
   //   iRet = convertSaleData(myCounty.acCntyCode, acCSalFile, 3);
   //   if (!iLoadFlag)
   //      return 0;
   //}

   // Extract cum sale from O01 file
   //if (iLoadFlag & EXTR_CSAL)    // -Xc
   //{
   //   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   //   sprintf(acTmp, acRawTmpl, "SJX", "SJX", "O01");
   //   ExtrHSale(acTmp, acCSalFile);
   //}

   // Fix history sale 
   //sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   //iRet = FixCumSale(acCSalFile, SALE_FLD_DOCTYPE);

   // Remove salecode if sale price is 0 - 20161229
   iRet = FixSCSal(acCSalFile, SALE_FIX_REMSCODE);

   // Extract prop8 flag to text file
   if (lOptProp8 & MYOPT_EXT)                   // -X8 
   {
      GetIniString(myCounty.acCntyCode, "Prop8File", "", acTmp, _MAX_PATH, acIniFile);
      if (_access(acTmp, 0))
      {
         strcpy(acTmp, acSecuredMdb);    
         LogMsg("*** No prop8 file specified, use current roll file: %s", acTmp);
      }
      iRet = Sjx_ExtractProp8(acTmp);
   }

   // Extract lien
   if (iLoadFlag & EXTR_LIEN)
      iRet = Sjx_ExtrLien(myCounty.acCntyCode);

   // Extract sale (from salemstr) 
   if (iLoadFlag & EXTR_SALE)                   // -Xs
   {
      iRet = Sjx_CreateSale();
      if (iRet)
         return iRet;

      iLoadFlag |= MERG_CSAL;
   }

   // Load roll file (from secured master)
   if (iLoadFlag & LOAD_LIEN)
   {
      iRet = Sjx_Load_LDR(iSkip);               // Create lien roll R01
   } else if (iLoadFlag & LOAD_UPDT)
   {
      iRet = Sjx_Load_Roll(iSkip);              // Create update roll R01
      if (!iRet)
         bCreateUpd = true;
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )       // -Ms
   {
      // Apply Sjx_Sale.sls to R01 file, not update Transfer
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCSALREC);
   }

   // Fix TRA
   if (bFixTRA)
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   // Merge other values
   if (!iRet && bMergeOthers)
   {
      sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmp, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmp, GRP_SJX, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmp);
   }

   return iRet;
}