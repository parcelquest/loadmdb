
#if !defined(AFX_LOADMDB_H__36608E89_813F_463E_81EF_5D51E87E84CA__INCLUDED_)
#define AFX_LOADMDB_H__36608E89_813F_463E_81EF_5D51E87E84CA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"

#define  MAX_SUFFIX     256
#define  MAX_RECSIZE    4096
#define  MAX_FLD_TOKEN  256

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004
#define  NOTFOUND_ERR   0xF0000008
#define  BADRECSIZE_ERR 0xF0000010

#define  UPDATE_R01     0
#define  CREATE_R01     1
#define  CREATE_LIEN    2
#define  CLEAR_R01      4
/*
#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_GRGR      0x00200000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_EXE       0x00000200
#define  EXTR_ROLL      0x00000020

#define  MERG_LIEN      0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_SALE      0x00040000
#define  MERG_ATTR      0x00004000
*/
#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000
#define  LOAD_ASSR      0x00000100
#define  LOAD_LAND      0x00000010
#define  LOAD_GREC      0x00000001

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_GRGR      0x00200000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_EXE       0x00000200
#define  EXTR_ROLL      0x00000020
#define  EXTR_ISAL      0x00000002
#define  EXTR_SADR      0x80000000
#define  EXTR_XSAL      0x00000008

#define  MERG_LIEN      0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_SALE      0x00040000
#define  MERG_ATTR      0x00004000
#define  MERG_SADR      0x00000400

#define  UPDT_XSAL      0x08000000
#define  UPDT_SALE      0x00080000
#define  UPDT_CHAR      0x00008000
#define  UPDT_ASSR      0x00000800

#define  MYOPT_SET      0x10000000
#define  MYOPT_UDB      0x01000000
#define  MYOPT_EXT      0x00100000
#define  MYOPT_OP1      0x00100000
#define  MYOPT_OP2      0x00010000
#define  MYOPT_OP3      0x00001000
#define  MYOPT_OP4      0x00000100
#define  MYOPT_OP5      0x00000010
#define  MYOPT_OP6      0x00000001

#define  MB_EXE_STATUS  0
#define  MB_EXE_ASMT    1
#define  MB_EXE_CODE    2
#define  MB_EXE_HOEXE   3
#define  MB_EXE_EXEAMT  4
#define  MB_EXE_EXEPCT  5

#define  MB_ROLL_ASMT                 0
#define  MB_ROLL_FEEPARCEL            1
#define  MB_ROLL_TRA                  2
#define  MB_ROLL_LEGAL                3
#define  MB_ROLL_ZONING               4
#define  MB_ROLL_USECODE              5
#define  MB_ROLL_NBHCODE              6
#define  MB_ROLL_ACRES                7
#define  MB_ROLL_DOCNUM               8
#define  MB_ROLL_DOCDATE              9
#define  MB_ROLL_TAXABILITY           10
#define  MB_ROLL_OWNER                11

#define  MB_ROLL_CAREOF               12
#define  MB_ROLL_DBA                  13
#define  MB_ROLL_M_ADDR               14
#define  MB_ROLL_M_CITY               15
#define  MB_ROLL_M_ST                 16
#define  MB_ROLL_M_ZIP                17

// This section overlays previous 6 fields
#define  MB_ROLL_S_STRNO              12
#define  MB_ROLL_S_DIR                13
#define  MB_ROLL_S_STRNAME            14
#define  MB_ROLL_S_CITY               15
#define  MB_ROLL_S_ST                 16
#define  MB_ROLL_S_ZIP                17

#define  MB_ROLL_LAND                 18
#define  MB_ROLL_HOMESITE             19
#define  MB_ROLL_IMPR                 20
#define  MB_ROLL_GROWING              21
#define  MB_ROLL_FIXTRS               22

#define  MB_ROLL_FIXTR_RP             23
#define  MB_ROLL_FIXTR_BUS            24

#define  MB_ROLL_PERSPROP             23  // New script
#define  MB_ROLL_BUSPROP              24

#define  MB_ROLL_PPMOBILHOME          25
#define  MB_ROLL_M_ADDR1              26
#define  MB_ROLL_M_ADDR2              27
#define  MB_ROLL_M_ADDR3              28
#define  MB_ROLL_M_ADDR4              29

#define  MB_SALES_ASMT                0
#define  MB_SALES_DOCNUM              1
#define  MB_SALES_DOCDATE             2
#define  MB_SALES_DOCCODE             3
#define  MB_SALES_SELLER              4
#define  MB_SALES_BUYER               5
#define  MB_SALES_PRICE               6
#define  MB_SALES_TAXAMT              7
#define  MB_SALES_GROUPSALE           8
#define  MB_SALES_GROUPASMT           9
#define  MB_SALES_XFERTYPE            10
#define  MB_SALES_ADJREASON           11
#define  MB_SALES_CONFCODE            12

#define  MB_SITUS_ASMT                0
#define  MB_SITUS_STRNAME             1
#define  MB_SITUS_STRNUM              2
#define  MB_SITUS_STRTYPE             3
#define  MB_SITUS_STRDIR              4
#define  MB_SITUS_UNIT                5
#define  MB_SITUS_COMMUNITY           6
#define  MB_SITUS_ZIP                 7
#define  MB_SITUS_SEQ                 8

#define  MB_CHAR_FEE_PRCL             0
#define  MB_CHAR_POOLS                1
#define  MB_CHAR_USECAT               2
#define  MB_CHAR_QUALITY              3
#define  MB_CHAR_YRBLT                4
#define  MB_CHAR_BLDGSQFT             5
#define  MB_CHAR_GARSQFT              6
#define  MB_CHAR_HEATING              7
#define  MB_CHAR_COOLING              8
#define  MB_CHAR_HEATING_SRC          9
#define  MB_CHAR_COOLING_SRC          10
#define  MB_CHAR_BEDS                 11
#define  MB_CHAR_FBATHS               12
#define  MB_CHAR_HBATHS               13
#define  MB_CHAR_FP                   14
#define  MB_CHAR_ASMT                 15
#define  MB_CHAR_HASSEPTIC            16
#define  MB_CHAR_HASSEWER             17
#define  MB_CHAR_HASWELL              18
#define  MBSIZ_CHAR_ASMT              12
#define  MBSIZ_CHAR_POOLS             2
#define  MBSIZ_CHAR_USECAT            2
#define  MBSIZ_CHAR_QUALITY           6
#define  MBSIZ_CHAR_YRBLT             4
#define  MBSIZ_CHAR_BLDGSQFT          9
#define  MBSIZ_CHAR_GARSQFT           9
#define  MBSIZ_CHAR_HEATING           2
#define  MBSIZ_CHAR_COOLING           2
#define  MBSIZ_CHAR_HEATSRC           2
#define  MBSIZ_CHAR_COOLSRC           2
#define  MBSIZ_CHAR_BEDS              3
#define  MBSIZ_CHAR_FBATHS            3
#define  MBSIZ_CHAR_HBATHS            3
#define  MBSIZ_CHAR_FP                2
#define  MBSIZ_CHAR_HASSEPTIC         1
#define  MBSIZ_CHAR_HASSEWER          1
#define  MBSIZ_CHAR_HASWELL           1

typedef struct _tCharRec
{
   char  Asmt[MBSIZ_CHAR_ASMT];
   char  NumPools[MBSIZ_CHAR_POOLS];
   char  LandUseCat[MBSIZ_CHAR_USECAT];
   char  QualityClass[MBSIZ_CHAR_QUALITY];
   char  YearBuilt[MBSIZ_CHAR_YRBLT];
   char  BuildingSize[MBSIZ_CHAR_BLDGSQFT];
   char  SqFTGarage[MBSIZ_CHAR_GARSQFT];
   char  Heating[MBSIZ_CHAR_HEATING];
   char  Cooling[MBSIZ_CHAR_COOLING];
   char  HeatingSource[MBSIZ_CHAR_HEATSRC];
   char  CoolingSource[MBSIZ_CHAR_COOLSRC];
   char  NumBedrooms[MBSIZ_CHAR_BEDS];
   char  NumFullBaths[MBSIZ_CHAR_FBATHS];
   char  NumHalfBaths[MBSIZ_CHAR_HBATHS];
   char  NumFireplaces[MBSIZ_CHAR_FP];
   char  FeeParcel[MBSIZ_CHAR_ASMT];
   char  HasSeptic;
   char  HasSewer;
   char  HasWell;
   char  CRLF[2];
} MB_CHAR;

typedef struct t_KnownData
{
   char  acApn[SIZ_APN_S];
   char  acZoning[SIZ_ZONE];
   char  acLandUse[SIZ_USE_CO];
   char  acAcres[SIZ_LOT_ACRES];
   char  acExeCd[3][8];
   char  acExeAmt[3][10];
   char  acLand[10];
   char  acImpr[10];
   char  acGrowingImpr[10];
   char  acPers[10];
   char  acPPMH[10];
   char  acFixt[10];
   char  acFixtRP[10];
   char  acFixtBus[10];
   char  acHomeSite[10];
} KNOWN_DATA;

/*
char  *myTrim(char *pString);
void  replChar(char *pBuf, char cSrch, char cRepl);
void  blankPad(char *pBuf, int iLen);
void  City2Code(char *pCityName, char *pCityCode);
long  sortFile(char *pFile1, char *pFile2, char *pCmd);
bool  ValidateSfx(char *pSuffix);
int   GetSfxCode(char *pSuffix);
int   atoin(char *pStr, int iLen);
int   appendFixFile(char *pFile1, char *pFile2, int iRecLen);
int   appendTxtFile(char *pFile1, char *pFile2);
*/
int   convertChar(char *pInfile, char *pOutfile=NULL);
bool  sqlConnect();

#endif // !defined(AFX_LOADMDB_H__36608E89_813F_463E_81EF_5D51E87E84CA__INCLUDED_)
