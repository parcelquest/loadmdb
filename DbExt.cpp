#include "stdafx.h"

#define INITGUID
#import "c:\Program Files\Common Files\System\ADO\msado15.dll" \
   no_namespace \
   rename("EOF", "EndOfFile")
#include <stdio.h>
#include "icrsint.h"
#include "logs.h"

#define SAFE_CALL( expression ) { HRESULT hr; if ( FAILED(hr=expression) ) _com_issue_error( hr ); }

int But_MatchGrGr(LPCSTR pProvider)
{
   char     acTmp[256], acType[16], acName[16];
   int      iTmp, iIdx, iCnt, iRet;
   CString  strType, strName, strOwner;
   HRESULT  hRet;

   _variant_t	vTmp;

   // Init
   strcpy(acType, "Type0");
   strcpy(acName, "Name0");

   // Initialize OLE.
   ::CoInitialize( NULL );

   _ConnectionPtr pConn( "ADODB.Connection" );	// Create a connection object.
   iRet = 0;

   try
   {
      // Open the connection.
      //pConn->ConnectionString = "Provider=SQLOLEDB;trusted_connection=yes;DATABASE=BUT;SERVER=INFOSVR;";
      LogMsg("But_MatchGrGr().Connection: %s", pProvider);
      pConn->ConnectionString = pProvider;
      pConn->Open("","","",NULL);

      // uses __uuidof instead of the name - there's no difference
      // but it's shown here to show there are two ways of doing this.
      _RecordsetPtr pRS(__uuidof(Recordset));	// Create a recordset object

      sprintf(acTmp, "SELECT * FROM But_GrGr INNER JOIN v_ApnOwner ON Apn=Left(R_Apn,9)");
      //SAFE_CALL( pRS->Open(acTmp, pConn.GetInterfacePtr(), adOpenDynamic, adLockOptimistic, adCmdUnknown) );
      hRet = pRS->Open(acTmp, pConn.GetInterfacePtr(), adOpenDynamic, adLockOptimistic, adCmdUnknown);

      if (!pRS->EndOfFile)
      {
         pRS->MoveFirst();

         FieldPtr	pFldNameCnt    =pRS->Fields->GetItem("NameCnt");
         FieldPtr	pFldOwner      =pRS->Fields->GetItem("Owner");
         FieldPtr	pFldApnMatched =pRS->Fields->GetItem("Apn_Matched");
         FieldPtr	pFldOnrmatched =pRS->Fields->GetItem("Owner_Matched");
         while (!pRS->EndOfFile)
         {
            vTmp.Clear();
            vTmp = pFldNameCnt->Value;
            iCnt = vTmp.intVal;
            if (iCnt > 10) iCnt = 10;

            // We only need to compare 9 names.  That's enough
            for (iIdx = 1; iIdx < iCnt; iIdx++)
            {
               acType[4] = iIdx | 0x30;
               acName[4] = iIdx | 0x30;
               vTmp.Clear();
               vTmp = pRS->Fields->GetItem(acType)->Value;
               vTmp.ChangeType(VT_BSTR);
               strType = (LPCTSTR)((_bstr_t)vTmp);
               if (strType == "O")
               {
                  vTmp.Clear();
                  vTmp = pRS->Fields->GetItem(acName)->Value;
                  vTmp.ChangeType(VT_BSTR);
                  strName = (LPCTSTR)((_bstr_t)vTmp);

                  vTmp.Clear();
                  vTmp = pFldOwner->Value;
                  vTmp.ChangeType(VT_BSTR);
                  strOwner = (LPCTSTR)((_bstr_t)vTmp);
                  iTmp = strOwner.FindOneOf(" ,");
                  if (iTmp < 3) iTmp = 10;         
                  if (strName.Left(iTmp) == strOwner.Left(iTmp))
                  {
                     pFldOnrmatched->Value = "Y";
                     break;
                  }
               }
            }

            pFldApnMatched->Value = "Y";
            pRS->Update();
            pRS->MoveNext();
            iRet++;
         }
      }

      SAFE_CALL( pRS->Close() );

      // Cleanup
      pConn->Close();

   }
   catch ( _com_error &theErr )
   {      
      // Iterating through the error collection.
      ErrorPtr	pADOError;
      long		numErrors;

      LogMsg( "COM Error in But_MatchGrGr()\n" );
      LogMsg0( "\tCode = %0xlx", theErr.Error() );

      _bstr_t bstrDescription(theErr.Description());
      LogMsg0( "\tDescription = %s", (LPCSTR)bstrDescription );
      LogMsg0( "\tHelp Context = %ld", theErr.HelpContext() );
      _bstr_t bstrHelpFile(theErr.HelpFile());
      LogMsg0( "\tHelp File = %s", (LPCSTR)bstrHelpFile );

      LogMsg0( "\nADO Error object" );

      numErrors = pConn->Errors->GetCount();

      for ( long i = 0; i < numErrors; i++ )
      {
         pADOError = pConn->Errors->GetItem( i );
         _bstr_t bstrDescription(pADOError->GetDescription());
         LogMsg0( "\tDescription = %s", (LPCSTR)bstrDescription );
         LogMsg0( "\tHelp Context = %ld", pADOError->GetHelpContext() );
         _bstr_t bstrHelpFile(pADOError->GetHelpFile());
         LogMsg0( "\tHelp File = %s", (LPCSTR)bstrHelpFile );
         LogMsg0( "\tNative Error = %ld", pADOError->GetNativeError() );
         LogMsg0( "\tNumber = %0xlx", pADOError->GetNumber() );
         _bstr_t bstrSource(pADOError->GetSource());
         LogMsg0( "\tSource = %s", (LPCSTR)bstrSource );
         LogMsg0( "\tSQLState = %ld\n", pADOError->GetSQLState() );
      }
      iRet = -1;
   }

   CoUninitialize();

   return iRet;
}

int doApnXlat(LPCSTR pProvider, LPCSTR pTable)
{
   char     acTmp[256];
   CString  strApn;
   HRESULT  hRet;
   int		iRet=0;

   _variant_t	vTmp;

   // Initialize OLE.
   ::CoInitialize( NULL );
   _ConnectionPtr pConn( "ADODB.Connection" );	// Create a connection object.

   try
   {
      // Open the connection.
      //pConn->ConnectionString = "Provider=SQLOLEDB;trusted_connection=yes;DATABASE=BUT;SERVER=INFOSVR;";
      pConn->ConnectionString = pProvider;
      pConn->Open("","","",NULL);

      // uses __uuidof instead of the name - there's no difference
      // but it's shown here to show there are two ways of doing this.
      _RecordsetPtr pRS(__uuidof(Recordset));	// Create a recordset object

      sprintf(acTmp, "SELECT Apn, NewApn FROM %s INNER JOIN ApnXlat ON %s.Apn=ApnXlat.OldApn", pTable, pTable);
      hRet = pRS->Open(acTmp, pConn.GetInterfacePtr(), adOpenDynamic, adLockOptimistic, adCmdUnknown);
      if (!pRS->EndOfFile)
      {
         pRS->MoveFirst();

         FieldPtr	pFldApn =pRS->Fields->GetItem("APN");
         FieldPtr	pFldNewApn =pRS->Fields->GetItem("NewApn");
         while (!pRS->EndOfFile)
         {
            //vTmp.Clear();
            //vTmp = pRS->Fields->GetItem("NewApn")->Value;
            //vTmp.ChangeType(VT_BSTR);
            //strApn = (LPCTSTR)((_bstr_t)vTmp);

            pFldApn->Value = pFldNewApn->Value;
            pRS->Update();
            pRS->MoveNext();
         }
         iRet++;
      }

      SAFE_CALL( pRS->Close() );

      // Cleanup
      pConn->Close();

   }
   catch ( _com_error &theErr )
   {      
      // Iterating through the error collection.
      ErrorPtr	pADOError;
      long		numErrors;

      LogMsg0( "COM Error in doApnXlat()\n" );
      LogMsg0( "\tCode = %0xlx\n", theErr.Error() );

      _bstr_t bstrDescription(theErr.Description());
      LogMsg0( "\tDescription = %s\n", (LPCSTR)bstrDescription );
      LogMsg0( "\tHelp Context = %ld\n", theErr.HelpContext() );
      _bstr_t bstrHelpFile(theErr.HelpFile());
      LogMsg0( "\tHelp File = %s\n", (LPCSTR)bstrHelpFile );

      LogMsg0( "\n\nADO Error object\n" );

      numErrors = pConn->Errors->GetCount();

      for ( long i = 0; i < numErrors; i++ )
      {
         LogMsg0( "\n" );
         pADOError = pConn->Errors->GetItem( i );
         _bstr_t bstrDescription(pADOError->GetDescription());
         LogMsg0( "\tDescription = %s\n", (LPCSTR)bstrDescription );
         LogMsg0( "\tHelp Context = %ld\n", pADOError->GetHelpContext() );
         _bstr_t bstrHelpFile(pADOError->GetHelpFile());
         LogMsg0( "\tHelp File = %s\n", (LPCSTR)bstrHelpFile );
         LogMsg0( "\tNative Error = %ld\n", pADOError->GetNativeError() );
         LogMsg0( "\tNumber = %0xlx\n", pADOError->GetNumber() );
         _bstr_t bstrSource(pADOError->GetSource());
         LogMsg0( "\tSource = %s\n", (LPCSTR)bstrSource );
         LogMsg0( "\tSQLState = %ld\n", pADOError->GetSQLState() );
      }
      iRet = -1;
   }

   CoUninitialize();

   return iRet;
}
